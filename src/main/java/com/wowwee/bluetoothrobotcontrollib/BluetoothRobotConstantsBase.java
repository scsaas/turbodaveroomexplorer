package com.wowwee.bluetoothrobotcontrollib;

public class BluetoothRobotConstantsBase {
    public static final String KMipProgrammableIOServiceUUID = "0000fff0-0000-1000-8000-00805f9b34fb";
    public static final String KMipPwmOutputServiceUUID = "0000ffb0-0000-1000-8000-00805f9b34fb";
    public static final String kMipReceiveDataCharateristicString = "receiveData";
    public static final String kMipReceiveDataCharateristicUUID = "0000ffe4-0000-1000-8000-00805f9b34fb";
    public static final String kMipReceiveDataNotificationUUID = "00002902-0000-1000-8000-00805f9b34fb";
    public static final String kMipReceiveDataServiceString = "recieveData";
    public static final String kMipReceiveDataServiceUUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
    public static final String kMipSendBytesDataCharateristicString = "sendData";
    public static final String kMipSendBytesDataCharateristicUUID = "0000ffe9-0000-1000-8000-00805f9b34fb";
    public static final String kMipSendDataServiceString = "sendData";
    public static final String kMipSendDataServiceUUID = "0000ffe5-0000-1000-8000-00805f9b34fb";

    public enum kMipModuleParameter_RemoteControlExtensionValues {
        kMipModuleParameter_RemoteControlExtension_SaveIOState((byte) 1),
        kMipModuleParameter_RemoteControlExtension_ForceSleepMode((byte) 2),
        kMipModuleParameter_RemoteControlExtension_DisconnectBluetoothClient((byte) 3),
        kMipModuleParameter_RemoteControlExtension_WriteCustomBroadcastDataToFlash((byte) 4);
        
        byte value;

        private kMipModuleParameter_RemoteControlExtensionValues(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kMipModuleParameter_RemoteControlExtensionValues getParamWithValue(byte value) {
            for (kMipModuleParameter_RemoteControlExtensionValues param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum kMipModuleParameter_TransmitPowerValues {
        kMipModuleParameter_TransmitPower_Plus4dBm((byte) 0),
        kMipModuleParameter_TransmitPower_0dBm((byte) 1),
        kMipModuleParameter_TransmitPower_Minus6dBm((byte) 2),
        kMipModuleParameter_TransmitPower_Minus23dBm((byte) 3);
        
        byte value;

        private kMipModuleParameter_TransmitPowerValues(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kMipModuleParameter_TransmitPowerValues getParamWithValue(byte value) {
            for (kMipModuleParameter_TransmitPowerValues param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum kModuleParameterBTCommunicationIntervalValues {
        kModuleParameterBTCommunicationInterval20ms((byte) 0),
        kModuleParameterBTCommunicationInterval50ms((byte) 1),
        kModuleParameterBTCommunicationInterval100ms((byte) 2),
        kModuleParameterBTCommunicationInterval200ms((byte) 3),
        kModuleParameterBTCommunicationInterval300ms((byte) 4),
        kModuleParameterBTCommunicationInterval400ms((byte) 5),
        kModuleParameterBTCommunicationInterval500ms((byte) 6),
        kModuleParameterBTCommunicationInterval1000ms((byte) 7),
        kModuleParameterBTCommunicationInterval2000ms((byte) 8);
        
        byte value;

        private kModuleParameterBTCommunicationIntervalValues(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kModuleParameterBTCommunicationIntervalValues getParamWithValue(byte value) {
            for (kModuleParameterBTCommunicationIntervalValues param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum kModuleParameterValues {
        kModuleParameter_ResetModuleRestoreFactorySettings((byte) 54),
        kModuleParameter_ResetModuleResetUserData((byte) 53),
        kModuleParameter_RestartModule((byte) 85);
        
        byte value;

        private kModuleParameterValues(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }
    }

    public enum kModuleParameter_BroadcastPeriodValues {
        kModuleParameter_BroadcastPeriod200MS((byte) 0),
        kModuleParameter_BroadcastPeriod500MS((byte) 1),
        kModuleParameter_BroadcastPeriod1000MS((byte) 2),
        kModuleParameter_BroadcastPeriod1500MS((byte) 3),
        kModuleParameter_BroadcastPeriod2000MS((byte) 4),
        kModuleParameter_BroadcastPeriod2500MS((byte) 5),
        kModuleParameter_BroadcastPeriod3000MS((byte) 6),
        kModuleParameter_BroadcastPeriod4000MS((byte) 7),
        kModuleParameter_BroadcastPeriod5000MS((byte) 8);
        
        byte value;

        private kModuleParameter_BroadcastPeriodValues(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kModuleParameter_BroadcastPeriodValues getParamWithValue(byte value) {
            for (kModuleParameter_BroadcastPeriodValues param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }
}
