package com.wowwee.bluetoothrobotcontrollib.sdk;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobot;
import com.wowwee.bluetoothrobotcontrollib.BuildConfig;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues.kMipHeadLedValue;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues.kMipPingResponse;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues.kMipResetMcu;
import com.wowwee.bluetoothrobotcontrollib.MipRobotSound;
import com.wowwee.bluetoothrobotcontrollib.RobotCommand;
import com.wowwee.bluetoothrobotcontrollib.util.AdRecord;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MipRobot extends BluetoothRobot {
    public static int MipType_CoderMip = 1;
    public static int MipType_MinionMip = 2;
    public static int MipType_Mip = 0;
    public int batteryLevel;
    public kMipPingResponse bootMode;
    protected MipRobotInterface callbackInterface = null;
    public boolean disableReceivedCommandProcessing = false;
    public Date mipFirmwareVersionDate;
    public int mipFirmwareVersionId;
    public int mipHardwareVersion;
    public int mipRobotType = MipType_Mip;
    public int mipVoiceFirmwareVersion;
    public int mipVolume;
    public byte position;
    public byte toyActivationStatus;

    public interface MipRobotInterface {
        void mipDeviceDisconnected(MipRobot mipRobot);

        void mipDeviceReady(MipRobot mipRobot);

        void mipRobotDidReceiveBatteryLevelReading(MipRobot mipRobot, int i);

        void mipRobotDidReceiveClapDetectionStatusIsEnabled(MipRobot mipRobot, boolean z, long j);

        void mipRobotDidReceiveGesture(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveHardwareVersion(MipRobot mipRobot, int i, int i2);

        void mipRobotDidReceiveIRCommand(MipRobot mipRobot, ArrayList<Byte> arrayList, int i);

        void mipRobotDidReceiveNumberOfClaps(MipRobot mipRobot, int i);

        void mipRobotDidReceivePosition(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveSoftwareVersion(Date date, int i);

        void mipRobotDidReceiveVolumeLevel(MipRobot mipRobot, int i);

        void mipRobotDidReceiveWeightReading(MipRobot mipRobot, byte b, boolean z);

        void mipRobotIsCurrentlyInBootloader(MipRobot mipRobot, boolean z);

        void mipRobotDidRecieveRadarResponse(MipRobot mipRobot, byte b);
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.sdk.MipRobot$1 */
    class C00221 implements Runnable {
        C00221() {
        }

        public void run() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MipRobot.this.getMipVolumeLevel();
            MipRobot.this.readMipStatus();
            MipRobot.this.getMipActivationStatus();
            MipRobot.this.readMipHardwareVersion();
            MipRobot.this.readMipFirmwareVersion();
        }
    }

    public MipRobot(BluetoothDevice pBluetoothDevice, List<AdRecord> pScanRecords, BluetoothLeService pBluetoothLeService) {
        super(pBluetoothDevice, pScanRecords, pBluetoothLeService);
    }

    public void setCallbackInterface(MipRobotInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public void peripheralDidConnect() {
        super.peripheralDidConnect();
        MipRobotFinder.getInstance().mipRobotDidConnect(this);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void peripheralDidDisconnect() {
        if (this.kBluetoothRobotState == 2) {
            super.peripheralDidDisconnect();
            return;
        }
        super.peripheralDidDisconnect();
        MipRobotFinder.getInstance().mipRobotDidDisconnect(this);
        if (this.callbackInterface != null) {
            this.callbackInterface.mipDeviceDisconnected(this);
        }
    }

    public void peripheralDidBecomeReady() {
        super.peripheralDidBecomeReady();
        initDefaultValues();
        if (this.callbackInterface != null) {
            this.callbackInterface.mipDeviceReady(this);
        } else {
            Log.e("MipRobot", BuildConfig.FLAVOR + hashCode() + " peripheralDidBecomeReady callbackInterface = null");
        }
        new Thread(new C00221()).start();
    }

    private void initDefaultValues() {
        this.position = MipCommandValues.kMipPositionOnBack;
    }

    public void readMipStatus() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetStatus));
    }

    public void readMipHardwareVersion() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetHardwareVersion));
    }

    public void readMipFirmwareVersion() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetSoftwareVersion));
    }

    public void getMipVolumeLevel() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetVolumeLevel));
    }

    public void getMipActivationStatus() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetToyActivatedStatus));
    }

    public void readMipSensorWeightLevel() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetWeightLevel));
    }

    public void setMipVolumeLevel(byte volumeLevel) {
        this.mipVolume = Integer.parseInt(Byte.toString(volumeLevel));
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetVolumeLevel, volumeLevel));
    }

    public void setMipClapEnable(boolean enabled) {
        if (enabled) {
            sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetClapDetectionStatus, (byte) 1));
        } else {
            sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetClapDetectionStatus, (byte) 0));
        }
    }

    public void getMipClapStatus() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipClapDetectionStatus));
    }

    public void setMipClapDelayTime(int milliseconds) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetClapDetectTiming, (byte) ((milliseconds >> 8) & 255), (byte) (milliseconds & 255)));
    }

    public void setMipGestureMode(byte gestureMode) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetRadarModeOrRadarResponse, gestureMode));
    }

    public void getMipGestureMode() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetRadarMode));
    }

    public void mipRebootWriteFlash(boolean writeFlash, boolean writeIO) {
        if (!writeFlash || !writeIO) {
            if (writeFlash && !writeIO) {
                sendRobotCommand(RobotCommand.create(MipCommandValues.kMipReboot, kMipResetMcu.kMipResetMcu_ResetAndForceBootloader.getValue()));
            } else if (writeFlash || !writeIO) {
                sendRobotCommand(RobotCommand.create(MipCommandValues.kMipReboot, kMipResetMcu.kMipResetMcu_NormalReset.getValue()));
            }
        }
    }

    public void mipDrive(float[] vector) {
        boolean driveForward;
        boolean turnRight = true;
        int xValue = Math.max(Math.min(Math.round(vector[0] * 32.0f), 32), -32);
        int yValue = Math.max(Math.min(Math.round(vector[1] * 32.0f), 32), -32);
        if (yValue > 0) {
            driveForward = true;
        } else {
            driveForward = false;
        }
        if (xValue <= 0) {
            turnRight = false;
        }
        xValue = Math.abs(xValue);
        yValue = Math.abs(yValue);
        byte driveValue = (byte) 0;
        if (yValue != 0) {
            driveValue = (byte) (driveForward ? MipCommandValues.kMipDriveCont_FW_Speed1 + yValue : MipCommandValues.kMipDriveCont_BW_Speed1 + yValue);
        }
        byte turnValue = (byte) 0;
        if (xValue != 0) {
            turnValue = (byte) (turnRight ? MipCommandValues.kMipDriveCont_Right_Speed1 + xValue : MipCommandValues.kMipDriveCont_Left_Speed1 + xValue);
        }
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDrive_Continuous, driveValue, turnValue));
    }

    public void mipDriveForwardForMilliseconds(int msecs, int speed) {
        byte rate = (byte) Math.min(speed, 30);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDriveForwardWithTime, rate, (byte) Math.min(msecs / 7, 255)));
    }

    public void mipDriveBackwardForMilliseconds(int msecs, int speed) {
        byte rate = (byte) Math.min(speed, 30);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDriveBackwardWithTime, rate, (byte) Math.min(msecs / 7, 255)));
    }

    public void mipPunchLeftWithSpeed(int speed) {
        mipTurnRightByDegrees(90, speed);
    }

    public void mipPunchRightWithSpeed(int speed) {
        mipTurnLeftByDegrees(90, speed);
    }

    public void mipTurnLeftByDegrees(int degrees, int speed) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kTurnLeftByAngle, (byte) (Math.min(degrees, 360) / 4), (byte) Math.min(speed, 24)));
    }

    public void mipTurnRightByDegrees(int degrees, int speed) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kTurnRightByAngle, (byte) (Math.min(degrees, 360) / 4), (byte) Math.min(speed, 24)));
    }

    public void mipDriveDistanceByCm(int distanceInCm) {
        mipDriveDistanceByCm(distanceInCm, 0);
    }

    public void mipDriveDistanceByCm(int distanceInCm, int angleInDegrees) {
        byte direction = MipCommandValues.kMipDrive_DriveDirectionForward;
        if (distanceInCm < 0) {
            direction = MipCommandValues.kMipDrive_DriveDirectionBackward;
        }
        distanceInCm = Math.abs(distanceInCm);
        byte angleDirection = MipCommandValues.kMipDrive_TurnDirectionClockwise;
        if (angleInDegrees < 0) {
            angleDirection = MipCommandValues.kMipDrive_TurnDirectionAnticlockwise;
        }
        angleInDegrees = Math.abs(angleInDegrees);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDrive_FixedDistance, direction, (byte) distanceInCm, angleDirection, (byte) ((angleInDegrees >> 8) & 255), (byte) (angleInDegrees & 255)));
    }

    public void mipFalloverWithStyle(byte position) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipShouldFallover));
    }

    public void mipPing() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipCheckBootmode));
    }

    public void setMipHeadLeds(kMipHeadLedValue led1, kMipHeadLedValue led2, kMipHeadLedValue led3, kMipHeadLedValue led4) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetHeadLed, led1.getValue(), led2.getValue(), led3.getValue(), led4.getValue()));
    }

    public void setMipChestRGBLedFlashingWithColor(byte red, byte green, byte blue, byte timeOn, byte timeOff) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipFlashChestRGBLed, red, green, blue, timeOn, timeOff));
    }

    public void setMipChestRGBLedWithColor(byte red, byte green, byte blue, byte fadeIn) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetChestRGBLed, red, green, blue, fadeIn));
    }

    public void mipTransmitIRGameDataWithGameType(byte gameType, byte mipId, short gameData, byte powerDistanceCm) {
        mipTransmitIRMipCommandOfBitLength((byte) 32, gameType, mipId, (byte) (gameData >> 8), (byte) (gameData & 255), powerDistanceCm);
    }

    public void mipTransmitIRMipCommandOfBitLength(byte bitLength, byte byte1, byte byte2, byte byte3, byte byte4, byte distanceCm) {
        byte b;
        byte b2 = (byte) 0;
        if (bitLength < (byte) 1 || bitLength > (byte) 32) {
            Log.e("MipRobot", "Send IR bitLength must be 1-32 bits");
        }
        if (distanceCm < (byte) 1 || distanceCm > (byte) 120) {
            Log.e("MipRobot", "Send IR distance must be in the range of 1-120 (1-300cm)");
        }
        byte b3 = MipCommandValues.kMipTransmitIRCommand;
        if (bitLength >= (byte) 25) {
            b = byte4;
        } else {
            b = (byte) 0;
        }
        byte b4 = bitLength >= (byte) 17 ? byte3 : (byte) 0;
        if (bitLength >= (byte) 9) {
            b2 = byte2;
        }
        sendRobotCommand(RobotCommand.create(b3, b, b4, b2, byte1, bitLength, distanceCm));
    }

    public void mipPlaySound(MipRobotSound sound) {
        sendRobotCommand(sound.robotCommand());
    }

    protected void handleReceivedMipCommand(RobotCommand robotCommand) {
        if (robotCommand == null) {
            Log.d("MipRobot", "handleReceivedMipCommand - RobotCommand is nil");
            return;
        }
        byte commandValue = robotCommand.getCmdByte();
        ArrayList<Byte> dataArray = robotCommand.getDataArray();
        if (commandValue == MipCommandValues.kMipGetGameMode) {
            return;
        }
        if (commandValue == MipCommandValues.kMipGetStatus) {
            if (dataArray.size() == 2) {
                this.batteryLevel = ((Byte) dataArray.get(0)).byteValue();
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveBatteryLevelReading(this, this.batteryLevel);
                }
                byte newPosition = ((Byte) dataArray.get(1)).byteValue();
                if (this.position != newPosition) {
                    this.position = newPosition;
                }
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceivePosition(this, this.position);
                    return;
                }
                return;
            }
            Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetStatus command");
        } else if (commandValue == MipCommandValues.kMipGetHardwareVersion) {
            if (dataArray.size() == 2) {
                int voiceFirmwareVersion = ((Byte) dataArray.get(0)).byteValue();
                if (voiceFirmwareVersion >= 6 && voiceFirmwareVersion < 14) {
                    this.mipVoiceFirmwareVersion = 1;
                } else if (voiceFirmwareVersion >= 14 && voiceFirmwareVersion < 22) {
                    this.mipVoiceFirmwareVersion = 2;
                } else if (voiceFirmwareVersion >= 22 && voiceFirmwareVersion < 30) {
                    this.mipVoiceFirmwareVersion = 3;
                } else if (voiceFirmwareVersion >= 30 && voiceFirmwareVersion < 38) {
                    this.mipVoiceFirmwareVersion = 4;
                } else if (voiceFirmwareVersion >= 38 && voiceFirmwareVersion < 46) {
                    this.mipVoiceFirmwareVersion = 5;
                } else if (voiceFirmwareVersion >= 46 && voiceFirmwareVersion < 54) {
                    this.mipVoiceFirmwareVersion = 6;
                } else if (voiceFirmwareVersion >= 54 && voiceFirmwareVersion < 62) {
                    this.mipVoiceFirmwareVersion = 7;
                } else if (voiceFirmwareVersion >= 62 && voiceFirmwareVersion < 70) {
                    this.mipVoiceFirmwareVersion = 8;
                } else if (voiceFirmwareVersion >= 70 && voiceFirmwareVersion < 78) {
                    this.mipVoiceFirmwareVersion = 9;
                } else if (voiceFirmwareVersion >= 78 && voiceFirmwareVersion < 86) {
                    this.mipVoiceFirmwareVersion = 10;
                } else if (voiceFirmwareVersion >= 86 && voiceFirmwareVersion < 94) {
                    this.mipVoiceFirmwareVersion = 11;
                } else if (voiceFirmwareVersion < 94 || voiceFirmwareVersion >= 102) {
                    this.mipVoiceFirmwareVersion = 0;
                } else {
                    this.mipVoiceFirmwareVersion = 12;
                }
                this.mipHardwareVersion = ((Byte) dataArray.get(1)).byteValue();
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveHardwareVersion(this, this.mipHardwareVersion, this.mipVoiceFirmwareVersion);
                    return;
                }
                return;
            }
            Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetHardwareVersion command");
        } else if (commandValue == MipCommandValues.kMipGetSoftwareVersion) {
            if (dataArray.size() == 4) {
                Calendar cal = Calendar.getInstance();
                cal.set(((Byte) dataArray.get(0)).byteValue() + 2000, Math.max(0, ((Byte) dataArray.get(1)).byteValue() - 1), ((Byte) dataArray.get(2)).byteValue());
                this.mipFirmwareVersionId = ((Byte) dataArray.get(3)).byteValue();
                this.mipFirmwareVersionDate = cal.getTime();
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveSoftwareVersion(this.mipFirmwareVersionDate, this.mipFirmwareVersionId);
                    return;
                }
                return;
            }
            Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetSoftwareVersion command: " + robotCommand.description());
        } else if (commandValue == MipCommandValues.kMipGetVolumeLevel) {
            if (dataArray.size() == 1) {
                this.mipVolume = ((Byte) dataArray.get(0)).byteValue();
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveVolumeLevel(this, this.mipVolume);
                    return;
                }
                return;
            }
            Log.e("MipRobot", "Data array count incorrect for MipGetVolumeLevel command");
        } else if (commandValue == MipCommandValues.kMipGetToyActivatedStatus) {
            if (dataArray.size() == 1) {
                this.toyActivationStatus = ((Byte) dataArray.get(0)).byteValue();
            } else {
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetToyActivatedStatus command");
            }
        } else if (commandValue == MipCommandValues.kMipReceiveIRCommand) {
            if (this.callbackInterface != null) {
                this.callbackInterface.mipRobotDidReceiveIRCommand(this, dataArray, dataArray.size());
            }
        } else if (commandValue == MipCommandValues.kMipGetWeightLevel) {
            if (dataArray.size() == 1) {
                byte weightData = ((Byte) dataArray.get(0)).byteValue();
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveWeightReading(this, weightData, weightData < (byte) -45);
                    return;
                }
                return;
            }
            Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetWeightLevel command");
        } else if (commandValue == MipCommandValues.kMipSetGestureModeOrGestureDetected) {
            if (dataArray.size() != 1) {
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipSetGestureModeOrGestureDetected command");
            } else if (this.callbackInterface != null) {
                this.callbackInterface.mipRobotDidReceiveGesture(this, ((Byte) dataArray.get(0)).byteValue());
            }
        } else if (commandValue == MipCommandValues.kMipClapsDetected) {
            if (dataArray.size() != 1) {
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipClapsDetected command");
            } else if (this.callbackInterface != null) {
                this.callbackInterface.mipRobotDidReceiveNumberOfClaps(this, ((Byte) dataArray.get(0)).intValue());
            }
        } else if (commandValue == MipCommandValues.kMipClapDetectionStatus) {
            if (dataArray.size() == 3) {
                long value = (long) (((Byte) dataArray.get(2)).intValue() | (((Byte) dataArray.get(1)).intValue() << 8));
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveClapDetectionStatusIsEnabled(this, ((Byte) dataArray.get(0)).byteValue() != (byte) 0, value);
                    return;
                }
                return;
            }
            Log.e("MipRobot", "ERROR: Data array count incorrect for kMipClapDetectionStatus command");
        } else if (commandValue == MipCommandValues.kMipCheckBootmode) {

            if (dataArray.size() == 1) {
                this.bootMode = kMipPingResponse.getParamWithValue(((Byte) dataArray.get(0)).byteValue());
                if (this.bootMode == kMipPingResponse.kMipPingResponseBootloader) {
                    this.callbackInterface.mipRobotIsCurrentlyInBootloader(this, true);
                } else {
                    this.callbackInterface.mipRobotIsCurrentlyInBootloader(this, false);
                }
            } else if (dataArray.size() == 0) {
                this.bootMode = kMipPingResponse.kMipPingResponseNormalRomNoBootloader;
                this.callbackInterface.mipRobotIsCurrentlyInBootloader(this, false);
            } else {
                Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetUserData command");
            }
        }
        else if(commandValue == MipCommandValues.kMipSetRadarModeOrRadarResponse) {

            if (dataArray.size() == 1){
                if(this.callbackInterface!=null) {
                    this.callbackInterface.mipRobotDidRecieveRadarResponse(this, dataArray.get(0));
                }
                else{
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipRadar command");
                }
            }

        }
    }

    public void didReceiveRobotCommand(RobotCommand robotCommand) {
        Log.d("MipRobot", "didReceiveRobotCommand:: " + robotCommand.description());
        handleReceivedMipCommand(robotCommand);
    }
}
