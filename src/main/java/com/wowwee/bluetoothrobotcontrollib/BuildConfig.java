package com.wowwee.bluetoothrobotcontrollib;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.wowwee.bluetoothrobotcontrollib";
    public static final String BUILD_TYPE = "debug";
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 3;
    public static final String VERSION_NAME = "0.4";
}
