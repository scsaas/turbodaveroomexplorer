package com.wowwee.bluetoothrobotcontrollib.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AdRecord {
    public static final int TYPE_CONNINTERVAL = 18;
    public static final int TYPE_FLAGS = 1;
    public static final int TYPE_MANUFACTURERDATA = -1;
    public static final int TYPE_NAME = 9;
    public static final int TYPE_NAME_SHORT = 8;
    public static final int TYPE_SERVICEDATA = 22;
    public static final int TYPE_TRANSMITPOWER = 10;
    public static final int TYPE_UUID128 = 7;
    public static final int TYPE_UUID128_INC = 6;
    public static final int TYPE_UUID16 = 3;
    public static final int TYPE_UUID16_INC = 2;
    public static final int TYPE_UUID32 = 5;
    public static final int TYPE_UUID32_INC = 4;
    protected static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private byte[] mData;
    private int mLength;
    private int mType;

    public static List<AdRecord> parseScanRecord(byte[] scanRecord) {
        List<AdRecord> records = new ArrayList();
        int index = 0;
        while (index < scanRecord.length) {
            int index2 = index + 1;
            int length = scanRecord[index];
            if (length == 0) {
                index = index2;
                break;
            }
            int type = scanRecord[index2];
            if (type == 0) {
                index = index2;
                break;
            }
            records.add(new AdRecord(length, type, Arrays.copyOfRange(scanRecord, index2 + 1, index2 + length)));
            index = index2 + length;
        }
        return records;
    }

    public static String getName(AdRecord nameRecord) {
        return new String(nameRecord.mData);
    }

    public static int getServiceDataUuid(AdRecord serviceData) {
        if (serviceData.mType != 22) {
            return -1;
        }
        byte[] raw = serviceData.mData;
        return ((raw[1] & 255) << 8) + (raw[0] & 255);
    }

    public static byte[] getServiceData(AdRecord serviceData) {
        if (serviceData.mType != 22) {
            return null;
        }
        byte[] raw = serviceData.mData;
        return Arrays.copyOfRange(raw, 2, raw.length);
    }

    public static byte[] getRawData(AdRecord record) {
        return record.mData;
    }

    public AdRecord(int length, int type, byte[] data) {
        this.mLength = length;
        this.mType = type;
        this.mData = data;
    }

    public int getLength() {
        return this.mLength;
    }

    public int getType() {
        return this.mType;
    }

    public byte[] getmData() {
        return this.mData;
    }

    public String toString() {
        switch (this.mType) {
            case -1:
                int initialProductId = (this.mData[0] << 8) | this.mData[1];
                int initialBatteryLevel = this.mData[6];
                int initialIOModes = this.mData[7];
                return "Manufacturer Data :" + initialProductId + " Battery Level: " + initialBatteryLevel + " initialIOModes: " + initialIOModes + " initialIOStates: " + this.mData[8];
            case 1:
                return "Flags: " + this.mData[0];
            case 2:
            case 3:
                return "UUIDs: " + bytesToHex(this.mData);
            case TYPE_NAME_SHORT /*8*/:
            case TYPE_NAME /*9*/:
                return "Name: " + getName(this);
            case 10:
                return "Transmit Power: " + this.mData[0];
            case TYPE_CONNINTERVAL /*18*/:
                return "Connect Interval: " + Float.intBitsToFloat((((this.mData[0] & 255) | ((this.mData[1] & 255) << 8)) | ((this.mData[2] & 255) << 16)) | ((this.mData[3] & 255) << 24));
            case TYPE_SERVICEDATA /*22*/:
                return "Service Data";
            default:
                return "Unknown Structure " + this.mType + ": " + getName(this);
        }
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[(bytes.length * 2)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[(j * 2) + 1] = hexArray[v & 15];
        }
        return new String(hexChars);
    }
}
