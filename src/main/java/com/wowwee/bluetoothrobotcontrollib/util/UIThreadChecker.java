package com.wowwee.bluetoothrobotcontrollib.util;

import android.os.Looper;
import android.util.Log;

public class UIThreadChecker {
    public static boolean checkIsUIThread(String debugMessage) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            Log.d("UIThreadChecker", debugMessage + " is on UI Thread");
            return true;
        }
        Log.d("UIThreadChecker", debugMessage + " is NOT on UI Thread");
        return false;
    }
}
