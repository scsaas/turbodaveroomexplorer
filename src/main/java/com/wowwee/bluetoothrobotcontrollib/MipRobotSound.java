package com.wowwee.bluetoothrobotcontrollib;

public class MipRobotSound {
    public byte soundDelay;
    public byte soundFile;
    public byte soundVolume;

    public static MipRobotSound create(byte file) {
        return new MipRobotSound(file);
    }

    public static MipRobotSound create(byte file, byte delay) {
        return new MipRobotSound(file, delay);
    }

    public static MipRobotSound create(byte file, byte delay, byte volume) {
        return new MipRobotSound(file, delay, volume);
    }

    public MipRobotSound(byte file) {
        this(file, (byte) 0, (byte) -1);
    }

    public MipRobotSound(byte file, byte delay) {
        this(file, delay, (byte) -1);
    }

    public MipRobotSound(byte file, byte delay, byte volume) {
        this.soundFile = file;
        this.soundDelay = delay;
        this.soundVolume = volume;
    }

    public RobotCommand robotCommand() {
        if (this.soundVolume == (byte) -1) {
            return RobotCommand.create(MipCommandValues.kMipPlaySound, this.soundFile, this.soundDelay);
        }
        return RobotCommand.create(MipCommandValues.kMipPlaySound, this.soundFile, this.soundDelay, this.soundVolume, (byte) 0);
    }
}
