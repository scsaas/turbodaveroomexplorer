package com.wowwee.bluetoothrobotcontrollib;

import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService.BRServiceAction;
import java.util.ArrayList;
import java.util.Iterator;

public class RobotCommand {
    protected static final char[] hexArray = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private byte cmdByte;
    public BRServiceAction completedCallback;
    private ArrayList<Byte> dataArray;
    public int length;

    public static RobotCommand create(String asciiHex) {
        if (asciiHex == null) {
            return null;
        }
        return new RobotCommand(asciiHex);
    }

    public static RobotCommand create(byte command) {
        return new RobotCommand(command);
    }

    public static RobotCommand create(byte command, ArrayList<Byte> dataArray) {
        return new RobotCommand(command, dataArray);
    }

    public static RobotCommand create(byte command, byte byte1) {
        ArrayList<Byte> dataArray = new ArrayList();
        dataArray.add(Byte.valueOf(byte1));
        return new RobotCommand(command, dataArray);
    }

    public static RobotCommand create(byte command, byte byte1, byte byte2) {
        ArrayList<Byte> dataArray = new ArrayList();
        dataArray.add(Byte.valueOf(byte1));
        dataArray.add(Byte.valueOf(byte2));
        return new RobotCommand(command, dataArray);
    }

    public static RobotCommand create(byte command, byte byte1, byte byte2, byte byte3) {
        ArrayList<Byte> dataArray = new ArrayList();
        dataArray.add(Byte.valueOf(byte1));
        dataArray.add(Byte.valueOf(byte2));
        dataArray.add(Byte.valueOf(byte3));
        return new RobotCommand(command, dataArray);
    }

    public static RobotCommand create(byte command, byte byte1, byte byte2, byte byte3, byte byte4) {
        ArrayList<Byte> dataArray = new ArrayList();
        dataArray.add(Byte.valueOf(byte1));
        dataArray.add(Byte.valueOf(byte2));
        dataArray.add(Byte.valueOf(byte3));
        dataArray.add(Byte.valueOf(byte4));
        return new RobotCommand(command, dataArray);
    }

    public static RobotCommand create(byte command, byte byte1, byte byte2, byte byte3, byte byte4, byte byte5) {
        ArrayList<Byte> dataArray = new ArrayList();
        dataArray.add(Byte.valueOf(byte1));
        dataArray.add(Byte.valueOf(byte2));
        dataArray.add(Byte.valueOf(byte3));
        dataArray.add(Byte.valueOf(byte4));
        dataArray.add(Byte.valueOf(byte5));
        return new RobotCommand(command, dataArray);
    }

    public static RobotCommand create(byte command, byte byte1, byte byte2, byte byte3, byte byte4, byte byte5, byte byte6) {
        ArrayList<Byte> dataArray = new ArrayList();
        dataArray.add(Byte.valueOf(byte1));
        dataArray.add(Byte.valueOf(byte2));
        dataArray.add(Byte.valueOf(byte3));
        dataArray.add(Byte.valueOf(byte4));
        dataArray.add(Byte.valueOf(byte5));
        dataArray.add(Byte.valueOf(byte6));
        return new RobotCommand(command, dataArray);
    }

    public static RobotCommand create(byte command, byte byte1, byte byte2, byte byte3, byte byte4, byte byte5, byte byte6, byte byte7) {
        ArrayList<Byte> dataArray = new ArrayList();
        dataArray.add(Byte.valueOf(byte1));
        dataArray.add(Byte.valueOf(byte2));
        dataArray.add(Byte.valueOf(byte3));
        dataArray.add(Byte.valueOf(byte4));
        dataArray.add(Byte.valueOf(byte5));
        dataArray.add(Byte.valueOf(byte6));
        dataArray.add(Byte.valueOf(byte7));
        return new RobotCommand(command, dataArray);
    }

    public RobotCommand(String asciiHex) {
        boolean uneven = true;
        this.dataArray = new ArrayList();
        this.completedCallback = null;
        if (asciiHex == null || asciiHex.length() <= 1) {
            Log.w("RobotCommand", "Received invalid command " + asciiHex + " invalid length! Must be at least 2 characters");
        }
        int asciiLength = asciiHex.length();
        ArrayList<Byte> theDataArrayList = new ArrayList();
        if (asciiLength % 2 != 1) {
            uneven = false;
        }
        if (uneven) {
            asciiLength--;
            Log.w("RobotCommand", "Received invalid command " + asciiHex + " invalid length! Chopping off last character");
        }
        for (int i = 0; i < asciiLength; i += 2) {
            theDataArrayList.add(Byte.valueOf(Integer.valueOf(Integer.parseInt(asciiHex.substring(i, i + 2), 16)).byteValue()));
        }
        this.cmdByte = ((Byte) theDataArrayList.get(0)).byteValue();
        if (this.cmdByte == (byte) 0) {
            Log.w("RobotCommand", "Received invalid command, it cannot be zero");
        }
        this.length = theDataArrayList.size();
        theDataArrayList.remove(0);
        this.dataArray = theDataArrayList;
    }

    public RobotCommand(byte command) {
        this(command, null);
    }

    public RobotCommand(byte command, ArrayList<Byte> dataArray) {
        this.dataArray = new ArrayList();
        this.completedCallback = null;
        this.cmdByte = command;
        this.dataArray = dataArray;
        if (dataArray != null) {
            this.length = dataArray.size() + 1;
        } else {
            this.length = 1;
        }
    }

    public RobotCommand(ArrayList<Byte> byteArray) {
        this.dataArray = new ArrayList();
        this.completedCallback = null;
        if (byteArray.size() > 0) {
            this.cmdByte = ((Byte) byteArray.get(0)).byteValue();
            this.dataArray = new ArrayList();
            for (int i = 1; i < byteArray.size(); i++) {
                this.dataArray.add(byteArray.get(i));
            }
            this.length = this.dataArray.size() + 1;
            return;
        }
        Log.e("RobotCommand", "Byte array cannot be empty");
    }

    public byte[] data() {
        byte[] commandData;
        if (this.dataArray != null) {
            commandData = new byte[(this.dataArray.size() + 1)];
        } else {
            commandData = new byte[1];
        }
        commandData[0] = this.cmdByte;
        if (this.dataArray != null) {
            for (int i = 0; i < this.dataArray.size(); i++) {
                commandData[i + 1] = ((Byte) this.dataArray.get(i)).byteValue();
            }
        }
        return commandData;
    }

    public byte getCmdByte() {
        return this.cmdByte;
    }

    public ArrayList<Byte> getDataArray() {
        return this.dataArray;
    }

    public String description() {
        String desc = "RobotCommand: " + byteArrayToHexString(new byte[]{this.cmdByte});
        if (this.dataArray != null) {
            Iterator it = this.dataArray.iterator();
            while (it.hasNext()) {
                Byte b = (Byte) it.next();
                desc = desc + " " + byteArrayToHexString(new byte[]{b.byteValue()});
            }
        }
        if (this.completedCallback != null) {
            return desc + " with callback";
        }
        return desc;
    }

    public static String byteArrayToHexString(byte[] bytes) {
        char[] hexChars = new char[(bytes.length * 2)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[(j * 2) + 1] = hexArray[v & 15];
        }
        return new String(hexChars);
    }
}
