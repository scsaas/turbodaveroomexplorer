package com.wowwee.bluetoothrobotcontrollib.minionmip.sdk;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues;
import com.wowwee.bluetoothrobotcontrollib.RobotCommand;
import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobot;
import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobot.MipRobotInterface;
import com.wowwee.bluetoothrobotcontrollib.util.AdRecord;
import java.util.List;

public class MinionMipRobot extends MipRobot {
    protected MinionMipRobotInterface callbackInterface = null;

    public interface MinionMipRobotInterface extends MipRobotInterface {
    }

    public MinionMipRobot(BluetoothDevice pBluetoothDevice, List<AdRecord> pScanRecords, BluetoothLeService pBluetoothLeService) {
        super(pBluetoothDevice, pScanRecords, pBluetoothLeService);
    }

    public void setCallbackInterface(MinionMipRobotInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
        this.callbackInterface = callbackInterface;
    }

    public void peripheralDidBecomeReady() {
        Log.d("MipRobot", " peripheralDidBecomeReady() ");
        super.peripheralDidBecomeReady();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void playMinionMipSoundWithIndex(Byte soundValue) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipPlaySound, soundValue.byteValue(), (byte) 0));
    }

    public void handleReceivedMipCommand(RobotCommand robotCommand) {
        super.handleReceivedMipCommand(robotCommand);
        if ((this.callbackInterface == null || !this.disableReceivedCommandProcessing) && robotCommand == null) {
            Log.d("MinionMipRobot", "handleReceivedMipCommand - RobotCommand is nil");
        }
    }
}
