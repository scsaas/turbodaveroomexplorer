package com.wowwee.bluetoothrobotcontrollib.minionmip.sdk;

public class MinionMipCommandValues {
    public static final int MINIONMIP_BLUETOOTH_PRODUCT_DFU_ID = 69;
    public static final int MINIONMIP_BLUETOOTH_PRODUCT_ID = 68;
    public static Byte kMinionMipSoundFile_ANGRY_STUART_DIAL_042_GROWL_LO_SI_POLATA = Byte.valueOf((byte) 12);
    public static Byte kMinionMipSoundFile_ANGRY_STUART_DIAL_043_GROWLING_THEN_LAUGHING = Byte.valueOf((byte) 13);
    public static Byte kMinionMipSoundFile_ANGRY_STUART_DIAL_075_MACURISTO_BAAH_CUSSING = Byte.valueOf((byte) 14);
    public static Byte kMinionMipSoundFile_ANGRY_STUART_SCREAMS_016_WHAAA_BLUMOCK = Byte.valueOf((byte) 15);
    public static Byte kMinionMipSoundFile_HAPPY_DMF_GIGGLES = Byte.valueOf((byte) 16);
    public static Byte kMinionMipSoundFile_HAPPY_DMF_MINION_HEH_HEH_BELLO = Byte.valueOf((byte) 17);
    public static Byte kMinionMipSoundFile_HAPPY_DMF_MINION_LAUGHING_HYSTERICALLY_DM2 = Byte.valueOf((byte) 18);
    public static Byte kMinionMipSoundFile_HAPPY_DMF_MINION_LAUGHS_DM2 = Byte.valueOf((byte) 19);
    /* renamed from: kMinionMipSoundFile_HAPPY_DMF_MINION_REACTS_TO_GOLF_BALL_BABBLE_DM2 */
    public static Byte f15xaa713e5f = Byte.valueOf((byte) 20);
    public static Byte kMinionMipSoundFile_HAPPY_DMF_MINION_RELAXES_DM2 = Byte.valueOf((byte) 21);
    public static Byte kMinionMipSoundFile_HAPPY_DMF_MINION_WOO_HOO_DM1 = Byte.valueOf((byte) 22);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_DIAL_048_CUMBAYA = Byte.valueOf((byte) 23);
    /* renamed from: kMinionMipSoundFile_HAPPY_KEVIN_DIAL_095_LAUGH_EN_CHE_LE_UN_LA_BOSS */
    public static Byte f16xe7cdd29e = Byte.valueOf((byte) 24);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_DIAL_096_LAUGH_TOCARINA_BOCALOO = Byte.valueOf((byte) 25);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_DIAL_097_LAUGH_HEY_BUDDIES = Byte.valueOf((byte) 26);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_DIAL_116_OH_NO_ME_LE_DO_IT = Byte.valueOf((byte) 27);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_LAUGHING_003_HA_HA_HA_HA_HA_HA = Byte.valueOf((byte) 28);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_LAUGHING_008_LA_BUGEE = Byte.valueOf((byte) 29);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_LAUGHING_013_STIFLED_AT_FIRST = Byte.valueOf((byte) 30);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_REACTION_042_WOOO_HO_HO = Byte.valueOf((byte) 31);
    public static Byte kMinionMipSoundFile_HAPPY_KEVIN_REACTION_045_YES_HA_HA = Byte.valueOf((byte) 32);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_DIAL_007_AH_HA_HA_PATEL = Byte.valueOf((byte) 33);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_DIAL_027_BRAVO_BRAVO = Byte.valueOf((byte) 34);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_DIAL_046_HE_HA_HA_WOO_HOO = Byte.valueOf((byte) 35);
    /* renamed from: kMinionMipSoundFile_HAPPY_STUART_DIAL_120_WOO_HOO_LA_FRITA_PA_TA_KEYS */
    public static Byte f17x55615163 = Byte.valueOf((byte) 36);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_LAUGHING_003_MOCKING = Byte.valueOf((byte) 37);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_LAUGHING_006_AH_HA_HA = Byte.valueOf((byte) 38);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_LAUGHING_013_HA_HA_HA_DIALGOUE = Byte.valueOf((byte) 39);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_LAUGHING_015_LONG = Byte.valueOf((byte) 40);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_LAUGHING_019_THEN_TRAILS_OFF = Byte.valueOf((byte) 41);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_REACTION_002_OH_HA_HA_HA = Byte.valueOf((byte) 42);
    public static Byte kMinionMipSoundFile_HAPPY_STUART_REACTION_012_HA_HA_HA_HA_YEAA = Byte.valueOf((byte) 43);
    /* renamed from: kMinionMipSoundFile_HAPPY_STUART_REACTION_043_YEAHHHH_WOO_HOO_HOO */
    public static Byte f18x4038dc3 = Byte.valueOf((byte) 44);
    public static Byte kMinionMipSoundFile_JOKES_DMF_MINION_IDIOT_LAUGHS_DM1 = Byte.valueOf((byte) 45);
    /* renamed from: kMinionMipSoundFile_JOKES_DMF_MINION_MAKING_KISSING_SOUNDS_DM1_SHORT */
    public static Byte f19x55e0fa84 = Byte.valueOf((byte) 46);
    public static Byte kMinionMipSoundFile_JOKES_DMF_PAPADUM_EH = Byte.valueOf((byte) 47);
    public static Byte kMinionMipSoundFile_JOKES_DMF_UH_HAH_HAH_BLOWS_AIR = Byte.valueOf((byte) 48);
    public static Byte kMinionMipSoundFile_JOKES_STUART_EFFORTS_008_DIAL_LE_BUTT = Byte.valueOf((byte) 49);
    public static Byte kMinionMipSoundFile_NEW_CHARACTER_FART_01 = Byte.valueOf((byte) -113);
    public static Byte kMinionMipSoundFile_NEW_CREAKY_FART_02 = Byte.valueOf((byte) -112);
    public static Byte kMinionMipSoundFile_NEW_FART_10 = Byte.valueOf((byte) -120);
    public static Byte kMinionMipSoundFile_NEW_FART_11 = Byte.valueOf((byte) -119);
    public static Byte kMinionMipSoundFile_NEW_FART_19 = Byte.valueOf((byte) -118);
    public static Byte kMinionMipSoundFile_NEW_FART_2 = Byte.valueOf((byte) -122);
    public static Byte kMinionMipSoundFile_NEW_FART_21 = Byte.valueOf((byte) -117);
    public static Byte kMinionMipSoundFile_NEW_FART_9 = Byte.valueOf((byte) -121);
    public static Byte kMinionMipSoundFile_NEW_FART_SQUEAK_4 = Byte.valueOf((byte) -116);
    public static Byte kMinionMipSoundFile_NEW_FART_WET = Byte.valueOf((byte) -115);
    public static Byte kMinionMipSoundFile_NEW_SING_DM3_MAJOR_GENERAL_SHORT = Byte.valueOf((byte) -114);
    public static Byte kMinionMipSoundFile_NO0ZERO = Byte.valueOf((byte) 1);
    public static Byte kMinionMipSoundFile_NO1ONE = Byte.valueOf((byte) 2);
    public static Byte kMinionMipSoundFile_NO2TWO = Byte.valueOf((byte) 3);
    public static Byte kMinionMipSoundFile_NO3THREE = Byte.valueOf((byte) 4);
    public static Byte kMinionMipSoundFile_NO4FOUR = Byte.valueOf((byte) 5);
    public static Byte kMinionMipSoundFile_NO5FIVE = Byte.valueOf((byte) 6);
    public static Byte kMinionMipSoundFile_NO6SIX = Byte.valueOf((byte) 7);
    public static Byte kMinionMipSoundFile_NO7SEVEN = Byte.valueOf((byte) 8);
    public static Byte kMinionMipSoundFile_NO8EIGHT = Byte.valueOf((byte) 9);
    public static Byte kMinionMipSoundFile_NO9NIGHT = Byte.valueOf((byte) 10);
    public static Byte kMinionMipSoundFile_NOTEN = Byte.valueOf((byte) 11);
    public static Byte kMinionMipSoundFile_ONEKHZ_500MS_8K16BIT = Byte.valueOf((byte) 0);
    public static Byte kMinionMipSoundFile_SAD_DMF_MINION_INJECTED_DM2 = Byte.valueOf((byte) 50);
    public static Byte kMinionMipSoundFile_SAD_DMF_MINION_RELAXED_SIGH_DM2 = Byte.valueOf((byte) 51);
    public static Byte kMinionMipSoundFile_SAD_STUART_DIAL_022_BLUMMOCK = Byte.valueOf((byte) 52);
    public static Byte kMinionMipSoundFile_SAD_STUART_DIAL_033_DEPRESSED_RAMBLING = Byte.valueOf((byte) 53);
    public static Byte kMinionMipSoundFile_SAD_STUART_DIAL_078_MOCALE = Byte.valueOf((byte) 54);
    public static Byte kMinionMipSoundFile_SAD_STUART_DIAL_104_PROFITORAL_MUMBLING = Byte.valueOf((byte) 55);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_ALMOST_FALLS_DM1 = Byte.valueOf((byte) 56);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_CRUSHED_DM1 = Byte.valueOf((byte) 57);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_CRYING_DM2 = Byte.valueOf((byte) 58);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_FALLING_DM2 = Byte.valueOf((byte) 59);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_FIGHTING_SOUNDS_DM2 = Byte.valueOf((byte) 60);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_IN_VIBRATING_CHAIR_DM1 = Byte.valueOf((byte) 61);
    /* renamed from: kMinionMipSoundFile_SCREAMS_DMF_MINION_LONG_SCREAM_WHEN_WHIPED_DM2 */
    public static Byte f20xd560b714 = Byte.valueOf((byte) 62);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_SCREAMS_DM2 = Byte.valueOf((byte) 63);
    /* renamed from: kMinionMipSoundFile_SCREAMS_DMF_MINION_SCREAMS_INTO_TRASH_CAN_DM2 */
    public static Byte f21xa5ec5734 = Byte.valueOf((byte) 64);
    /* renamed from: kMinionMipSoundFile_SCREAMS_DMF_MINION_SCREAMS_SEING_GRU_CAPTURED_DM2 */
    public static Byte f22xbe6d09ac = Byte.valueOf((byte) 65);
    /* renamed from: kMinionMipSoundFile_SCREAMS_DMF_MINION_SCREAMS_SHORT_DM2_POS_TEMP */
    public static Byte f23x88612df8 = Byte.valueOf((byte) 66);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_MINION_STUGGLES_WITH_DUCK_DM2 = Byte.valueOf((byte) 67);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_STOP_STOP_HEY_HO = Byte.valueOf((byte) 68);
    public static Byte kMinionMipSoundFile_SCREAMS_DMF_SUCKED_INTO_OCEAN = Byte.valueOf((byte) 69);
    public static Byte kMinionMipSoundFile_SCREAMS_STUART_EFFORTS_EH_OH_AHHHH = Byte.valueOf((byte) 70);
    public static Byte kMinionMipSoundFile_SING_DMF_BEE_DO_CLEAN = Byte.valueOf((byte) 71);
    public static Byte kMinionMipSoundFile_SING_DMF_MINION_HUMMING_1_DM1 = Byte.valueOf((byte) 72);
    public static Byte kMinionMipSoundFile_SING_DMF_MINION_HUMMING_2_DM1 = Byte.valueOf((byte) 73);
    public static Byte kMinionMipSoundFile_SING_DMF_MINION_SINGING_2_MAID = Byte.valueOf((byte) 74);
    public static Byte kMinionMipSoundFile_SING_DMF_MINION_SINGING_FRUIT = Byte.valueOf((byte) 75);
    public static Byte kMinionMipSoundFile_SING_DMF_MINION_SINGS_GO_TO_SLEEP_TEMP_DM2 = Byte.valueOf((byte) 76);
    public static Byte kMinionMipSoundFile_SING_DMF_MINION_SINIGING_BELLA = Byte.valueOf((byte) 77);
    public static Byte kMinionMipSoundFile_SING_DMF_MINION_TRIBAL_DANCE_SONG_DM2 = Byte.valueOf((byte) 78);
    public static Byte kMinionMipSoundFile_SING_KEVIN_SINGING_INDIAN_CHANT = Byte.valueOf((byte) 79);
    public static Byte kMinionMipSoundFile_SING_KEVIN_SINGING_KUKA_CHOW_KUKA_CHOW = Byte.valueOf((byte) 80);
    public static Byte kMinionMipSoundFile_SING_KEVIN_SINGING_OPERATIC = Byte.valueOf((byte) 81);
    public static Byte kMinionMipSoundFile_SING_STUART_SINGING_LA_LA_LA_LA = Byte.valueOf((byte) 82);
    public static Byte kMinionMipSoundFile_SPEECH_L_DMF_MINION_EXERCISING_DM1 = Byte.valueOf((byte) 83);
    /* renamed from: kMinionMipSoundFile_SPEECH_L_DMF_MINION_GETTING_SOMEONES_ATTENTION_DM1 */
    public static Byte f24xebcf0b82 = Byte.valueOf((byte) 84);
    public static Byte kMinionMipSoundFile_SPEECH_L_DMF_MINION_JABBER_DM2 = Byte.valueOf((byte) 85);
    /* renamed from: kMinionMipSoundFile_SPEECH_L_DMF_MINION_MAKING_SHADOW_PUPPET_IAMGRU_DM1 */
    public static Byte f25xfce84fe0 = Byte.valueOf((byte) 86);
    public static Byte kMinionMipSoundFile_SPEECH_L_DMF_MINION_MAKING_TOAST_DM2 = Byte.valueOf((byte) 87);
    public static Byte kMinionMipSoundFile_SPEECH_L_DMF_MINION_SWINGING_CLUB_DM2 = Byte.valueOf((byte) 88);
    public static Byte kMinionMipSoundFile_SPEECH_L_DMF_PUPOI_1_TOY = Byte.valueOf((byte) 89);
    /* renamed from: kMinionMipSoundFile_SPEECH_L_STUART_DIAL_041_FLIRTING_WITH_FIRE_HYDRANT */
    public static Byte f26x6f906ed0 = Byte.valueOf((byte) 90);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_021_BELLO = Byte.valueOf((byte) 91);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_048_HELLO = Byte.valueOf((byte) 92);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_063_HUH = Byte.valueOf((byte) 93);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_073_LAUGH_THANK_YOU = Byte.valueOf((byte) 94);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_091_OK_OK = Byte.valueOf((byte) 95);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_092_OK_POLATACA = Byte.valueOf((byte) 96);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_094_OKAY = Byte.valueOf((byte) 97);
    public static Byte kMinionMipSoundFile_SPEECH_S_DIAL_112_SI_SI_LA_MATACO = Byte.valueOf((byte) 98);
    /* renamed from: kMinionMipSoundFile_SPEECH_S_DIAL_115_THANK_YOU_LAPORTA_THANK_YOU */
    public static Byte f27x633bce6a = Byte.valueOf((byte) 99);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_AHH_GOOQUAH = Byte.valueOf((byte) 100);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_BACAFOOT = Byte.valueOf((byte) 101);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_BAPPLE = Byte.valueOf((byte) 102);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_GEKO = Byte.valueOf((byte) 103);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_GROWL_FOR_LUCY = Byte.valueOf((byte) 104);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_21_GUN_SALUTE_DM2 = Byte.valueOf((byte) 105);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_AHHH_NO_DM2 = Byte.valueOf((byte) 106);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_AWWW_DM2 = Byte.valueOf((byte) 107);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_BABBLE_READY_DM2 = Byte.valueOf((byte) 108);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_EHHH_PPPPP_DM1 = Byte.valueOf((byte) 109);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_GOLF_KARATE_SOUND_DM2 = Byte.valueOf((byte) 110);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_HELLO = Byte.valueOf((byte) 111);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_MAKES_DUCK_SOUND_DM2 = Byte.valueOf((byte) 112);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_MM_HMM_YES_DM2 = Byte.valueOf((byte) 113);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_OKAY_OKAY_DM1 = Byte.valueOf((byte) 114);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_RELAXES_AND_WAVES_DM2 = Byte.valueOf((byte) 115);
    /* renamed from: kMinionMipSoundFile_SPEECH_S_DMF_MINION_TALKING_OFFERING_PAPOY_DM1 */
    public static Byte f28x70bc4e47 = Byte.valueOf((byte) 116);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MINION_VOILA_DM2_TEMP_DM2 = Byte.valueOf((byte) 117);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_MOCA = Byte.valueOf((byte) 118);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_PUTT_PUTT = Byte.valueOf((byte) 119);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_UH = Byte.valueOf((byte) 120);
    public static Byte kMinionMipSoundFile_SPEECH_S_DMF_UHH = Byte.valueOf((byte) 121);
    public static Byte kMinionMipSoundFile_SPEECH_S_KEVIN_DIAL_018_BANANA = Byte.valueOf((byte) 122);
    public static Byte kMinionMipSoundFile_SPEECH_S_KEVIN_DIAL_063_HA_BELLO = Byte.valueOf((byte) 123);
    public static Byte kMinionMipSoundFile_SPEECH_S_KEVIN_DIAL_125_OOH_BATA_ME_HA_HA = Byte.valueOf((byte) 124);
    /* renamed from: kMinionMipSoundFile_SPEECH_S_KEVIN_DIAL_181_YOU_GO_THAT_WAY_I_GO_THIS_WAY */
    public static Byte f29x12b95c1a = Byte.valueOf((byte) 125);
    public static Byte kMinionMipSoundFile_SPEECH_S_KEVIN_SINGING_001_BIG_BOSS_BIG_BOSS = Byte.valueOf((byte) 126);
    public static Byte kMinionMipSoundFile_SPEECH_S_REACTION_018_HUH = Byte.valueOf(Byte.MAX_VALUE);
    public static Byte kMinionMipSoundFile_SPEECH_S_REACTION_042_YEAH_YEAH_OOH_OOH = Byte.valueOf(Byte.MIN_VALUE);
    /* renamed from: kMinionMipSoundFile_SPEECH_S_STUART_DIAL_005_AH_MANATONITO_PORA_MUR */
    public static Byte f30x4bad2093 = Byte.valueOf((byte) -127);
    /* renamed from: kMinionMipSoundFile_SPEECH_S_STUART_DIAL_008_AH_MI_MALA_TIKA_A_TU_NI */
    public static Byte f31x1f626daa = Byte.valueOf((byte) -126);
    public static Byte kMinionMipSoundFile_SPEECH_S_STUART_DIAL_011_AH_SI = Byte.valueOf((byte) -125);
    /* renamed from: kMinionMipSoundFile_SPEECH_S_STUART_DIAL_083_NO_LE_ME_DO_IT_SPETA */
    public static Byte f32xfd891a6b = Byte.valueOf((byte) -124);
    /* renamed from: kMinionMipSoundFile_SPEECH_S_STUART_DIAL_101_PONYA_BURRITO_MACARA_DOH */
    public static Byte f33x161f7103 = Byte.valueOf((byte) -123);
    public static byte kMinionPlayBodycon = (byte) 118;
}
