package com.wowwee.bluetoothrobotcontrollib;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.nuvotonBootloaderMode;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.nuvotonFirmwareCompleteStatus;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.nuvotonFirmwareStatus;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues.kMipHeadLedValue;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues.kMipPingResponse;
import com.wowwee.bluetoothrobotcontrollib.MipCommandValues.kMipResetMcu;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService;
import com.wowwee.bluetoothrobotcontrollib.services.BRDeviceInformationService;
import com.wowwee.bluetoothrobotcontrollib.services.BRModuleParametersService;
import com.wowwee.bluetoothrobotcontrollib.services.BRRSSIReportService;
import com.wowwee.bluetoothrobotcontrollib.services.BRReceiveDataService;
import com.wowwee.bluetoothrobotcontrollib.services.BRSendDataService;
import com.wowwee.bluetoothrobotcontrollib.util.AdRecord;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MipRobot extends BluetoothRobotPrivate {
    public static int MipType_CoderMip = 1;
    public static int MipType_MinionMip = 2;
    public static int MipType_Mip = 0;
    public int batteryLevel;
    public kMipPingResponse bootMode;
    protected MipRobotInterface callbackInterface = null;
    public boolean disableReceivedCommandProcessing = false;
    private boolean isMipDevice = false;
    public Date mipFirmwareVersionDate;
    public int mipFirmwareVersionId;
    public int mipHardwareVersion;
    public int mipRobotType = MipType_Mip;
    public int mipVoiceFirmwareVersion;
    public int mipVolume;
    public byte position;
    private byte toyActivationStatus;

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.MipRobot$1 */
    class C00131 implements Runnable {
        C00131() {
        }

        public void run() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MipRobot.this.getBluetoothModuleSoftwareVersion();
            MipRobot.this.getBluetoothModuleSystemID();
            MipRobot.this.readMipStatus();
            MipRobot.this.getMipActivationStatus();
            MipRobot.this.readMipHardwareVersion();
        }
    }

    public interface MipRobotInterface {
        void deviceNotMip();

        void mipDeviceDisconnected(MipRobot mipRobot);

        void mipDeviceReady(MipRobot mipRobot);

        boolean mipRobotBluetoothDidProcessedReceiveRobotCommand(MipRobot mipRobot, RobotCommand robotCommand);

        void mipRobotDidDetectOtherMip(MipRobot mipRobot, int i);

        void mipRobotDidReceiveBatteryLevelReading(MipRobot mipRobot, int i);

        void mipRobotDidReceiveChestRGBLedFlash(MipRobot mipRobot, int i, int i2, int i3, int i4, int i5);

        void mipRobotDidReceiveChestRGBLedUpdate(MipRobot mipRobot, int i, int i2, int i3, int i4);

        void mipRobotDidReceiveClapDetectionStatusIsEnabled(MipRobot mipRobot, boolean z, long j);

        void mipRobotDidReceiveGameMode(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveGesture(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveGestureMode(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveHardwareVersion(MipRobot mipRobot, int i, int i2);

        void mipRobotDidReceiveHeadLedUpdate(MipRobot mipRobot, kMipHeadLedValue kmipheadledvalue, kMipHeadLedValue kmipheadledvalue2, kMipHeadLedValue kmipheadledvalue3, kMipHeadLedValue kmipheadledvalue4);

        void mipRobotDidReceiveIRCommand(MipRobot mipRobot, ArrayList<Byte> arrayList, int i);

        void mipRobotDidReceiveIRRemoteEnabled(MipRobot mipRobot, boolean z);

        void mipRobotDidReceiveNumberOfClaps(MipRobot mipRobot, int i);

        void mipRobotDidReceiveOdometerReading(MipRobot mipRobot, long j);

        void mipRobotDidReceiveOtherMipDetectionModeOn(MipRobot mipRobot, boolean z);

        void mipRobotDidReceivePosition(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveRadarMode(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveRadarResponse(MipRobot mipRobot, byte b);

        void mipRobotDidReceiveShake(MipRobot mipRobot);

        void mipRobotDidReceiveSoftwareVersion(MipRobot mipRobot, Date date, int i);

        void mipRobotDidReceiveToyActivationStatus(MipRobot mipRobot, boolean z, boolean z2, boolean z3, boolean z4);

        void mipRobotDidReceiveUserData(MipRobot mipRobot, ArrayList<Byte> arrayList);

        void mipRobotDidReceiveVolumeLevel(MipRobot mipRobot, int i);

        void mipRobotDidReceiveWeightReading(MipRobot mipRobot, byte b, boolean z);

        void mipRobotFirmwareCompleteStatus(MipRobot mipRobot, nuvotonFirmwareCompleteStatus nuvotonfirmwarecompletestatus);

        void mipRobotFirmwareDataStatus(MipRobot mipRobot, nuvotonFirmwareStatus nuvotonfirmwarestatus);

        void mipRobotFirmwareSent(MipRobot mipRobot, int i);

        void mipRobotFirmwareToChip(MipRobot mipRobot, int i);

        void mipRobotIsCurrentlyInBootloader(MipRobot mipRobot, boolean z);

        void mipRobotNuvotonChipstatus(MipRobot mipRobot, nuvotonBootloaderMode nuvotonbootloadermode);
    }

    public MipRobot(BluetoothDevice pBluetoothDevice, List<AdRecord> pScanRecords, BluetoothLeService pBluetoothLeService) {
        super(pBluetoothDevice, pScanRecords, pBluetoothLeService);
    }

    public void setCallbackInterface(MipRobotInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public HashMap<String, BRBaseService> buildPeripheralServiceDict(BluetoothLeService pBluetoothLeService) {
        HashMap<String, BRBaseService> serviceDict = super.buildPeripheralServiceDict(pBluetoothLeService);
        serviceDict.put(BluetoothRobotConstants.kMipRSSIReportServiceUUID, new BRRSSIReportService(pBluetoothLeService, this, this.mBluetoothDevice.getAddress()));
        serviceDict.put(BluetoothRobotConstantsBase.kMipSendDataServiceUUID, new BRSendDataService(pBluetoothLeService, this.mBluetoothDevice.getAddress()));
        serviceDict.put(BluetoothRobotConstantsBase.kMipReceiveDataServiceUUID, new BRReceiveDataService(pBluetoothLeService, this, this.mBluetoothDevice.getAddress()));
        serviceDict.put(BluetoothRobotConstants.kMipModuleParametersServiceUUID, new BRModuleParametersService(pBluetoothLeService, this, this.mBluetoothDevice.getAddress()));
        serviceDict.put(BluetoothRobotConstants.kMipDeviceInformationServiceUUID, new BRDeviceInformationService(pBluetoothLeService, this, this.mBluetoothDevice.getAddress()));
        return serviceDict;
    }

    public void peripheralDidConnect() {
        super.peripheralDidConnect();
        MipRobotFinder.getInstance().mipRobotDidConnect(this);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void peripheralDidDisconnect() {
        if (this.kBluetoothRobotState == 2) {
            super.peripheralDidDisconnect();
            return;
        }
        super.peripheralDidDisconnect();
        MipRobotFinder.getInstance().mipRobotDidDisconnect(this);
        if (this.callbackInterface != null) {
            this.callbackInterface.mipDeviceDisconnected(this);
        }
    }

    public void peripheralDidBecomeReady() {
        Log.d("MipRobot", " peripheralDidBecomeReady() ");
        super.peripheralDidBecomeReady();
        checkDFU();
        MipRobotFinder.getInstance().stopScanForMips();
        this.isMipDevice = false;
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        readMipFirmwareVersion();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        readMipStatus();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e22) {
            e22.printStackTrace();
        }
        getMipCurrentGameMode();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e222) {
            e222.printStackTrace();
        }
        getMipVolumeLevel();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e2222) {
            e2222.printStackTrace();
        }
        getBluetoothModuleSoftwareVersion();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e22222) {
            e22222.printStackTrace();
        }
        getBluetoothModuleSystemID();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e222222) {
            e222222.printStackTrace();
        }
        getMipActivationStatus();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e2222222) {
            e2222222.printStackTrace();
        }
        readMipHardwareVersion();
        if (this.callbackInterface != null) {
            this.callbackInterface.mipDeviceReady(this);
        } else {
            Log.e("MipRobot", BuildConfig.FLAVOR + hashCode() + " peripheralDidBecomeReady callbackInterface = null");
        }
    }

    private void initDefaultValues() {
        this.position = MipCommandValues.kMipPositionOnBack;
    }

    public void readMipStatus() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetStatus));
    }

    public void readMipHardwareVersion() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetHardwareVersion));
    }

    public void readMipFirmwareVersion() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetSoftwareVersion));
    }

    public void getMipVolumeLevel() {
        Log.d("getMipVolumeLevel", "start get");
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetVolumeLevel));
    }

    public void getMipActivationStatus() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetToyActivatedStatus));
    }

    public void getMipCurrentGameMode() {
        Log.d("MipRobot", "getMipGameMode()");
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipCurrentGameMode));
    }

    public void readMipSensorWeightLevel() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetWeightLevel));
    }

    public void resetMipProductActivated() {
        this.toyActivationStatus = MipCommandValues.kMipActivation_FactoryDefault;
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetToyActivatedStatus, this.toyActivationStatus));
    }

    public void setMipProductActivated() {
        this.toyActivationStatus = (byte) (this.toyActivationStatus | MipCommandValues.kMipActivation_Activate);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetToyActivatedStatus, this.toyActivationStatus));
    }

    public void setMipActivationProductUploaded() {
        this.toyActivationStatus = (byte) (this.toyActivationStatus | MipCommandValues.kMipActivation_ActivationSentToFlurry);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetToyActivatedStatus, this.toyActivationStatus));
    }

    public void setMipActivationHackerUploaded() {
        this.toyActivationStatus = (byte) (this.toyActivationStatus | MipCommandValues.kMipActivation_HackerUartUsedSentToFlurry);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetToyActivatedStatus, this.toyActivationStatus));
    }

    public void setMipVolumeLevel(byte volumeLevel) {
        this.mipVolume = Integer.parseInt(Byte.toString(volumeLevel));
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetVolumeLevel, volumeLevel));
    }

    public void setMipClapEnable(boolean enabled) {
        if (enabled) {
            sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetClapDetectionStatus, (byte) 1));
        } else {
            sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetClapDetectionStatus, (byte) 0));
        }
    }

    public void getMipClapStatus() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipClapDetectionStatus));
    }

    public void setMipClapDelayTime(int milliseconds) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetClapDetectTiming, (byte) ((milliseconds >> 8) & 255), (byte) (milliseconds & 255)));
    }

    public void setMipGestureMode(byte gestureMode) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetRadarModeOrRadarResponse, gestureMode));
    }

    public void getMipGestureMode() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetRadarMode));
    }

    public void setMipOtherMipDetectionModeOnUsingMipId(byte mipId, byte powerDistanceCm) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetDetectionModeOrDetectionResponse, mipId, powerDistanceCm));
    }

    public void setMipOtherMipDetectionModeOff() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetDetectionModeOrDetectionResponse, MipCommandValues.kMipDetectionMode_Off, (byte) 0));
    }

    public void getMipOtherMipDetectionModeStatus() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipGetDetectionMode));
    }

    public void mipRebootWriteFlash(boolean writeFlash, boolean writeIO) {
        if (!writeFlash || !writeIO) {
            if (writeFlash && !writeIO) {
                sendRobotCommand(RobotCommand.create(MipCommandValues.kMipReboot, kMipResetMcu.kMipResetMcu_ResetAndForceBootloader.getValue()));
            } else if (writeFlash || !writeIO) {
                sendRobotCommand(RobotCommand.create(MipCommandValues.kMipReboot, kMipResetMcu.kMipResetMcu_NormalReset.getValue()));
            }
        }
    }

    public void mipDrive(float[] vector) {
        boolean driveForward;
        boolean turnRight = true;
        int xValue = Math.max(Math.min(Math.round(vector[0] * 32.0f), 32), -32);
        int yValue = Math.max(Math.min(Math.round(vector[1] * 32.0f), 32), -32);
        if (yValue > 0) {
            driveForward = true;
        } else {
            driveForward = false;
        }
        if (xValue <= 0) {
            turnRight = false;
        }
        xValue = Math.abs(xValue);
        yValue = Math.abs(yValue);
        byte driveValue = (byte) 0;
        if (yValue != 0) {
            driveValue = (byte) (driveForward ? MipCommandValues.kMipDriveCont_FW_Speed1 + yValue : MipCommandValues.kMipDriveCont_BW_Speed1 + yValue);
        }
        byte turnValue = (byte) 0;
        if (xValue != 0) {
            turnValue = (byte) (turnRight ? MipCommandValues.kMipDriveCont_Right_Speed1 + xValue : MipCommandValues.kMipDriveCont_Left_Speed1 + xValue);
        }
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDrive_Continuous, driveValue, turnValue));
    }

    public void mipDriveForwardForMilliseconds(int msecs, int speed) {
        byte rate = (byte) Math.min(speed, 30);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDriveForwardWithTime, rate, (byte) Math.min(msecs / 7, 255)));
    }

    public void mipDriveBackwardForMilliseconds(int msecs, int speed) {
        byte rate = (byte) Math.min(speed, 30);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDriveBackwardWithTime, rate, (byte) Math.min(msecs / 7, 255)));
    }

    public void mipPunchLeftWithSpeed(int speed) {
        mipTurnRightByDegrees(90, speed);
    }

    public void mipPunchRightWithSpeed(int speed) {
        mipTurnLeftByDegrees(90, speed);
    }

    public void mipTurnLeftByDegrees(int degrees, int speed) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kTurnLeftByAngle, (byte) Math.min(degrees / 4, 255), (byte) Math.min(speed, 24)));
    }

    public void mipTurnRightByDegrees(int degrees, int speed) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kTurnRightByAngle, (byte) Math.min(degrees / 4, 255), (byte) Math.min(speed, 24)));
    }

    public void mipDriveDistanceByCm(int distanceInCm) {
        mipDriveDistanceByCm(distanceInCm, 0);
    }

    public void mipDriveDistanceByCm(int distanceInCm, int angleInDegrees) {
        byte direction = MipCommandValues.kMipDrive_DriveDirectionForward;
        if (distanceInCm < 0) {
            direction = MipCommandValues.kMipDrive_DriveDirectionBackward;
        }
        distanceInCm = Math.abs(distanceInCm);
        byte angleDirection = MipCommandValues.kMipDrive_TurnDirectionClockwise;
        if (angleInDegrees < 0) {
            angleDirection = MipCommandValues.kMipDrive_TurnDirectionAnticlockwise;
        }
        angleInDegrees = Math.abs(angleInDegrees);
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipDrive_FixedDistance, direction, (byte) distanceInCm, angleDirection, (byte) ((angleInDegrees >> 8) & 255), (byte) (angleInDegrees & 255)));
    }

    public void mipFalloverWithStyle(byte position) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipShouldFallover, position));
    }

    public void mipPing() {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipCheckBootmode));
    }

    public void setMipHeadLeds(kMipHeadLedValue led1, kMipHeadLedValue led2, kMipHeadLedValue led3, kMipHeadLedValue led4) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetHeadLed, led1.getValue(), led2.getValue(), led3.getValue(), led4.getValue()));
    }

    public void setMipChestRGBLedFlashingWithColor(byte red, byte green, byte blue, byte timeOn, byte timeOff) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipFlashChestRGBLed, red, green, blue, timeOn, timeOff));
    }

    public void setMipChestRGBLedWithColor(byte red, byte green, byte blue, byte fadeIn) {
        sendRobotCommand(RobotCommand.create(MipCommandValues.kMipSetChestRGBLed, red, green, blue, fadeIn));
    }

    public void mipTransmitIRGameDataWithGameType(byte gameType, byte mipId, short gameData, byte powerDistanceCm) {
        mipTransmitIRMipCommandOfBitLength((byte) 32, gameType, mipId, (byte) (gameData >> 8), (byte) (gameData & 255), powerDistanceCm);
    }

    public void mipTransmitIRMipCommandOfBitLength(byte bitLength, byte byte1, byte byte2, byte byte3, byte byte4, byte distanceCm) {
        byte b;
        byte b2 = (byte) 0;
        if (bitLength < (byte) 1 || bitLength > (byte) 32) {
            Log.e("MipRobot", "Send IR bitLength must be 1-32 bits");
        }
        if (distanceCm < (byte) 1 || distanceCm > (byte) 120) {
            Log.e("MipRobot", "Send IR distance must be in the range of 1-120 (1-300cm)");
        }
        byte b3 = MipCommandValues.kMipTransmitIRCommand;
        if (bitLength >= (byte) 25) {
            b = byte4;
        } else {
            b = (byte) 0;
        }
        byte b4 = bitLength >= (byte) 17 ? byte3 : (byte) 0;
        if (bitLength >= (byte) 9) {
            b2 = byte2;
        }
        sendRobotCommand(RobotCommand.create(b3, b, b4, b2, byte1, bitLength, distanceCm));
    }

    public void mipPlaySound(MipRobotSound sound) {
        sendRobotCommand(sound.robotCommand());
    }

    public void handleReceivedMipCommand(RobotCommand robotCommand) {
        if (this.callbackInterface == null || !this.callbackInterface.mipRobotBluetoothDidProcessedReceiveRobotCommand(this, robotCommand) || !this.disableReceivedCommandProcessing) {
            if (robotCommand == null) {
                Log.d("MipRobot", "handleReceivedMipCommand - RobotCommand is nil");
                return;
            }
            byte commandValue = robotCommand.getCmdByte();
            ArrayList<Byte> dataArray = robotCommand.getDataArray();
            if (commandValue == MipCommandValues.kMipGetGameMode) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetGameMode command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveGameMode(this, ((Byte) dataArray.get(0)).byteValue());
                }
            } else if (commandValue == MipCommandValues.kMipGetStatus) {
                if (dataArray.size() == 2) {
                    this.batteryLevel = ((Byte) dataArray.get(0)).byteValue();
                    Log.i("MipRobot", "batteryLevel " + this.batteryLevel);
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveBatteryLevelReading(this, this.batteryLevel);
                    }
                    byte newPosition = ((Byte) dataArray.get(1)).byteValue();
                    if (this.position != newPosition) {
                        this.position = newPosition;
                    }
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceivePosition(this, this.position);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetStatus command");
            } else if (commandValue == MipCommandValues.kMipGetHardwareVersion) {
                if (dataArray.size() == 2) {
                    int voiceFirmwareVersion = ((Byte) dataArray.get(0)).byteValue();
                    if (voiceFirmwareVersion >= 6 && voiceFirmwareVersion < 14) {
                        this.mipVoiceFirmwareVersion = 1;
                    } else if (voiceFirmwareVersion >= 14 && voiceFirmwareVersion < 22) {
                        this.mipVoiceFirmwareVersion = 2;
                    } else if (voiceFirmwareVersion >= 22 && voiceFirmwareVersion < 30) {
                        this.mipVoiceFirmwareVersion = 3;
                    } else if (voiceFirmwareVersion >= 30 && voiceFirmwareVersion < 38) {
                        this.mipVoiceFirmwareVersion = 4;
                    } else if (voiceFirmwareVersion >= 38 && voiceFirmwareVersion < 46) {
                        this.mipVoiceFirmwareVersion = 5;
                    } else if (voiceFirmwareVersion >= 46 && voiceFirmwareVersion < 54) {
                        this.mipVoiceFirmwareVersion = 6;
                    } else if (voiceFirmwareVersion >= 54 && voiceFirmwareVersion < 62) {
                        this.mipVoiceFirmwareVersion = 7;
                    } else if (voiceFirmwareVersion >= 62 && voiceFirmwareVersion < 70) {
                        this.mipVoiceFirmwareVersion = 8;
                    } else if (voiceFirmwareVersion >= 70 && voiceFirmwareVersion < 78) {
                        this.mipVoiceFirmwareVersion = 9;
                    } else if (voiceFirmwareVersion >= 78 && voiceFirmwareVersion < 86) {
                        this.mipVoiceFirmwareVersion = 10;
                    } else if (voiceFirmwareVersion >= 86 && voiceFirmwareVersion < 94) {
                        this.mipVoiceFirmwareVersion = 11;
                    } else if (voiceFirmwareVersion < 94 || voiceFirmwareVersion >= 102) {
                        this.mipVoiceFirmwareVersion = 0;
                    } else {
                        this.mipVoiceFirmwareVersion = 12;
                    }
                    this.mipHardwareVersion = ((Byte) dataArray.get(1)).byteValue();
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveHardwareVersion(this, this.mipHardwareVersion, this.mipVoiceFirmwareVersion);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetHardwareVersion command");
            } else if (commandValue == MipCommandValues.kMipGetSoftwareVersion) {
                Log.d("MipRobot", "MipCommandValues.kMipGetSoftwareVersion");
                if (dataArray.size() == 4) {
                    Calendar cal = Calendar.getInstance();
                    cal.set(((Byte) dataArray.get(0)).byteValue() + 2000, Math.max(0, ((Byte) dataArray.get(1)).byteValue() - 1), ((Byte) dataArray.get(2)).byteValue());
                    this.mipFirmwareVersionId = ((Byte) dataArray.get(3)).byteValue();
                    this.mipFirmwareVersionDate = cal.getTime();
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveSoftwareVersion(this, this.mipFirmwareVersionDate, this.mipFirmwareVersionId);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetSoftwareVersion command: " + robotCommand.description());
            } else if (commandValue == MipCommandValues.kMipGetVolumeLevel) {
                Log.d("getMipVolumeLevel", "return data");
                if (dataArray.size() == 1) {
                    this.mipVolume = ((Byte) dataArray.get(0)).byteValue();
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveVolumeLevel(this, this.mipVolume);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "Data array count incorrect for MipGetVolumeLevel command");
            } else if (commandValue == MipCommandValues.kMipGetIRRemoteEnabledDisabled) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "Data array count incorrect for kMipGetIRRemoteEnabledDisabled command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveIRRemoteEnabled(this, ((Byte) dataArray.get(0)).intValue() != 0);
                }
            } else if (commandValue == MipCommandValues.kMipGetOdometer) {
                if (dataArray.size() == 4) {
                    long value = (long) (((Byte) dataArray.get(3)).intValue() | (((((Byte) dataArray.get(0)).intValue() << 24) | (((Byte) dataArray.get(1)).intValue() << 16)) | (((Byte) dataArray.get(2)).intValue() << 8)));
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveOdometerReading(this, value);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "Data array count incorrect for kMipGetOdometer command");
            } else if (commandValue == MipCommandValues.kMipGetToyActivatedStatus) {
                if (dataArray.size() == 1) {
                    this.toyActivationStatus = ((Byte) dataArray.get(0)).byteValue();
                    if ((this.toyActivationStatus & MipCommandValues.kMipActivation_Activate) == 0) {
                        setMipProductActivated();
                    }
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveToyActivationStatus(this, (this.toyActivationStatus & MipCommandValues.kMipActivation_Activate) != 0, (this.toyActivationStatus & MipCommandValues.kMipActivation_ActivationSentToFlurry) != 0, (this.toyActivationStatus & MipCommandValues.kMipActivation_HackerUartUsed) != 0, (this.toyActivationStatus & MipCommandValues.kMipActivation_HackerUartUsedSentToFlurry) != 0);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetToyActivatedStatus command");
            } else if (commandValue == MipCommandValues.kMipReceiveIRCommand) {
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveIRCommand(this, dataArray, dataArray.size());
                }
            } else if (commandValue == MipCommandValues.kMipGetUserData) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetUserData command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveUserData(this, dataArray);
                }
            } else if (commandValue == MipCommandValues.kMipShakeDetected) {
                if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveShake(this);
                }
            } else if (commandValue == MipCommandValues.kMipGetWeightLevel) {
                if (dataArray.size() == 1) {
                    byte weightData = ((Byte) dataArray.get(0)).byteValue();
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveWeightReading(this, weightData, weightData < (byte) -45);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetWeightLevel command");
            } else if (commandValue == MipCommandValues.kMipGetChestRGBLed) {
                if (dataArray.size() == 4) {
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveChestRGBLedUpdate(this, ((Byte) dataArray.get(0)).intValue(), ((Byte) dataArray.get(1)).intValue(), ((Byte) dataArray.get(2)).intValue(), ((Byte) dataArray.get(3)).intValue());
                    }
                } else if (dataArray.size() != 5) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetChestRGBLed command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveChestRGBLedFlash(this, ((Byte) dataArray.get(0)).intValue(), ((Byte) dataArray.get(1)).intValue(), ((Byte) dataArray.get(2)).intValue(), ((Byte) dataArray.get(3)).intValue(), ((Byte) dataArray.get(4)).intValue());
                }
            } else if (commandValue == MipCommandValues.kMipGetHeadLed) {
                if (dataArray.size() != 4) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetHeadLed command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveHeadLedUpdate(this, kMipHeadLedValue.getParamWithValue(((Byte) dataArray.get(0)).byteValue()), kMipHeadLedValue.getParamWithValue(((Byte) dataArray.get(1)).byteValue()), kMipHeadLedValue.getParamWithValue(((Byte) dataArray.get(2)).byteValue()), kMipHeadLedValue.getParamWithValue(((Byte) dataArray.get(3)).byteValue()));
                }
            } else if (commandValue == MipCommandValues.kMipSetGestureModeOrGestureDetected) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipSetGestureModeOrGestureDetected command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveGesture(this, ((Byte) dataArray.get(0)).byteValue());
                }
            } else if (commandValue == MipCommandValues.kMipGetRadarMode) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetRadarMode command");
                } else if (this.callbackInterface != null) {
                    byte val = ((Byte) dataArray.get(0)).byteValue();
                    if (val == MipCommandValues.kMipRadarMode_On) {
                        this.callbackInterface.mipRobotDidReceiveRadarMode(this, val);
                    } else if (val == MipCommandValues.kMipGestureMode_On) {
                        this.callbackInterface.mipRobotDidReceiveGestureMode(this, val);
                    } else {
                        this.callbackInterface.mipRobotDidReceiveRadarMode(this, val);
                        this.callbackInterface.mipRobotDidReceiveGestureMode(this, val);
                    }
                }
            } else if (commandValue == MipCommandValues.kMipSetRadarModeOrRadarResponse) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipSetRadarModeOrRadarResponse command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveRadarResponse(this, ((Byte) dataArray.get(0)).byteValue());
                }
            } else if (commandValue == MipCommandValues.kMipClapsDetected) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipClapsDetected command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidReceiveNumberOfClaps(this, ((Byte) dataArray.get(0)).intValue());
                }
            } else if (commandValue == MipCommandValues.kMipClapDetectionStatus) {
                if (dataArray.size() == 3) {
                    long value = (long) (((Byte) dataArray.get(2)).intValue() | (((Byte) dataArray.get(1)).intValue() << 8));
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveClapDetectionStatusIsEnabled(this, ((Byte) dataArray.get(0)).byteValue() != (byte) 0, value);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipClapDetectionStatus command");
            } else if (commandValue == MipCommandValues.kMipGetDetectionMode) {
                if (dataArray.size() == 1) {
                    boolean value = ((Byte) dataArray.get(0)).intValue() != 0;
                    if (this.callbackInterface != null) {
                        this.callbackInterface.mipRobotDidReceiveOtherMipDetectionModeOn(this, value);
                        return;
                    }
                    return;
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGetDetectionMode command");
            } else if (commandValue == MipCommandValues.kMipOtherMipDetected) {
                if (dataArray.size() != 1) {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for kMipOtherMipDetected command");
                } else if (this.callbackInterface != null) {
                    this.callbackInterface.mipRobotDidDetectOtherMip(this, ((Byte) dataArray.get(0)).intValue());
                }
            } else if (commandValue == MipCommandValues.kMipCheckBootmode) {
                if (dataArray.size() == 1) {
                    this.bootMode = kMipPingResponse.getParamWithValue(((Byte) dataArray.get(0)).byteValue());
                    if (this.bootMode == kMipPingResponse.kMipPingResponseBootloader) {
                        this.callbackInterface.mipRobotIsCurrentlyInBootloader(this, true);
                    } else {
                        this.callbackInterface.mipRobotIsCurrentlyInBootloader(this, false);
                    }
                } else if (dataArray.size() == 0) {
                    this.bootMode = kMipPingResponse.kMipPingResponseNormalRomNoBootloader;
                    this.callbackInterface.mipRobotIsCurrentlyInBootloader(this, false);
                } else {
                    Log.e("MipRobot", "ERROR: Data array count incorrect for MipGetUserData command");
                }
            } else if (commandValue == MipCommandValues.kMipCurrentGameMode) {
                Log.d("MipRobot", " commandValue == MipCommandValues.kMipGameMode");
                if (dataArray.size() == 1) {
                    byte gameMode = ((Byte) dataArray.get(0)).byteValue();
                    if (((gameMode <= (byte) 8 ? 1 : 0) & (gameMode >= (byte) 1 ? 1 : 0)) != 0) {
                        initDefaultValues();
                        if (this.callbackInterface != null) {
                            this.callbackInterface.mipDeviceReady(this);
                        } else {
                            Log.e("MipRobot", BuildConfig.FLAVOR + hashCode() + " peripheralDidBecomeReady callbackInterface = null");
                        }
                        new Thread(new C00131()).start();
                        return;
                    } else if (this.callbackInterface != null) {
                        MipRobotFinder.getInstance().scanForMips();
                        this.callbackInterface.deviceNotMip();
                        return;
                    } else {
                        return;
                    }
                }
                Log.e("MipRobot", "ERROR: Data array count incorrect for kMipGameMode command");
            }
        }
    }

    public void didReceiveBatteryUpdate(int batteryPercentage) {
        Log.d("MipRobot", "Received battery level: " + batteryPercentage);
    }

    public void didReceiveBluetoothRSSIUpdate(int rssi) {
        Log.d("MipRobot", "Received RSSI: " + rssi);
    }

    public void didReceiveRawCommandData(byte[] data) {
    }

    public void didReceiveRobotCommand(RobotCommand robotCommand) {
        Log.d("MipRobot", "didReceiveRobotCommand:: " + robotCommand.description());
        handleReceivedMipCommand(robotCommand);
    }

    public void didReceiveNuvotonChipstatus(nuvotonBootloaderMode NuvotonChipstatus) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveNuvotonChipstatus " + NuvotonChipstatus + " - ");
        if (this.callbackInterface != null) {
            this.callbackInterface.mipRobotNuvotonChipstatus(this, NuvotonChipstatus);
        }
    }

    public void didReceiveFirmwareCompleteStatus(nuvotonFirmwareCompleteStatus NuvotonFirmwareCompleteStatus) {
        Log.w("BluetoothRobotPrivate", "should override nuvotonFirmwareCompleteStatus " + NuvotonFirmwareCompleteStatus + " - ");
        if (this.callbackInterface != null) {
            this.callbackInterface.mipRobotFirmwareCompleteStatus(this, NuvotonFirmwareCompleteStatus);
        }
    }

    public void didReceiveFirmwareDataStatus(nuvotonFirmwareStatus FirmwareDataStatus) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveFirmwareDataStatus " + FirmwareDataStatus + " - ");
        if (this.callbackInterface != null) {
            this.callbackInterface.mipRobotFirmwareDataStatus(this, FirmwareDataStatus);
        }
    }

    public void didReceiveFirmwareSentdata(int sentdata) {
        Log.d("MiposaurRobot", "didReceiveFirmwareSentdata call interface call back");
        if (this.callbackInterface != null) {
            this.callbackInterface.mipRobotFirmwareSent(this, sentdata);
        }
    }

    public void didReceiveFirmwareToChip(int sentdata) {
        Log.d("MiposaurRobot", "didReceiveFirmwareSentdata call interface call back");
        if (this.callbackInterface != null) {
            this.callbackInterface.mipRobotFirmwareToChip(this, sentdata);
        }
    }
}
