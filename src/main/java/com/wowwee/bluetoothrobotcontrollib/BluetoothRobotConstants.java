package com.wowwee.bluetoothrobotcontrollib;

public class BluetoothRobotConstants extends BluetoothRobotConstantsBase {
    public static final String kDeviceDFUCharacteristicString = "dfuchar";
    public static final String kDeviceDFUCharacteristicUUID = "0000ff31-0000-1000-8000-00805f9b34fb";
    public static final String kDeviceDFUServiceString = "dfu";
    public static final String kDeviceDFUServiceUUID = "0000ff30-0000-1000-8000-00805f9b34fb";
    public static final String kDeviceNuvotonBootloaderServiceString = "nuvotonBootloader";
    public static final String kDeviceNuvotonBootloaderServiceUUID = "0000ff00-0000-1000-8000-00805f9b34fb";
    /* renamed from: kDeviceNuvotonBootloader_FirmwareWriteCompleteCharacteristicString */
    public static final String f0x11fe8ee1 = "firmwareWriteComplete";
    public static final String kDeviceNuvotonBootloader_FirmwareWriteCompleteCharacteristicUUID = "0000ff08-0000-1000-8000-00805f9b34fb";
    public static final String kDeviceNuvotonBootloader_GetChipStatusCharacteristicString = "getChipStatus";
    public static final String kDeviceNuvotonBootloader_GetChipStatusCharacteristicUUID = "0000ff01-0000-1000-8000-00805f9b34fb";
    /* renamed from: kDeviceNuvotonBootloader_ReadFirmwareDataStatusCharacteristicString */
    public static final String f1x482e5f41 = "readFirmwareDataStatus";
    /* renamed from: kDeviceNuvotonBootloader_ReadFirmwareDataStatusCharacteristicUUID */
    public static final String f2xfd2582cb = "0000ff06-0000-1000-8000-00805f9b34fb";
    public static final String kDeviceNuvotonBootloader_RestartChipCharacteristicString = "restartChip";
    public static final String kDeviceNuvotonBootloader_RestartChipCharacteristicUUID = "0000ff02-0000-1000-8000-00805f9b34fb";
    public static final String kDeviceNuvotonBootloader_StopTransferCharacteristicString = "stoptransfer";
    public static final String kDeviceNuvotonBootloader_StopTransferCharacteristicUUID = "0000ff04-0000-1000-8000-00805f9b34fb";
    /* renamed from: kDeviceNuvotonBootloader_TransferFirmwareDataCharacteristicString */
    public static final String f3x416604c4 = "transferData";
    public static final String kDeviceNuvotonBootloader_TransferFirmwareDataCharacteristicUUID = "0000ff09-0000-1000-8000-00805f9b34fb";
    /* renamed from: kDeviceNuvotonBootloader_TransferFirmwareHeaderCharacteristicString */
    public static final String f4x6e7744a7 = "headerData";
    /* renamed from: kDeviceNuvotonBootloader_TransferFirmwareHeaderCharacteristicUUID */
    public static final String f5xe31489b1 = "0000ff03-0000-1000-8000-00805f9b34fb";
    /* renamed from: kDeviceNuvotonBootloader_TransferFirmwareStatusCharacteristicString */
    public static final String f6x59b08d8c = "transferFirmwareStatus";
    /* renamed from: kDeviceNuvotonBootloader_TransferFirmwareStatusCharacteristicUUID */
    public static final String f7x2164c7d6 = "0000ff05-0000-1000-8000-00805f9b34fb";
    /* renamed from: kDeviceNuvotonBootloader_WriteFirmwareToNuvotonCharacteristicString */
    public static final String f8x2943a32 = "writeFirmwareToNuvoton";
    /* renamed from: kDeviceNuvotonBootloader_WriteFirmwareToNuvotonCharacteristicUUID */
    public static final String f9x447765fc = "0000ff07-0000-1000-8000-00805f9b34fb";
    /* renamed from: kDeviceNuvotonBootloader_WriteFirmwareToNuvotonProgressCharacteristicString */
    public static final String f10x43f051ff = "firmwareToNuvotonProgress";
    /* renamed from: kDeviceNuvotonBootloader_WriteFirmwareToNuvotonProgressCharacteristicUUID */
    public static final String f11x29a10d09 = "0000ff0a-0000-1000-8000-00805f9b34fb";
    public static final String kMipBatteryLevelReportCharacteristicString = "batteryLevelReport";
    public static final String kMipBatteryLevelReportCharacteristicUUID = "00002a19-0000-1000-8000-00805f9b34fb";
    public static final String kMipBatteryLevelServiceString = "batteryLevel";
    public static final String kMipBatteryLevelServiceUUID = "0000180f-0000-1000-8000-00805f9b34fb";
    public static final String kMipDeviceInformationModuleSoftwareVerCharacteristicString = "softwareVersion";
    public static final String kMipDeviceInformationModuleSoftwareVerCharacteristicUUID = "00002a26-0000-1000-8000-00805f9b34fb";
    public static final String kMipDeviceInformationServiceString = "deviceInfo";
    public static final String kMipDeviceInformationServiceUUID = "0000180a-0000-1000-8000-00805f9b34fb";
    public static final String kMipDeviceInformationSystemIDCharacteristicString = "systemId";
    public static final String kMipDeviceInformationSystemIDCharacteristicUUID = "00002a23-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterBTCommunicationIntervalCharacteristicString = "btCommInterval";
    public static final String kMipModuleParameterBTCommunicationIntervalCharacteristicUUID = "0000ff92-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterBroadcastPeriodCharacteristicString = "setOrReadBroadcastPeriod";
    public static final String kMipModuleParameterBroadcastPeriodCharacteristicUUID = "0000ff95-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterCustomBroadcastDataCharacteristicString = "setOrReadCustomBroadcastData";
    public static final String kMipModuleParameterCustomBroadcastDataCharacteristicUUID = "0000ff98-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterDeviceNameCharacteristicString = "deviceName";
    public static final String kMipModuleParameterDeviceNameCharacteristicUUID = "0000ff91-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterProductIDCharacteristicString = "setOrReadProductID";
    public static final String kMipModuleParameterProductIDCharacteristicUUID = "0000ff96-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterRemoteControlExtensionCharacteristicString = "setRemoteControlExtension";
    public static final String kMipModuleParameterRemoteControlExtensionCharacteristicUUID = "0000ff99-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterResetModuleCharacteristicString = "resetModule";
    public static final String kMipModuleParameterResetModuleCharacteristicUUID = "0000ff94-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterSetConnectedBroadcastDataCharacteristicString = "setConnectedBroadcastData";
    public static final String kMipModuleParameterSetConnectedBroadcastDataCharacteristicUUID = "0000ff9B-0000-1000-8000-00805f9b34fb";
    /* renamed from: kMipModuleParameterSetConnectedBroadcastOnOffCharacteristicString */
    public static final String f12x71c370b6 = "setConnectedBroadcastOnOff";
    public static final String kMipModuleParameterSetConnectedBroadcastOnOffCharacteristicUUID = "0000ff9C-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterStandbyModeCharacteristicString = "readOrWriteStandbyMode";
    public static final String kMipModuleParameterStandbyModeCharacteristicUUID = "0000ff9A-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterTransmitPowerCharacteristicString = "setOrReadTransmitPower";
    public static final String kMipModuleParameterTransmitPowerCharacteristicUUID = "0000ff97-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParameterUARTBaudRateCharacteristicString = "uartBaudRate";
    public static final String kMipModuleParameterUARTBaudRateCharacteristicUUID = "0000ff93-0000-1000-8000-00805f9b34fb";
    public static final String kMipModuleParametersServiceString = "moduleParams";
    public static final String kMipModuleParametersServiceUUID = "0000ff90-0000-1000-8000-00805f9b34fb";
    public static final String kMipRSSIReportReadChracteristicString = "rssiLevel";
    public static final String kMipRSSIReportReadChracteristicUUID = "0000ffa1-0000-1000-8000-00805f9b34fb";
    public static final String kMipRSSIReportServiceString = "rssiReport";
    public static final String kMipRSSIReportServiceUUID = "0000ffa0-0000-1000-8000-00805f9b34fb";
    public static final String kMipRSSIReportSetIntervalChracteristicString = "setRssiNotification";
    public static final String kMipRSSIReportSetIntervalChracteristicUUID = "0000ffa2-0000-1000-8000-00805f9b34fb";
    public static final String kSettingProductActivationCharacteristicString = "activation";
    public static final String kSettingProductActivationCharacteristicUUID = "0000ff1b-0000-1000-8000-00805f9b34fb";
    public static final String kSettingServiceString = "deviceSetting";
    public static final String kSettingServiceUUID = "0000ff10-0000-1000-8000-00805f9b34fb";

    public enum kDFURebootMode {
        kDeviceApplicationMode((byte) 1),
        kDeviceDFUMode((byte) 2);
        
        byte value;

        private kDFURebootMode(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kDFURebootMode getParamWithValue(byte value) {
            for (kDFURebootMode param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum kModuleParameterUARTBaudRateValues {
        kModuleParameterUARTBaudRate4800((byte) 0),
        kModuleParameterUARTBaudRate9600((byte) 1),
        kModuleParameterUARTBaudRate19200((byte) 2),
        kModuleParameterUARTBaudRate38400((byte) 3),
        kModuleParameterUARTBaudRate57600((byte) 4),
        kModuleParameterUARTBaudRate115200((byte) 5);
        
        byte value;

        private kModuleParameterUARTBaudRateValues(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kModuleParameterUARTBaudRateValues getParamWithValue(byte value) {
            for (kModuleParameterUARTBaudRateValues param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum nuvotonBootloaderMode {
        kNuvotonAppMode((byte) 0),
        kNuvotonBootloaderMode((byte) 1);
        
        byte value;

        private nuvotonBootloaderMode(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static nuvotonBootloaderMode getParamWithValue(byte value) {
            for (nuvotonBootloaderMode param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum nuvotonFirmwareCompleteStatus {
        kNuvotonFirmwareCompleteStatus_ready((byte) -1),
        kNuvotonFirmwareCompleteStatus_Success((byte) 0),
        kNuvotonFirmwareCompleteStatus_BadFirmwareData((byte) 1),
        kNuvotonFirmwareCompleteStatus_UpdateFailure((byte) 2);
        
        byte value;

        private nuvotonFirmwareCompleteStatus(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static nuvotonFirmwareCompleteStatus getParamWithValue(byte value) {
            for (nuvotonFirmwareCompleteStatus param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum nuvotonFirmwareStatus {
        kNuvotonFirmwareStatus_ready((byte) -1),
        kNuvotonFirmwareStatus_DataOK((byte) 0),
        kNuvotonFirmwareStatus_BadChecksum((byte) 1),
        kNuvotonFirmwareStatus_BadData((byte) 2),
        kNuvotonFirmwareStatus_DataEmpty((byte) 3),
        kNuvotonFirmwareStatus_NextPacket((byte) 4),
        kNuvotonFirmwareStatus_HeaderOK((byte) 5);
        
        byte value;

        private nuvotonFirmwareStatus(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static nuvotonFirmwareStatus getParamWithValue(byte value) {
            for (nuvotonFirmwareStatus param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }
}
