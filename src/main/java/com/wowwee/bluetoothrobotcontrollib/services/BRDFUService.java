package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants;
import java.beans.PropertyChangeListener;
import java.util.UUID;

public class BRDFUService extends BRBaseService {
    public BRDFUService(BluetoothLeService pBluetoothLeService, PropertyChangeListener pListener, String pBluetoothDeviceAddress) {
        super(BluetoothRobotConstants.kDeviceDFUServiceString, UUID.fromString(BluetoothRobotConstants.kDeviceDFUServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
        addPropertyChangeListener(pListener);
    }

    public void rebootToMode(byte mode) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kDeviceDFUCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterService", "This device does not support btCommInterval characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{mode});
        }
    }
}
