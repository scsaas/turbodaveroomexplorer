package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants;
import java.beans.PropertyChangeListener;
import java.util.UUID;

public class BRSettingService extends BRBaseService {
    public static final String activationStatusKeyPathKVO = "activationStatus";
    private int activationStatus = -1;

    public BRSettingService(BluetoothLeService pBluetoothLeService, PropertyChangeListener pListener, String pBluetoothDeviceAddress) {
        super(BluetoothRobotConstants.kSettingServiceString, UUID.fromString(BluetoothRobotConstants.kSettingServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
        addPropertyChangeListener(pListener);
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
        if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kSettingProductActivationCharacteristicUUID)) {
            byte[] data = pCharacteristic.getValue();
            if (data.length >= 1) {
                byte value = data[0];
                int oldActivationStatus = this.activationStatus;
                this.activationStatus = value;
                this.changes.fireIndexedPropertyChange(activationStatusKeyPathKVO, 1, String.valueOf(oldActivationStatus), String.valueOf(this.activationStatus));
                Log.d("BLE", "BRProductionActivationService receive kProductActivationCharacteristicUUID: " + String.valueOf(oldActivationStatus) + " " + String.valueOf(this.activationStatus));
            }
        }
    }

    public void readProductActivationStatus() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kSettingProductActivationCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRProductionActivationService", "This device does not support readActivationStatus characteristic");
            } else {
                this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
            }
        }
    }

    public void writeProductActivationStatus(int activationType) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kSettingProductActivationCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRProductionActivationService", "This device does not support writeActivationStatus characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{(byte) activationType});
        }
    }
}
