package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService.BRServiceAction;
import java.util.UUID;

public class BRSendDataService extends BRBaseService {
    private static final int sendMaxChunkSize = 20;
    private boolean dataNextData = true;

    public BRSendDataService(BluetoothLeService pBluetoothLeService, String pBluetoothDeviceAddress) {
        super("sendData", UUID.fromString(BluetoothRobotConstantsBase.kMipSendDataServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
        this.dataNextData = true;
    }

    public void sendData(byte[] data) {
        sendData(data, null);
    }

    public void sendData(byte[] data, BRServiceAction writeCallback) {
        if (this.mBluetoothService != null) {
            final BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstantsBase.kMipSendBytesDataCharateristicUUID));
            final BRSendDataService sendDataService = this;
            this.dataNextData = true;
            final byte[] bArr = data;
            final BRServiceAction bRServiceAction = writeCallback;
            new Thread() {
                public void run() {
                    synchronized (sendDataService) {
                        int length = bArr.length;
                        int offset = 0;
                        do {
                            int thisChunkSize = length - offset > BRSendDataService.sendMaxChunkSize ? BRSendDataService.sendMaxChunkSize : length - offset;
                            byte[] chunk = new byte[thisChunkSize];
                            System.arraycopy(bArr, offset, chunk, 0, thisChunkSize);
                            offset += thisChunkSize;
                            if (offset < length) {
                                BRSendDataService.this.dataNextData = false;
                            } else {
                                BRSendDataService.this.dataNextData = true;
                            }
                            if (characteristic == null) {
                                break;
                            }
                            BRSendDataService.this.mBluetoothLeService.writeCharacteristic(characteristic, BRSendDataService.this.mBluetoothDeviceAddress, chunk);
                            do {
                                try {
                                    Thread.sleep(5);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } while (!BRSendDataService.this.dataNextData);
                        } while (offset < length);
                        if (bRServiceAction != null) {
                            bRServiceAction.serviceActionCallback(null);
                        }
                    }
                }
            }.start();
        }
    }
}
