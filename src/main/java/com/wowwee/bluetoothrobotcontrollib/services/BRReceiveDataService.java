package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase;
import com.wowwee.bluetoothrobotcontrollib.BuildConfig;
import com.wowwee.bluetoothrobotcontrollib.RobotCommand;
import java.beans.PropertyChangeListener;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.UUID;

public class BRReceiveDataService extends BRBaseService {
    public static final String commandKeyPathKVO = "lastRobotCommand";
    public static final String firmwareKeyPathKVO = "lastFirmwareCommand";
    public static final String rawDataKeyPathKVO = "lastCommandData";
    public boolean firmwareUpdateMode = false;
    private byte[] lastCommandData = null;
    private byte[] lastFirmwareCommand;
    private RobotCommand lastRobotCommand = null;
    public boolean processRobotCommands = true;

    public BRReceiveDataService(BluetoothLeService pBluetoothLeService, PropertyChangeListener pListener, String pBluetoothDeviceAddress) {
        super(BluetoothRobotConstantsBase.kMipReceiveDataServiceString, UUID.fromString(BluetoothRobotConstantsBase.kMipReceiveDataServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
        addPropertyChangeListener(pListener);
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
        if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstantsBase.kMipReceiveDataCharateristicUUID)) {
            byte[] data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                ArrayList byteList = new ArrayList();
                boolean isUnsigned = false;
                for (int i = 0; i < data.length; i++) {
                    if (data[i] < (byte) 0) {
                        isUnsigned = true;
                    }
                    byteList.add(Byte.valueOf(data[i]));
                }
                String ascii = null;
                if (!isUnsigned) {
                    try {
                        ascii = new String(data, "US-ASCII");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("Bootloader", "ascii = " + ascii + ", data = " + data);
                byte[] oldData = this.lastCommandData;
                this.lastCommandData = data;
                this.changes.fireIndexedPropertyChange(rawDataKeyPathKVO, 0, oldData, this.lastCommandData);
                if (!this.processRobotCommands) {
                    Log.e("MipRobot", "ReceiveData !processRobotCommands return");
                } else if (this.firmwareUpdateMode) {
                    byte[] dat = this.lastFirmwareCommand;
                    this.lastFirmwareCommand = data;
                    this.changes.fireIndexedPropertyChange(firmwareKeyPathKVO, 0, dat, this.lastFirmwareCommand);
                } else {
                    RobotCommand robotCommand = null;
                    if (isUnsigned) {
                        try {
                            robotCommand = RobotCommand.create(data[0], byteList);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    } else if (!ascii.equals(BuildConfig.FLAVOR)) {
                        robotCommand = RobotCommand.create(ascii);
                    }
                    if (robotCommand != null) {
                        RobotCommand oldRobotCommand = this.lastRobotCommand;
                        this.lastRobotCommand = robotCommand;
                        this.changes.fireIndexedPropertyChange(commandKeyPathKVO, 1, oldRobotCommand, this.lastRobotCommand);
                        return;
                    }
                    Log.e("MipRobot", "ReceiveData robotCommand is null");
                }
            }
        }
    }

    public void turnOff() {
        setNotifications(false);
    }

    public void turnOn() {
        setNotifications(true);
    }

    private void setNotifications(boolean value) {
        if (this.mBluetoothService != null) {
            this.mBluetoothLeService.setCharacteristicNotification(this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstantsBase.kMipReceiveDataCharateristicUUID)), this.mBluetoothDeviceAddress, value);
        }
    }
}
