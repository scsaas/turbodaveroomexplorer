package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.UUID;

public class BRBaseService {
    protected PropertyChangeSupport changes = new PropertyChangeSupport(this);
    protected boolean isOptional;
    protected String mBluetoothDeviceAddress;
    protected BluetoothLeService mBluetoothLeService;
    protected BluetoothGattService mBluetoothService;
    protected String mName;
    protected UUID mUuid;

    public interface BRServiceAction {
        void serviceActionCallback(Error error);
    }

    public BRBaseService(String pName, UUID uuid, BluetoothLeService pBluetoothLeService, String pBluetoothDeviceAddress) {
        this.mBluetoothLeService = pBluetoothLeService;
        this.mName = pName;
        this.mUuid = uuid;
        this.mBluetoothDeviceAddress = pBluetoothDeviceAddress;
        configureBluetoothService();
    }

    public void configureBluetoothService() {
        this.mBluetoothService = this.mBluetoothLeService.getGattService(this.mUuid, this.mBluetoothDeviceAddress);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.changes.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.changes.removePropertyChangeListener(l);
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
    }

    public void descriptorHandler(BluetoothGattDescriptor pdescriptor) {
    }
}
