package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants;
import com.wowwee.bluetoothrobotcontrollib.util.AdRecord;
import java.beans.PropertyChangeListener;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.UUID;

public class BRDeviceInformationService extends BRBaseService {
    public static final String softwareVersionKeyPathKVO = "moduleSoftwareVersion";
    public static final String systemIdKeyPathKVO = "systemId";
    public String moduleSoftwareVersion;
    public String systemid;

    public static void reverse(byte[] array) {
        if (array != null) {
            int j = array.length - 1;
            for (int i = 0; j > i; i++) {
                byte tmp = array[j];
                array[j] = array[i];
                array[i] = tmp;
                j--;
            }
        }
    }

    public BRDeviceInformationService(BluetoothLeService pBluetoothLeService, PropertyChangeListener pListener, String pBluetoothDeviceAddress) {
        super(BluetoothRobotConstants.kMipDeviceInformationServiceString, UUID.fromString(BluetoothRobotConstants.kMipDeviceInformationServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
        addPropertyChangeListener(pListener);
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
        byte[] data;
        if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipDeviceInformationSystemIDCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                reverse(data);
                String oldSystemId = this.systemid;
                String systemIdentifier = AdRecord.bytesToHex(data);
                this.systemid = systemIdentifier;
                this.changes.fireIndexedPropertyChange("systemId", 1, oldSystemId, this.systemid);
                Log.d("BLE", "BRDeviceInformationService receive system id: " + systemIdentifier);
            }
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipDeviceInformationModuleSoftwareVerCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                String ascii;
                try {
                    ascii = new String(data, "US-ASCII");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    ascii = new String(Arrays.toString(data));
                }
                String oldModuleVersion = this.moduleSoftwareVersion;
                this.moduleSoftwareVersion = ascii;
                this.changes.fireIndexedPropertyChange(softwareVersionKeyPathKVO, 2, oldModuleVersion, this.moduleSoftwareVersion);
                Log.d("BLE", "BRDeviceInformationService receive module software version: " + this.moduleSoftwareVersion);
            }
        }
    }

    public void readSystemId() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipDeviceInformationSystemIDCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterService", "This device does not support SystemID characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void readModuleSoftwareVersion() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipDeviceInformationModuleSoftwareVerCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterService", "This device does not support module software version characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
                Log.d("BRDeviceInformationServer", "charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0");
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }
}
