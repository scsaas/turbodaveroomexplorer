package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants;
import java.beans.PropertyChangeListener;
import java.nio.ByteBuffer;
import java.util.UUID;

public class BRBatteryLevelService extends BRBaseService {
    public static final String batteryReadingKeyPathKVO = "batteryReading";
    private int mBatteryReading = 0;

    public BRBatteryLevelService(BluetoothLeService pBluetoothLeService, PropertyChangeListener pListener, String pBluetoothDeviceAddress) {
        super(BluetoothRobotConstants.kMipBatteryLevelServiceString, UUID.fromString(BluetoothRobotConstants.kMipBatteryLevelServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
        addPropertyChangeListener(pListener);
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
        if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipBatteryLevelReportCharacteristicUUID)) {
            byte[] data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                ByteBuffer byteBuffer = ByteBuffer.wrap(data);
                int oldBatteryReading = this.mBatteryReading;
                this.mBatteryReading = byteBuffer.get();
                this.changes.fireIndexedPropertyChange(batteryReadingKeyPathKVO, this.mBatteryReading, oldBatteryReading, this.mBatteryReading);
            }
        }
    }

    public void readBatteryLevel() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipBatteryLevelReportCharacteristicUUID));
            int charaProp = characteristic.getProperties();
            if ((charaProp | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            if ((charaProp | 2) > 0) {
                this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
            }
        }
    }

    public void turnOff() {
        setNotifications(false);
    }

    public void turnOn() {
        setNotifications(true);
    }

    private void setNotifications(boolean value) {
        if (this.mBluetoothService != null) {
            this.mBluetoothLeService.setCharacteristicNotification(this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipBatteryLevelReportCharacteristicUUID)), this.mBluetoothDeviceAddress, value);
        }
    }
}
