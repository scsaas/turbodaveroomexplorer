package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants;
import java.beans.PropertyChangeListener;
import java.nio.ByteBuffer;
import java.util.UUID;

public class BRRSSIReportService extends BRBaseService {
    public static final String rssiLevelKVO = "rssiLevel";
    public boolean mIsNotifying = false;
    public int mNotificationPeriod = 0;
    public int mRssiLevel = 0;

    public BRRSSIReportService(BluetoothLeService pBluetoothLeService, PropertyChangeListener pListener, String pBluetoothDeviceAddress) {
        super(BluetoothRobotConstants.kMipRSSIReportServiceString, UUID.fromString(BluetoothRobotConstants.kMipRSSIReportServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
        addPropertyChangeListener(pListener);
        this.mIsNotifying = false;
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
        if (!pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipRSSIReportSetIntervalChracteristicUUID) && pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipRSSIReportReadChracteristicUUID)) {
            byte[] data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                ByteBuffer byteBuffer = ByteBuffer.wrap(data);
                int oldRssiLevel = this.mRssiLevel;
                this.mRssiLevel = byteBuffer.get();
                this.changes.fireIndexedPropertyChange("rssiLevel", this.mRssiLevel, oldRssiLevel, this.mRssiLevel);
            }
        }
    }

    public void setConfigPeriod(byte value) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipRSSIReportSetIntervalChracteristicUUID));
            int charaProp = characteristic.getProperties();
            if ((charaProp | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            if ((charaProp | 8) > 0) {
                this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{value});
            }
            this.mNotificationPeriod = value;
        }
    }

    public void readConfigPeriod() {
    }

    public void readRSSI() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipRSSIReportReadChracteristicUUID));
            int charaProp = characteristic.getProperties();
            if ((charaProp | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            if ((charaProp | 2) > 0) {
                this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
            }
        }
    }

    public void turnOff() {
        setConfigPeriod((byte) 0);
        setNotifications(false);
    }

    public void turnOnWithPeriod(byte period) {
        setConfigPeriod(period);
        setNotifications(true);
    }

    private void setNotifications(boolean value) {
        if (this.mBluetoothService != null) {
            this.mBluetoothLeService.setCharacteristicNotification(this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipRSSIReportReadChracteristicUUID)), this.mBluetoothDeviceAddress, value);
        }
    }
}
