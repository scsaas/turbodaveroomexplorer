package com.wowwee.bluetoothrobotcontrollib.services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.kModuleParameterUARTBaudRateValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kMipModuleParameter_RemoteControlExtensionValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kMipModuleParameter_TransmitPowerValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kModuleParameterBTCommunicationIntervalValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kModuleParameterValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kModuleParameter_BroadcastPeriodValues;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;
//import org.apache.http.util.ByteArrayBuffer;

public class BRModuleParametersService extends BRBaseService {
    public static final String broadcastPeriodKeyPathKVO = "btBroadcastPeriod";
    public static final String btCommIntervalKeyPathKVO = "btCommInterval";
    public static final String customBroadcastDataKeyPathKVO = "customBroadcastData";
    public static final String deviceNameKeyPathKVO = "deviceName";
    public static final String productIdKeyPathKVO = "productId";
    public static final String pulseSleepModeKeyPathKVO = "pulseSleepMode";
    public static final String transmitPowerKeyPathKVO = "moduleTransmitPower";
    public static final String uartBaudRateKeyPathKVO = "moduleUartBaudRate";
    private final kModuleParameter_BroadcastPeriodValues broadcastPeriodDefaultValue = kModuleParameter_BroadcastPeriodValues.kModuleParameter_BroadcastPeriod200MS;
    private kModuleParameter_BroadcastPeriodValues btBroadcastPeriod;
    private kModuleParameterBTCommunicationIntervalValues btCommInterval;
    private final kModuleParameterBTCommunicationIntervalValues btCommIntervalDefaultValue = kModuleParameterBTCommunicationIntervalValues.kModuleParameterBTCommunicationInterval20ms;
    private HashMap<Byte, Byte> customBroadcastData;
    private String deviceName;
    private String deviceNameInProgres = null;
    private boolean isSettingsDeviceName = false;
    private kMipModuleParameter_TransmitPowerValues moduleTransmitPower;
    private kModuleParameterUARTBaudRateValues moduleUartBaudRate;
    private short productId;
    private boolean pulseSleepMode;
    private final kMipModuleParameter_TransmitPowerValues transmitPowerDefaultValue = kMipModuleParameter_TransmitPowerValues.kMipModuleParameter_TransmitPower_0dBm;
    private final kModuleParameterUARTBaudRateValues uartBuadDefaultValue = kModuleParameterUARTBaudRateValues.kModuleParameterUARTBaudRate9600;

    public BRModuleParametersService(BluetoothLeService pBluetoothLeService, PropertyChangeListener pListener, String pBluetoothDeviceAddress) {
        super(BluetoothRobotConstants.kMipModuleParametersServiceString, UUID.fromString(BluetoothRobotConstants.kMipModuleParametersServiceUUID), pBluetoothLeService, pBluetoothDeviceAddress);
        addPropertyChangeListener(pListener);
        this.moduleUartBaudRate = this.uartBuadDefaultValue;
        this.btCommInterval = this.btCommIntervalDefaultValue;
        this.btBroadcastPeriod = this.broadcastPeriodDefaultValue;
        this.pulseSleepMode = false;
        this.customBroadcastData = null;
        this.moduleTransmitPower = this.transmitPowerDefaultValue;
        this.productId = (short) 0;
        if (pBluetoothLeService.getGatt(this.mBluetoothDeviceAddress) != null) {
            this.deviceName = pBluetoothLeService.getGatt(this.mBluetoothDeviceAddress).getDevice().getName();
        }
    }

    public void notifyCharacteristicHandler(BluetoothGattCharacteristic pCharacteristic) {
        byte[] data;
        if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterDeviceNameCharacteristicUUID)) {
            String oldDeviceName;
            if (!this.isSettingsDeviceName || this.deviceNameInProgres == null) {
                data = pCharacteristic.getValue();
                if (data != null && data.length > 0) {
                    String asciiDeviceName = null;
                    try {
                        asciiDeviceName = new String(data, "US-ASCII");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    oldDeviceName = this.deviceName;
                    this.deviceName = asciiDeviceName;
                    this.changes.fireIndexedPropertyChange("deviceName", 1, oldDeviceName, this.deviceName);
                    Log.d("BLE", "BRModuleParametersService receive name: " + this.deviceName);
                }
            } else {
                oldDeviceName = this.deviceName;
                this.deviceName = this.deviceNameInProgres;
                this.deviceNameInProgres = null;
                Log.d("BLE", "BRModuleParametersService receive new name: " + this.deviceName);
                this.changes.fireIndexedPropertyChange("deviceName", 1, oldDeviceName, this.deviceName);
            }
            this.isSettingsDeviceName = false;
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterBTCommunicationIntervalCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                byte payload = data[0];
                kModuleParameterBTCommunicationIntervalValues oldParam = this.btCommInterval;
                this.btCommInterval = kModuleParameterBTCommunicationIntervalValues.getParamWithValue(payload);
                this.changes.fireIndexedPropertyChange("btCommInterval", 2, oldParam, this.btCommInterval);
                Log.d("BLE", "BRModuleParametersService receive btCommInterval: " + this.btCommInterval.getValue());
            }
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterUARTBaudRateCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                byte payload = data[0];
                kModuleParameterUARTBaudRateValues oldParam2 = this.moduleUartBaudRate;
                this.moduleUartBaudRate = kModuleParameterUARTBaudRateValues.getParamWithValue(payload);
                this.changes.fireIndexedPropertyChange(uartBaudRateKeyPathKVO, 3, oldParam2, this.moduleUartBaudRate);
                Log.d("BLE", "BRModuleParametersService receive moduleUartBaudRate: " + this.moduleUartBaudRate.getValue());
            }
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterBroadcastPeriodCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                byte payload = data[0];
                kModuleParameter_BroadcastPeriodValues oldParam3 = this.btBroadcastPeriod;
                this.btBroadcastPeriod = kModuleParameter_BroadcastPeriodValues.getParamWithValue(payload);
                this.changes.fireIndexedPropertyChange(broadcastPeriodKeyPathKVO, 4, oldParam3, this.btBroadcastPeriod);
                Log.d("BLE", "BRModuleParametersService receive btBroadcastPeriod: " + this.btBroadcastPeriod.getValue());
            }
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterProductIDCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 1) {
                Short oldProductId = Short.valueOf(this.productId);
                this.productId = (short) ((data[1] << 8) + (data[0] & 255));
                this.changes.fireIndexedPropertyChange(productIdKeyPathKVO, 5, oldProductId, Short.valueOf(this.productId));
                Log.d("BLE", "BRModuleParametersService receive btBroadcastPeriod: " + this.btBroadcastPeriod.getValue());
            }
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterTransmitPowerCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                byte payload = data[0];
                kMipModuleParameter_TransmitPowerValues oldParam4 = this.moduleTransmitPower;
                this.moduleTransmitPower = kMipModuleParameter_TransmitPowerValues.getParamWithValue(payload);
                this.changes.fireIndexedPropertyChange(transmitPowerKeyPathKVO, 5, oldParam4, this.moduleTransmitPower);
                Log.d("BLE", "BRModuleParametersService receive moduleTransmitPower: " + this.moduleTransmitPower.getValue());
            }
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterCustomBroadcastDataCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                int len = data.length;
                HashMap<Byte, Byte> newCustomBroadcastData = new HashMap();
                for (int i = 0; i < len - 1; i += 2) {
                    newCustomBroadcastData.put(Byte.valueOf(data[i]), Byte.valueOf(data[i + 1]));
                }
                Log.d("BLE", "BRModuleParametersService receive customBroadcastData: " + this.customBroadcastData.toString());
            }
        } else if (pCharacteristic.getUuid().toString().equals(BluetoothRobotConstants.kMipModuleParameterStandbyModeCharacteristicUUID)) {
            data = pCharacteristic.getValue();
            if (data != null && data.length > 0) {
                byte payload = data[0];
                boolean oldPulseSleepMode = this.pulseSleepMode;
                this.pulseSleepMode = payload != (byte) 0;
                this.changes.fireIndexedPropertyChange(pulseSleepModeKeyPathKVO, 7, oldPulseSleepMode, this.pulseSleepMode);
                Log.d("BLE", "BRModuleParametersService receive pulseSleepMode: " + this.pulseSleepMode);
            }
        }
    }

    public void readBTDeviceName() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterDeviceNameCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support deviceName characteristic");
                return;
            }
            this.isSettingsDeviceName = false;
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void setBTDeviceName(String newDeviceName) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterDeviceNameCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support deviceName characteristic");
            } else if (newDeviceName == null) {
                Log.e("BRModuleParameterServic", "Device name cannot be null");
            } else {
                if (newDeviceName.length() > 15) {
                    Log.w("BRModuleParameterServic", "Device Name [" + newDeviceName + "] cannot be longer than " + 15 + " characters, trimming to fit");
                }
                String trimmedDeviceName = newDeviceName.substring(0, Math.min(15, newDeviceName.length()));
                byte[] deviceNameData = null;
                try {
                    deviceNameData = trimmedDeviceName.getBytes("US-ASCII");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                if (deviceNameData != null) {
                    this.isSettingsDeviceName = true;
                    this.deviceNameInProgres = trimmedDeviceName;
                    ByteArrayOutputStream deviceNameByteBuffer = new ByteArrayOutputStream();
                    byte[] data = new byte[deviceNameData.length + 1];
                    deviceNameByteBuffer.write(deviceNameData, 0, deviceNameData.length);
                    deviceNameByteBuffer.write(new byte[]{(byte) 0}, 0, 1);

                    /*ByteArrayBuffer deviceNameByteBuffer = new ByteArrayBuffer(deviceNameData.length + 1);
                    deviceNameByteBuffer.append(deviceNameData, 0, deviceNameData.length);
                    deviceNameByteBuffer.append(new byte[]{(byte) 0}, 0, 1);*/
                    if ((characteristic.getProperties() | 16) > 0) {
                        this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
                    }
                    this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, deviceNameByteBuffer.toByteArray());
                }
            }
        }
    }

    public void readBTCommunicationInterval() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterBTCommunicationIntervalCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support btCommInterval characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void setBTCommunicationInterval(kModuleParameterBTCommunicationIntervalValues value) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterBTCommunicationIntervalCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support btCommInterval characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{value.getValue()});
        }
    }

    public void readUartBaudRate() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterUARTBaudRateCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support uartBaudRate characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void setUartBuadRate(kModuleParameterUARTBaudRateValues value) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterUARTBaudRateCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support uartBaudRate characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{value.getValue()});
        }
    }

    public void restartModule() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterResetModuleCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support resetModule characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{kModuleParameterValues.kModuleParameter_RestartModule.getValue()});
        }
    }

    public void restoreUserDataToFactorySettings() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterResetModuleCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support resetModule characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{kModuleParameterValues.kModuleParameter_ResetModuleResetUserData.getValue()});
        }
    }

    public void restoreFullyFactorySettings() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterResetModuleCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support resetModule characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{kModuleParameterValues.kModuleParameter_ResetModuleRestoreFactorySettings.getValue()});
        }
    }

    public void setBroadcastPeriod(kModuleParameter_BroadcastPeriodValues period) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterBroadcastPeriodCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadBroadcastPeriod characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{period.getValue()});
        }
    }

    public void readBroadcastPeriod() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterBroadcastPeriodCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadBroadcastPeriod characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void setProductIdentifier(short productIdentifier) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterProductIDCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadProductID characteristic");
                return;
            }
            byte[] productIdentifierData = new byte[]{(byte) (productIdentifier & 255), (byte) ((productIdentifier >> 8) & 255)};
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, productIdentifierData);
        }
    }

    public void readProductIdentifier() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterProductIDCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadProductID characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void setTransmitPower(kMipModuleParameter_TransmitPowerValues transmitPower) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterTransmitPowerCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadTransmitPower characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{transmitPower.getValue()});
        }
    }

    public void readTransmitPower() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterTransmitPowerCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadTransmitPower characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void setBroadcastData(HashMap<Byte, Byte> broadcastData) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterCustomBroadcastDataCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadCustomBroadcastData characteristic");
                return;
            }
            byte[] customData = new byte[16];
            for (Entry<Byte, Byte> pairs : broadcastData.entrySet()) {
                customData[((Byte) pairs.getKey()).byteValue()] = ((Byte) pairs.getValue()).byteValue();
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, customData);
            HashMap<Byte, Byte> oldBroadcastData = this.customBroadcastData;
            this.customBroadcastData = broadcastData;
            this.changes.fireIndexedPropertyChange(customBroadcastDataKeyPathKVO, 6, oldBroadcastData, this.customBroadcastData);
        }
    }

    public void setBroadcastDataToDefault() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterCustomBroadcastDataCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadCustomBroadcastData characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[16]);
            this.customBroadcastData = null;
        }
    }

    public void readBroadcastData() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterCustomBroadcastDataCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setOrReadCustomBroadcastData characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void forceModuleSleep() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterRemoteControlExtensionCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setRemoteControlExtension characteristic");
            } else if (this.pulseSleepMode) {
                if ((characteristic.getProperties() | 16) > 0) {
                    this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
                }
                this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{kMipModuleParameter_RemoteControlExtensionValues.kMipModuleParameter_RemoteControlExtension_ForceSleepMode.getValue()});
            } else {
                Log.w("BRModuleParameterServic", "The function forceModuleSleep is only available when in pulseSleepMode = true");
            }
        }
    }

    public void saveCurrentIOOutputInputState() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterRemoteControlExtensionCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setRemoteControlExtension characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{kMipModuleParameter_RemoteControlExtensionValues.kMipModuleParameter_RemoteControlExtension_SaveIOState.getValue()});
        }
    }

    public void setStandbyPulsedSleepMode(boolean pulsedSleep) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterStandbyModeCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support readOrWriteStandbyMode characteristic");
                return;
            }
            byte payloadValue = (byte) 0;
            if (pulsedSleep) {
                payloadValue = (byte) 1;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{payloadValue});
            boolean oldPulseSleepMode = this.pulseSleepMode;
            this.pulseSleepMode = pulsedSleep;
            this.changes.fireIndexedPropertyChange(pulseSleepModeKeyPathKVO, 7, oldPulseSleepMode, this.pulseSleepMode);
        }
    }

    public void readStandbyPulsedSleepMode() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterStandbyModeCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support readOrWriteStandbyMode characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.readCharacteristic(characteristic, this.mBluetoothDeviceAddress);
        }
    }

    public void writeCurrentCustomBroadcastDataToFlash() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterRemoteControlExtensionCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support writeCurrentCustomBroadcastDataToFlash characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{kMipModuleParameter_RemoteControlExtensionValues.kMipModuleParameter_RemoteControlExtension_WriteCustomBroadcastDataToFlash.getValue()});
        }
    }

    public void disconnectCurrentBluetoothClient() {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterStandbyModeCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support disconnectCurrentBluetoothClient characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{kMipModuleParameter_RemoteControlExtensionValues.kMipModuleParameter_RemoteControlExtension_DisconnectBluetoothClient.getValue()});
        }
    }

    public void setConnectedBroadcastData(byte[] data) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterSetConnectedBroadcastDataCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setConnectedBroadcastData characteristic");
                return;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, data);
        }
    }

    public void setConnectedBroadcastOn(boolean isOn) {
        if (this.mBluetoothService != null) {
            BluetoothGattCharacteristic characteristic = this.mBluetoothService.getCharacteristic(UUID.fromString(BluetoothRobotConstants.kMipModuleParameterSetConnectedBroadcastOnOffCharacteristicUUID));
            if (characteristic == null) {
                Log.e("BRModuleParameterServic", "This device does not support setConnectedBroadcastOn characteristic");
                return;
            }
            byte payloadValue = (byte) 0;
            if (isOn) {
                payloadValue = (byte) 1;
            }
            if ((characteristic.getProperties() | 16) > 0) {
                this.mBluetoothLeService.setCharacteristicNotification(characteristic, this.mBluetoothDeviceAddress, true);
            }
            this.mBluetoothLeService.writeCharacteristic(characteristic, this.mBluetoothDeviceAddress, new byte[]{payloadValue});
        }
    }
}
