package com.wowwee.bluetoothrobotcontrollib;

import android.bluetooth.BluetoothGatt;

public interface BluetoothLeServiceListener {
    void onBluetoothLeServiceConnectionStateChanged(BluetoothGatt bluetoothGatt, String str);

    void onServicesDiscovered();
}
