package com.wowwee.bluetoothrobotcontrollib;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BluetoothLeService extends Service {
    public static final String ACTION_DATA_AVAILABLE = "com.wowwee.bluetooth.le.ACTION_DATA_AVAILABLE";
    public static final String ACTION_GATT_CONNECTED = "com.wowwee.bluetooth.le.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.wowwee.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = "com.wowwee.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public static final String EXTRA_DATA = "com.wowwee.bluetooth.le.EXTRA_DATA";
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_DISCONNECTED = 0;
    private static final String TAG = BluetoothLeService.class.getSimpleName();
    private static final Queue<String> sAddresQsueue = new ConcurrentLinkedQueue();
    private static final Queue<byte[]> sChunkQueue = new ConcurrentLinkedQueue();
    private static boolean sIsWriting = false;
    private static final Queue<BluetoothGattCharacteristic> sWriteQueue = new ConcurrentLinkedQueue();
    private boolean isReadingCharacteristics = false;
    private long lastReadCharacteristicsTime = 0;
    private final IBinder mBinder = new LocalBinder();
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<String> mBluetoothDeviceAddress = new ArrayList();
    private HashMap<String, BluetoothGatt> mBluetoothGatt = new HashMap();
    private BluetoothManager mBluetoothManager;
    private int mConnectionState = 0;
    protected Context mContext;
    private final BluetoothGattCallback mGattCallback = new C00001();
    private HashMap<String, BluetoothLeServiceListener> mListener = new HashMap();
    public HashMap<String, HashMap<String, BRBaseService>> mServiceDict = new HashMap();
    private Handler readCharacteristicHandler = new Handler();
    private Runnable readCharacteristicRunnable;
    private ArrayList<String> readCharacteristicsAddressQueue = new ArrayList();
    private ArrayList<BluetoothGattCharacteristic> readCharacteristicsQueue = new ArrayList();
    private Handler writeCharacteristicHandler = new Handler();
    private Runnable writeCharacteristicRunnable;

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothLeService$1 */
    class C00001 extends BluetoothGattCallback {
        C00001() {
        }

        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            String address;
            if (newState == 2) {
                if (BluetoothLeService.this.mBluetoothGatt.containsValue(gatt)) {
                    intentAction = BluetoothLeService.ACTION_GATT_CONNECTED;
                    BluetoothLeService.this.mConnectionState = 2;
                    BluetoothLeService.this.broadcastUpdate(intentAction);
                    Log.i(BluetoothLeService.TAG, "Connected to GATT server.");
                    Log.i(BluetoothLeService.TAG, "Attempting to start service discovery:" + gatt.discoverServices());
                    address = BluetoothLeService.this.getAddressFromBluetoothGatt(gatt);
                    if (address != null && BluetoothLeService.this.mListener.containsKey(address)) {
                        ((BluetoothLeServiceListener) BluetoothLeService.this.mListener.get(address)).onBluetoothLeServiceConnectionStateChanged(gatt, BluetoothLeService.ACTION_GATT_CONNECTED);
                        return;
                    }
                    return;
                }
                Log.e(BluetoothLeService.TAG, "Receive BluetoothProfile.STATE_CONNECTED but BluetoothGatt is null");
                gatt.disconnect();
            } else if (newState == 0) {
                intentAction = BluetoothLeService.ACTION_GATT_DISCONNECTED;
                BluetoothLeService.this.mConnectionState = 0;
                Log.i(BluetoothLeService.TAG, "Disconnected from GATT server.");
                BluetoothLeService.this.broadcastUpdate(intentAction);
                address = BluetoothLeService.this.getAddressFromBluetoothGatt(gatt);
                if (address != null && BluetoothLeService.this.mListener.containsKey(address)) {
                    ((BluetoothLeServiceListener) BluetoothLeService.this.mListener.get(address)).onBluetoothLeServiceConnectionStateChanged(gatt, BluetoothLeService.ACTION_GATT_DISCONNECTED);
                }
            }
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == 0) {
                BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
                String address = BluetoothLeService.this.getAddressFromBluetoothGatt(gatt);
                if (address != null && BluetoothLeService.this.mListener.containsKey(address)) {
                    ((BluetoothLeServiceListener) BluetoothLeService.this.mListener.get(address)).onServicesDiscovered();
                    return;
                }
                return;
            }
            Log.w(BluetoothLeService.TAG, "onServicesDiscovered received: " + status);
        }

        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            BluetoothLeService.this.isReadingCharacteristics = false;
            if (status == 0) {
                BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_DATA_AVAILABLE, characteristic);
                String address = BluetoothLeService.this.getAddressFromBluetoothGatt(gatt);
                if (address != null) {
                    BRBaseService service = BluetoothLeService.this.findService(characteristic.getService().getUuid().toString(), address);
                    if (service != null) {
                        service.notifyCharacteristicHandler(characteristic);
                    }
                }
            }
            BluetoothLeService.this.readNextCharacteristics();
        }

        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_DATA_AVAILABLE, characteristic);
            String address = BluetoothLeService.this.getAddressFromBluetoothGatt(gatt);
            if (address != null) {
                BRBaseService service = BluetoothLeService.this.findService(characteristic.getService().getUuid().toString(), address);
                if (service != null) {
                    service.notifyCharacteristicHandler(characteristic);
                }
            }
        }

        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == 0) {
                String address = BluetoothLeService.this.getAddressFromBluetoothGatt(gatt);
                if (address != null) {
                    BRBaseService service = BluetoothLeService.this.findService(characteristic.getService().getUuid().toString(), address);
                    if (service != null) {
                        BluetoothLeService.sIsWriting = false;
                        BluetoothLeService.this.writeCharacteristicHandler.removeCallbacks(BluetoothLeService.this.writeCharacteristicRunnable);
                        BluetoothLeService.this.nextWrite();
                        service.notifyCharacteristicHandler(characteristic);
                    }
                }
            }
        }

        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (status == 0) {
                String address = BluetoothLeService.this.getAddressFromBluetoothGatt(gatt);
                if (address != null) {
                    BRBaseService service = BluetoothLeService.this.findService(descriptor.getCharacteristic().getService().getUuid().toString(), address);
                    if (service != null) {
                        service.descriptorHandler(descriptor);
                    }
                }
            }
        }
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothLeService$6 */
    class C00056 implements Runnable {
        C00056() {
        }

        public void run() {
            BluetoothLeService.sIsWriting = false;
            BluetoothLeService.this.nextWrite();
            BluetoothLeService.this.writeCharacteristicHandler.removeCallbacks(BluetoothLeService.this.writeCharacteristicRunnable);
            BluetoothLeService.this.writeCharacteristicRunnable = null;
        }
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothLeService$7 */
    class C00067 implements Runnable {
        C00067() {
        }

        public void run() {
            BluetoothLeService.sIsWriting = false;
            BluetoothLeService.this.nextWrite();
            BluetoothLeService.this.writeCharacteristicHandler.removeCallbacks(BluetoothLeService.this.writeCharacteristicRunnable);
            BluetoothLeService.this.writeCharacteristicRunnable = null;
        }
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    private void broadcastUpdate(String action) {
        sendBroadcast(new Intent(action));
    }

    private void broadcastUpdate(String action, BluetoothGattCharacteristic characteristic) {
        Intent intent = new Intent(action);
        byte[] data = characteristic.getValue();
        if (data != null && data.length > 0) {
            StringBuilder stringBuilder = new StringBuilder(data.length);
            int length = data.length;
            for (int i = 0; i < length; i++) {
                stringBuilder.append(String.format("%02X ", new Object[]{Byte.valueOf(data[i])}));
            }
            intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
        }
        sendBroadcast(intent);
    }

    public void setBluetoothLeServiceListener(BluetoothLeServiceListener pListener, String address) {
        this.mListener.put(address, pListener);
    }

    public void removeBluetoothLeServiceListener(String address) {
        this.mListener.remove(address);
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "BluetoothLeService unbind");
        closeAll();
        return super.onUnbind(intent);
    }

    public boolean initialize() {
        if (this.mBluetoothManager == null) {
            this.mBluetoothManager = (BluetoothManager) getSystemService("bluetooth");
            if (this.mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }
        this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();
        if (this.mBluetoothAdapter != null) {
            return true;
        }
        Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
        return false;
    }

    public void setServiceDict(HashMap<String, BRBaseService> pServiceDict, String address) {
        this.mServiceDict.put(address, pServiceDict);
    }

    public BRBaseService findService(String pUuid, String address) {
        if (this.mServiceDict.containsKey(address)) {
            return (BRBaseService) ((HashMap) this.mServiceDict.get(address)).get(pUuid);
        }
        return null;
    }

    public boolean connect(final String address) {
        boolean previousConnectionExist = false;
        if (this.mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }
        final BluetoothDevice device = this.mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        final BluetoothLeService self = this;
        if (!(this.mBluetoothDeviceAddress == null || !containsBluetoothDeviceAddress(address) || this.mBluetoothGatt == null)) {
            previousConnectionExist = true;
        }
        Handler mainHandler = new Handler(getMainLooper());
        if (previousConnectionExist) {
            mainHandler.post(new Runnable() {
                public void run() {
                    Log.d(BluetoothLeService.TAG, "Trying to use an existing mBluetoothGatt for connection.");
                    if (BluetoothLeService.this.mBluetoothGatt != null) {
                        BluetoothGatt bluetoothGatt = (BluetoothGatt) BluetoothLeService.this.mBluetoothGatt.get(address);
                        if (bluetoothGatt != null) {
                            bluetoothGatt.close();
                            BluetoothLeService.this.mBluetoothGatt.remove(address);
                        }
                    }
                }
            });
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mainHandler.post(new Runnable() {
            public void run() {
                BluetoothGatt bluetoothGatt = device.connectGatt(self, false, BluetoothLeService.this.mGattCallback);
                if (bluetoothGatt != null) {
                    BluetoothLeService.this.mBluetoothGatt.put(address, bluetoothGatt);
                }
                Log.d(BluetoothLeService.TAG, "Trying to create a new connection.");
                if (!BluetoothLeService.this.containsBluetoothDeviceAddress(address)) {
                    BluetoothLeService.this.mBluetoothDeviceAddress.add(address);
                }
                BluetoothLeService.this.mConnectionState = 1;
            }
        });
        return true;
    }

    public void disconnect(final String address) {
        if (this.mBluetoothAdapter == null || !this.mBluetoothGatt.containsKey(address)) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        } else {
            new Handler(getMainLooper()).post(new Runnable() {
                public void run() {
                    if (BluetoothLeService.this.mBluetoothGatt != null && BluetoothLeService.this.mBluetoothGatt.get(address) != null) {
                        ((BluetoothGatt) BluetoothLeService.this.mBluetoothGatt.get(address)).disconnect();
                    }
                }
            });
        }
    }

    public void close(final String address) {
        if (this.mBluetoothGatt != null && this.mBluetoothGatt.containsKey(address)) {
            new Handler(getMainLooper()).post(new Runnable() {
                public void run() {
                    if (BluetoothLeService.this.mBluetoothGatt != null && BluetoothLeService.this.mBluetoothGatt.containsKey(address)) {
                        ((BluetoothGatt) BluetoothLeService.this.mBluetoothGatt.get(address)).close();
                        BluetoothLeService.this.mBluetoothGatt.remove(address);
                    }
                }
            });
        }
    }

    public void closeAll() {
        Iterator it = this.mBluetoothDeviceAddress.iterator();
        while (it.hasNext()) {
            close((String) it.next());
        }
    }

    public synchronized void writeCharacteristic(BluetoothGattCharacteristic characteristic, String address, byte[] chunk) {
        if (sWriteQueue.isEmpty() && sAddresQsueue.isEmpty() && sChunkQueue.isEmpty() && !sIsWriting) {
            doWrite(characteristic, address, chunk);
            if (this.writeCharacteristicRunnable == null) {
                this.writeCharacteristicRunnable = new C00056();
                this.writeCharacteristicHandler.postDelayed(this.writeCharacteristicRunnable, 2000);
            }
        } else {
            sWriteQueue.add(characteristic);
            sAddresQsueue.add(address);
            sChunkQueue.add(chunk);
            if (!sIsWriting) {
                nextWrite();
            }
        }
    }

    public synchronized void doWrite(BluetoothGattCharacteristic characteristic, String address, byte[] chunk) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        } else {
            BluetoothGatt bluetoothGatt = (BluetoothGatt) this.mBluetoothGatt.get(address);
            if (bluetoothGatt != null) {
                sIsWriting = true;
                characteristic.setValue(chunk);
                boolean writeCharacteristicResult = bluetoothGatt.writeCharacteristic(characteristic);
                if (!writeCharacteristicResult) {
                    int retryCount = 0;
                    while (!writeCharacteristicResult) {
                        if (retryCount >= 5) {
                            sIsWriting = false;
                            break;
                        }
                        retryCount++;
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        writeCharacteristicResult = bluetoothGatt.writeCharacteristic(characteristic);
                    }
                }
            }
        }
    }

    private synchronized void nextWrite() {
        if (this.writeCharacteristicRunnable != null) {
            this.writeCharacteristicHandler.removeCallbacks(this.writeCharacteristicRunnable);
            this.writeCharacteristicRunnable = null;
        }
        if (!(sWriteQueue.isEmpty() || sAddresQsueue.isEmpty() || sChunkQueue.isEmpty() || sIsWriting)) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            doWrite((BluetoothGattCharacteristic) sWriteQueue.poll(), (String) sAddresQsueue.poll(), (byte[]) sChunkQueue.poll());
            if (this.writeCharacteristicRunnable == null) {
                this.writeCharacteristicRunnable = new C00067();
                this.writeCharacteristicHandler.postDelayed(this.writeCharacteristicRunnable, 5000);
            }
        }
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic, String address) {
        this.readCharacteristicsQueue.add(characteristic);
        this.readCharacteristicsAddressQueue.add(address);
        readNextCharacteristics();
    }

    private void readNextCharacteristics() {
        final long remainTime = System.currentTimeMillis() - this.lastReadCharacteristicsTime;
        if (this.isReadingCharacteristics && remainTime > 2000) {
            this.isReadingCharacteristics = false;
        }
        if (this.isReadingCharacteristics) {
            new Thread() {

                /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothLeService$8$1 */
                class C00071 implements Runnable {
                    C00071() {
                    }

                    public void run() {
                        BluetoothLeService.this.performNextCharacteristics();
                    }
                }

                public void run() {
                    if (BluetoothLeService.this.readCharacteristicRunnable == null) {
                        BluetoothLeService.this.readCharacteristicRunnable = new C00071();
                    }
                    BluetoothLeService.this.readCharacteristicHandler.postDelayed(BluetoothLeService.this.readCharacteristicRunnable, remainTime);
                }
            }.start();
        } else {
            performNextCharacteristics();
        }
    }

    private void performNextCharacteristics() {
        if (this.readCharacteristicRunnable != null) {
            this.readCharacteristicHandler.removeCallbacks(this.readCharacteristicRunnable);
            this.readCharacteristicRunnable = null;
        }
        if (this.readCharacteristicsQueue.size() > 0) {
            this.isReadingCharacteristics = true;
            this.lastReadCharacteristicsTime = System.currentTimeMillis();
            BluetoothGattCharacteristic nextReadCharacteristics = (BluetoothGattCharacteristic) this.readCharacteristicsQueue.get(0);
            String address = (String) this.readCharacteristicsAddressQueue.get(0);
            this.readCharacteristicsQueue.remove(0);
            this.readCharacteristicsAddressQueue.remove(0);
            performReadCharacteristic(nextReadCharacteristics, address);
        }
    }

    private void performReadCharacteristic(BluetoothGattCharacteristic characteristic, String address) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        BluetoothGatt bluetoothGatt = (BluetoothGatt) this.mBluetoothGatt.get(address);
        if (bluetoothGatt != null && !bluetoothGatt.readCharacteristic(characteristic)) {
        }
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, String address, boolean enabled) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        BluetoothGatt bluetoothGatt = (BluetoothGatt) this.mBluetoothGatt.get(address);
        if (bluetoothGatt != null && characteristic != null) {
            boolean setCharacteristicNotificationResult = bluetoothGatt.setCharacteristicNotification(characteristic, enabled);
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(BluetoothRobotConstantsBase.kMipReceiveDataNotificationUUID));
            if (descriptor != null) {
                descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : new byte[]{(byte) 0, (byte) 0});
                bluetoothGatt.writeDescriptor(descriptor);
            }
        } else if (characteristic == null) {
            Log.w(TAG, "BluetoothLeServer set notifications: characteristic is null");
        }
    }

    public List<BluetoothGattService> getSupportedGattServices(String address) {
        if (this.mBluetoothGatt != null && this.mBluetoothGatt.containsKey(address)) {
            return ((BluetoothGatt) this.mBluetoothGatt.get(address)).getServices();
        }
        return null;
    }

    public BluetoothGattService getGattService(UUID uuid, String address) {
        if (this.mBluetoothGatt != null && this.mBluetoothGatt.containsKey(address)) {
            return ((BluetoothGatt) this.mBluetoothGatt.get(address)).getService(uuid);
        }
        return null;
    }

    public int getConnectionState() {
        return this.mConnectionState;
    }

    public boolean isConnected() {
        return this.mConnectionState == 2;
    }

    public BluetoothGatt getGatt(String address) {
        return (BluetoothGatt) this.mBluetoothGatt.get(address);
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    private boolean containsBluetoothDeviceAddress(String address) {
        Iterator it = this.mBluetoothDeviceAddress.iterator();
        while (it.hasNext()) {
            if (((String) it.next()).equalsIgnoreCase(address)) {
                return true;
            }
        }
        return false;
    }

    private String getAddressFromBluetoothGatt(BluetoothGatt gatt) {
        for (Entry<String, BluetoothGatt> e : this.mBluetoothGatt.entrySet()) {
            String key = (String) e.getKey();
            if (gatt.equals((BluetoothGatt) e.getValue())) {
                return key;
            }
        }
        return null;
    }
}
