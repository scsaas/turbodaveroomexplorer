package com.wowwee.bluetoothrobotcontrollib;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothLeService.LocalBinder;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService.BRServiceAction;
import com.wowwee.bluetoothrobotcontrollib.services.BRReceiveDataService;
import com.wowwee.bluetoothrobotcontrollib.services.BRSendDataService;
import com.wowwee.bluetoothrobotcontrollib.util.AdRecord;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class BluetoothRobot implements PropertyChangeListener {
    protected static ServiceConnection currentServiceConnection = null;
    protected static boolean isBluetoothLeServiceBinded = false;
    public static final int kBluetoothRobotStateConnected = 2;
    public static final int kBluetoothRobotStateConnecting = 1;
    public static final int kBluetoothRobotStateDisconnected = 0;
    public static final int kBluetoothRobotStateDisconnecting = 3;
    protected static BluetoothLeService mBluetoothLeService;
    protected int connectAttempts;
    public HashMap<Byte, Byte> customBroadcastData = null;
    protected int kBluetoothRobotState = 0;
    protected BluetoothDevice mBluetoothDevice = null;
    private BluetoothLeServiceListener mBluetoothLeServiceListener = new C00101();
    protected int mConnectAttempts = 0;
    protected Timer mConnectTimer;
    protected Context mContext;
    protected int mInitialBatteryLevel;
    protected int mInitialIOModes;
    protected int mInitialIOStates;
    protected int mInitialProductId;
    protected String mName;
    protected int mProductId;
    private final ServiceConnection mServiceConnection = new C00112();
    protected List<BluetoothGattService> mServicesReady;

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothRobot$1 */
    class C00101 implements BluetoothLeServiceListener {

        /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothRobot$1$1 */
        class C00091 implements Runnable {
            C00091() {
            }

            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ((BRReceiveDataService) BluetoothRobot.mBluetoothLeService.findService(BluetoothRobotConstantsBase.kMipReceiveDataServiceUUID, BluetoothRobot.this.mBluetoothDevice.getAddress())).turnOn();
                BluetoothRobot.this.peripheralDidBecomeReady();
            }
        }

        C00101() {
        }

        public void onBluetoothLeServiceConnectionStateChanged(BluetoothGatt gatt, String pAction) {
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(pAction)) {
                BluetoothRobot.this.peripheralDidConnect();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(pAction)) {
                BluetoothRobot.this.peripheralDidDisconnect();
            } else if (!BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(pAction) && BluetoothLeService.ACTION_DATA_AVAILABLE.equals(pAction)) {
            }
        }

        public void onServicesDiscovered() {
            BluetoothRobot.mBluetoothLeService.setServiceDict(BluetoothRobot.this.buildPeripheralServiceDict(BluetoothRobot.mBluetoothLeService), BluetoothRobot.this.mBluetoothDevice.getAddress());
            new Thread(new C00091()).start();
        }
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothRobot$2 */
    class C00112 implements ServiceConnection {
        C00112() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder service) {
            BluetoothRobot.currentServiceConnection = this;
            BluetoothRobot.mBluetoothLeService = ((LocalBinder) service).getService();
            BluetoothRobot.mBluetoothLeService.setContext(BluetoothRobot.this.mContext);
            if (BluetoothRobot.mBluetoothLeService.initialize()) {
                BluetoothRobot.this.connect();
            } else {
                BluetoothRobot.this.connect();
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.BluetoothRobot$3 */
    class C00123 extends TimerTask {
        C00123() {
        }

        public void run() {
            if (BluetoothRobot.this.kBluetoothRobotState == 1) {
                Log.d("BLE", "Reconnect timeout, disconnect now");
                BluetoothRobot.this.disconnect();
                BluetoothRobot.this.peripheralDidDisconnect();
            }
        }
    }

    public BluetoothRobot(BluetoothDevice pBluetoothDevice, List<AdRecord> pScanRecords, BluetoothLeService pBluetoothLeService) {
        this.mBluetoothDevice = pBluetoothDevice;
        this.mName = this.mBluetoothDevice.getName();
        if (mBluetoothLeService == null && pBluetoothLeService != null) {
            mBluetoothLeService = pBluetoothLeService;
        }
        AdRecord manufactorerData = null;
        for (AdRecord scanRecord : pScanRecords) {
            if (scanRecord.getType() == -1) {
                manufactorerData = scanRecord;
                break;
            }
        }
        if (manufactorerData != null) {
            byte[] mData = AdRecord.getRawData(manufactorerData);
            Log.d("BluetoothRobot", "mData[0] " + mData[0] + " mData[1] " + mData[1]);
            if (mData[0] != (byte) 0) {
                this.mInitialProductId = 65535;
            } else {
                this.mInitialProductId = (mData[0] << 8) | mData[1];
            }
            this.mInitialProductId = (mData[0] << 8) | mData[1];
            if (mData.length == 9) {
                this.mInitialBatteryLevel = mData[6];
                this.mInitialIOModes = mData[7];
                this.mInitialIOStates = mData[8];
                return;
            }
            if (this.customBroadcastData == null) {
                this.customBroadcastData = new HashMap();
            } else {
                this.customBroadcastData.clear();
            }
            for (int i = 2; i < mData.length; i++) {
                this.customBroadcastData.put(Byte.valueOf((byte) (i - 2)), Byte.valueOf(mData[i]));
            }
            return;
        }
        Log.d("BluetoothRobot", "ERROR: manufacturer data is null");
    }

    public HashMap<String, BRBaseService> buildPeripheralServiceDict(BluetoothLeService pBluetoothLeService) {
        HashMap<String, BRBaseService> serviceDict = new HashMap();
        serviceDict.put(BluetoothRobotConstantsBase.kMipSendDataServiceUUID, new BRSendDataService(pBluetoothLeService, this.mBluetoothDevice.getAddress()));
        serviceDict.put(BluetoothRobotConstantsBase.kMipReceiveDataServiceUUID, new BRReceiveDataService(pBluetoothLeService, this, this.mBluetoothDevice.getAddress()));
        return serviceDict;
    }

    public void disconnect() {
        this.kBluetoothRobotState = 3;
        mBluetoothLeService.disconnect(this.mBluetoothDevice.getAddress());
    }

    public void connect(Context context) {
        if (mBluetoothLeService == null) {
            startBluetoothLeService(context);
        } else {
            connect();
        }
    }

    public void connect() {
        if (mBluetoothLeService != null) {
            mBluetoothLeService.setServiceDict(buildPeripheralServiceDict(mBluetoothLeService), this.mBluetoothDevice.getAddress());
            mBluetoothLeService.setBluetoothLeServiceListener(this.mBluetoothLeServiceListener, this.mBluetoothDevice.getAddress());
            this.kBluetoothRobotState = 1;
            mBluetoothLeService.connect(this.mBluetoothDevice.getAddress());
            return;
        }
        Log.e("BluetoothRobot", "Failed to connect to BluetoothRobot: mBluetoothLeService = null");
    }

    protected void startBluetoothLeService(Context context) {
        if (!isBluetoothLeServiceBinded) {
            this.mContext = context;
            if (context.bindService(new Intent(context, BluetoothLeService.class), this.mServiceConnection, 1)) {
                Log.d("BLE", "Bind service success");
                isBluetoothLeServiceBinded = true;
                return;
            }
            Log.d("BLE", "Bind service failed");
        }
    }

    public void peripheralDidConnect() {
        this.kBluetoothRobotState = 2;
    }

    public void peripheralDidDisconnect() {
        if (this.kBluetoothRobotState == 2) {
            Log.d("BLE", "Try to reconnect...");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            connect();
            new Timer().schedule(new C00123(), 2000);
            return;
        }
        this.kBluetoothRobotState = 0;
        if (mBluetoothLeService != null) {
            mBluetoothLeService.disconnect(this.mBluetoothDevice.getAddress());
            mBluetoothLeService.close(this.mBluetoothDevice.getAddress());
            mBluetoothLeService.removeBluetoothLeServiceListener(this.mBluetoothDevice.getAddress());
        }
    }

    public static void unbindBluetoothLeService(Context context) {
        if (isBluetoothLeServiceBinded && context != null && currentServiceConnection != null) {
            context.unbindService(currentServiceConnection);
            isBluetoothLeServiceBinded = false;
            mBluetoothLeService = null;
            currentServiceConnection = null;
        }
    }

    public void peripheralDidBecomeReady() {
        this.kBluetoothRobotState = 2;
    }

    public void sendRobotCommand(RobotCommand robotCommand) {
        sendRobotCommand(robotCommand, null);
    }

    private void sendRobotCommand(RobotCommand robotCommand, BRServiceAction callback) {
        if (callback != null) {
            robotCommand.completedCallback = callback;
        }
        _processRobotCommand(robotCommand);
    }

    private void _processRobotCommand(RobotCommand robotCommand) {
        sendRawCommandData(robotCommand.data(), robotCommand.completedCallback);
    }

    protected void sendRawCommandData(byte[] data, BRServiceAction callback) {
        BRSendDataService service = (BRSendDataService) findService(BluetoothRobotConstantsBase.kMipSendDataServiceUUID);
        if (service != null) {
            service.sendData(data, callback);
        } else {
            Log.d("BluetoothRobot", "BluetoothRobot: This device does not support Send Data Service");
        }
    }

    protected BRBaseService findService(String pUUIDString) {
        Log.i("BluetoothRobot", "pUUIDString " + pUUIDString + " getGattService " + mBluetoothLeService.getGattService(UUID.fromString(pUUIDString), this.mBluetoothDevice.getAddress()));
        if (mBluetoothLeService.getGattService(UUID.fromString(pUUIDString), this.mBluetoothDevice.getAddress()) == null) {
            return null;
        }
        return mBluetoothLeService.findService(pUUIDString, this.mBluetoothDevice.getAddress());
    }

    public void propertyChange(PropertyChangeEvent event) {
        String propertyName = event.getPropertyName();
        Log.d("propertyChange", "Received propertyChange");
        if (propertyName.equals(BRReceiveDataService.commandKeyPathKVO)) {
            didReceiveRobotCommand((RobotCommand) event.getNewValue());
        }
    }

    public void didReceiveRobotCommand(RobotCommand robotCommand) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveRobotCommand");
    }

    public static UUID[] getAdvertisedServiceUUIDs() {
        return new UUID[]{UUID.fromString(BluetoothRobotConstantsBase.KMipPwmOutputServiceUUID), UUID.fromString(BluetoothRobotConstantsBase.KMipProgrammableIOServiceUUID)};
    }

    public BluetoothDevice getBluetoothDevice() {
        return this.mBluetoothDevice;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getProductId() {
        return this.mInitialProductId;
    }
}
