package com.wowwee.bluetoothrobotcontrollib;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.kModuleParameterUARTBaudRateValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.nuvotonBootloaderMode;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.nuvotonFirmwareCompleteStatus;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstants.nuvotonFirmwareStatus;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kMipModuleParameter_TransmitPowerValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kModuleParameterBTCommunicationIntervalValues;
import com.wowwee.bluetoothrobotcontrollib.BluetoothRobotConstantsBase.kModuleParameter_BroadcastPeriodValues;
//import com.wowwee.bluetoothrobotcontrollib.firmwareupdate.FirmwareUpdateCommand;
//import com.wowwee.bluetoothrobotcontrollib.firmwareupdate.FirmwareUpdateProtocol;
//import com.wowwee.bluetoothrobotcontrollib.firmwareupdate.FirmwareUpdateProtocol.FirmwareUploadFailureCallback;
//import com.wowwee.bluetoothrobotcontrollib.firmwareupdate.FirmwareUpdateProtocol.FirmwareUploadProgressCallback;
//import com.wowwee.bluetoothrobotcontrollib.firmwareupdate.FirmwareUpdateProtocol.FirmwareUploadSuccessCallback;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService;
import com.wowwee.bluetoothrobotcontrollib.services.BRBaseService.BRServiceAction;
import com.wowwee.bluetoothrobotcontrollib.services.BRBatteryLevelService;
import com.wowwee.bluetoothrobotcontrollib.services.BRDFUService;
import com.wowwee.bluetoothrobotcontrollib.services.BRDeviceInformationService;
import com.wowwee.bluetoothrobotcontrollib.services.BRModuleParametersService;
import com.wowwee.bluetoothrobotcontrollib.services.BRRSSIReportService;
import com.wowwee.bluetoothrobotcontrollib.services.BRReceiveDataService;
import com.wowwee.bluetoothrobotcontrollib.services.BRSendDataService;
import com.wowwee.bluetoothrobotcontrollib.services.BRSettingService;
//import com.wowwee.bluetoothrobotcontrollib.services.nuvotonbootloader.BRNuvotonBootloaderService;
import com.wowwee.bluetoothrobotcontrollib.util.AdRecord;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BluetoothRobotPrivate extends BluetoothRobot implements PropertyChangeListener {
    public static byte kActivation_Activate = (byte) 1;
    public static byte kActivation_ActivationSentToFlurry = (byte) 2;
    public static byte kActivation_FactoryDefault = (byte) 0;
    public static byte kActivation_HackerUartUsed = (byte) 4;
    public static byte kActivation_HackerUartUsedSentToFlurry = (byte) 8;
    public String bleModuleSoftwareVersion;
    public String bleSystemId;
    //public FirmwareUpdateProtocol firmwareUpdateProtocol;
    public Boolean isDFUSupported;
    private byte toyActivationStatus;

    public BluetoothRobotPrivate(BluetoothDevice pBluetoothDevice, List<AdRecord> pScanRecords, BluetoothLeService pBluetoothLeService) {
        super(pBluetoothDevice, pScanRecords, pBluetoothLeService);
        //this.firmwareUpdateProtocol = null;
        //this.firmwareUpdateProtocol = new FirmwareUpdateProtocol(this);
    }

    public HashMap<String, BRBaseService> buildPeripheralServiceDict(BluetoothLeService pBluetoothLeService) {
        HashMap<String, BRBaseService> serviceDict = super.buildPeripheralServiceDict(pBluetoothLeService);
        //serviceDict.put(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID, new BRNuvotonBootloaderService(pBluetoothLeService, this, this.mBluetoothDevice.getAddress()));
        serviceDict.put(BluetoothRobotConstants.kDeviceDFUServiceUUID, new BRDFUService(pBluetoothLeService, this, this.mBluetoothDevice.getAddress()));
        return serviceDict;
    }

    public void propertyChange(PropertyChangeEvent event) {
        String propertyName = event.getPropertyName();
        Log.d("BluetoothRobotPrivate", "Received propertyChange " + event.getPropertyName() + " nuvotonChipStatusKeyPathKVO true ?" + propertyName.equals("nuvotonChipStatus"));
        if (propertyName.equals(BRBatteryLevelService.batteryReadingKeyPathKVO)) {
            didReceiveBatteryUpdate(((Integer) event.getNewValue()).intValue());
        } else if (propertyName.equals("rssiLevel")) {
            didReceiveBluetoothRSSIUpdate(((Integer) event.getNewValue()).intValue());
        } else if (propertyName.equals(BRReceiveDataService.commandKeyPathKVO)) {
            didReceiveRobotCommand((RobotCommand) event.getNewValue());
        } else if (propertyName.equals(BRReceiveDataService.rawDataKeyPathKVO)) {
            didReceiveRawCommandData((byte[]) event.getNewValue());
        } else if (propertyName.equals("deviceName")) {
            String name = (String) event.getNewValue();
            didReceiveModuleDeviceName(name);
            this.mName = name;
        } else if (propertyName.equals("btCommInterval")) {
            didReceiveModuleBtTransmissionInterval((kModuleParameterBTCommunicationIntervalValues) event.getNewValue());
        } else if (propertyName.equals(BRModuleParametersService.uartBaudRateKeyPathKVO)) {
            didReceiveModuleUARTBuadRate((kModuleParameterUARTBaudRateValues) event.getNewValue());
        } else if (propertyName.equals(BRModuleParametersService.broadcastPeriodKeyPathKVO)) {
            didReceiveModuleBtBroadcastPeriod((kModuleParameter_BroadcastPeriodValues) event.getNewValue());
        } else if (propertyName.equals(BRModuleParametersService.productIdKeyPathKVO)) {
            didReceiveModuleProductId(((Short) event.getNewValue()).shortValue());
        } else if (propertyName.equals(BRModuleParametersService.transmitPowerKeyPathKVO)) {
            didReceiveModuleBtTransmitPower((kMipModuleParameter_TransmitPowerValues) event.getNewValue());
        } else if (propertyName.equals(BRModuleParametersService.customBroadcastDataKeyPathKVO)) {
            HashMap<Byte, Byte> customBroadcastData = (HashMap) event.getNewValue();
            didReceiveModuleBtCustomBroadcastData(customBroadcastData);
            this.customBroadcastData = customBroadcastData;
        } else if (propertyName.equals("systemId")) {
            String newSystemId = (String) event.getNewValue();
            this.bleSystemId = newSystemId;
            didReceiveDeviceSystemID(newSystemId);
        } else if (propertyName.equals(BRDeviceInformationService.softwareVersionKeyPathKVO)) {
            String newModuleVersion = (String) event.getNewValue();
            this.bleModuleSoftwareVersion = newModuleVersion;
            didReceiveDeviceSoftwareVersion(newModuleVersion);
        } else if (propertyName.equals("firmwareDataStatus")) {
            didReceiveFirmwareDataStatus((nuvotonFirmwareStatus) event.getNewValue());
        } else if (propertyName.equals(BRSettingService.activationStatusKeyPathKVO)) {
            this.toyActivationStatus = Integer.valueOf((String) event.getNewValue()).byteValue();
            didReceiveProductActivationStatus(this.toyActivationStatus);
        } else if (propertyName.equals("nuvotonChipStatus")) {
            didReceiveNuvotonChipStatus((nuvotonBootloaderMode) event.getNewValue());
        } else if (propertyName.equals("firmwareCompleteStatus")) {
            didReceiveFirmwareCompleteStatus((nuvotonFirmwareCompleteStatus) event.getNewValue());
        } else if (propertyName.equals("firmwareSentData")) {
           // sentdata = ((Integer) event.getNewValue()).intValue();
           // didReceiveFirmwareSentdata(sentdata);
            //Log.d("BluetoothRobotPrivate", "propertyName.equals(BRNuvotonBootloaderService.firmwareSentDataKeyPathKVO)" + sentdata);
        } else if (propertyName.equals("firmwareToChip")) {
           // sentdata = ((Integer) event.getNewValue()).intValue();
           // didReceiveFirmwareToChip(sentdata);
           // Log.d("BluetoothRobotPrivate", "propertyName.equals(BRNuvotonBootloaderService.firmwareToChipKeyPathKVO)" + sentdata);
        }
    }

    public void debugLog() {
    }

    public void getBluetoothBatteryLevel() {
        BRBatteryLevelService service = (BRBatteryLevelService) findService(BluetoothRobotConstants.kMipBatteryLevelServiceUUID);
        if (service != null) {
            service.readBatteryLevel();
        } else {
            Log.d("BluetoothRobotPrivate", "BluetoothRobot: This device does not support Battery Level Service");
        }
    }

    public void getBluetoothRssiReading() {
        BRRSSIReportService service = (BRRSSIReportService) findService(BluetoothRobotConstants.kMipRSSIReportServiceUUID);
        if (service != null) {
            service.readRSSI();
        } else {
            Log.d("BluetoothRobotPrivate", "BluetoothRobot: This device does not support RSSI Report Service");
        }
    }

    //public void sendFirmwareCommand(FirmwareUpdateCommand command, BRServiceAction callback) {
    //    sendRawCommandData(command.asciiBase64EncodedData, callback);
   // }

    public void sendRobotCommand(RobotCommand robotCommand) {
        sendRobotCommand(robotCommand, null);
    }

    public void sendRobotCommand(RobotCommand robotCommand, BRServiceAction callback) {
        if (callback != null) {
            robotCommand.completedCallback = callback;
        }
        _processRobotCommand(robotCommand);
    }

    private void _processRobotCommand(RobotCommand robotCommand) {
        sendRawCommandData(robotCommand.data(), robotCommand.completedCallback);
    }

    public void sendRawCommandData(byte[] data, BRServiceAction callback) {
        BRSendDataService service = (BRSendDataService) findService(BluetoothRobotConstantsBase.kMipSendDataServiceUUID);
        if (service != null) {
            service.sendData(data, callback);
        } else {
            Log.d("BluetoothRobotPrivate", "BluetoothRobot: This device does not support Send Data Service");
        }
    }

    public void getBluetoothDeviceName() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readBTDeviceName();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setBluetoothDeviceName(String newName) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setBTDeviceName(newName);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void getBluetoothCommunicationInterval() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readBTCommunicationInterval();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void getBluetoothUARTBuardRate() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readUartBaudRate();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setBluetoothUARTBuardRate(kModuleParameterUARTBaudRateValues value) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setUartBuadRate(value);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void restartBluetoothDevice() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.restartModule();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void factoryResetBluetoothDevice(boolean userDataOnly) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service == null) {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        } else if (userDataOnly) {
            service.restoreUserDataToFactorySettings();
        } else {
            service.restoreFullyFactorySettings();
        }
    }

    public void setBroadcastPeriod(kModuleParameter_BroadcastPeriodValues period) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setBroadcastPeriod(period);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void getBroadcastPeriod() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readBroadcastPeriod();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setProductIdentifier(short productIdentifier) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setProductIdentifier(productIdentifier);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void getProductIdentifier() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readProductIdentifier();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setTransmitPower(kMipModuleParameter_TransmitPowerValues transmitPower) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setTransmitPower(transmitPower);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void getTransmitPower() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readTransmitPower();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setBroadcastData(HashMap<Byte, Byte> broadcastData) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setBroadcastData(broadcastData);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setBroadcastDataToDefault() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setBroadcastDataToDefault();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void getBroadcastData() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readBroadcastData();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void forceModuleSleep() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.forceModuleSleep();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void saveCurrentIOOutputInputState() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.saveCurrentIOOutputInputState();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setStandbyPulsedSleepMode(boolean pulsedSleep) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setStandbyPulsedSleepMode(pulsedSleep);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void getStandbyPulsedSleepMode() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.readStandbyPulsedSleepMode();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void saveBroadcastDataFlash() {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.writeCurrentCustomBroadcastDataToFlash();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setReceiveDataNotificationsEnabled(boolean turnOn) {
        BRReceiveDataService receiveDataService = (BRReceiveDataService) findService(BluetoothRobotConstantsBase.kMipReceiveDataServiceUUID);
        if (receiveDataService == null) {
            Log.d("BluetoothRobotPrivate", "This device does not support Receive Data Service");
        } else if (turnOn) {
            Log.d("BluetoothRobotPrivate", "Turn receive data notify ON");
            receiveDataService.turnOn();
        } else {
            receiveDataService.turnOff();
        }
    }

    /*public nuvotonFirmwareStatus readFirmwareDataStatus_im() {
        Log.d("BluetoothRobotPrivate", BluetoothRobotConstants.f1x482e5f41);
        BRNuvotonBootloaderService service = (BRNuvotonBootloaderService) findService(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID);
        if (service != null) {
            Log.d("BluetoothRobotPrivate", "readFirmwareDataStatus service " + service);
            return service.readFirmwareDataStatus_im();
        }
        Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service readFirmwareDataStatus");
        return null;
    }

    public nuvotonBootloaderMode readNuvotonChipStatus_im() {
        Log.d("BluetoothRobotPrivate", "readNuvotonChipStatus_im");
        BRNuvotonBootloaderService service = (BRNuvotonBootloaderService) findService(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID);
        if (service != null) {
            Log.d("BluetoothRobotPrivate", "readNuvotonChipStatus service " + service);
            return service.readNuvotonChipStatus_im();
        }
        Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service readNuvotonChipStatus");
        return null;
    }*/

    /*public void stopTransfer() {
        Log.d("BluetoothRobotPrivate", "stopTransfer");
        BRNuvotonBootloaderService service = (BRNuvotonBootloaderService) findService(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID);
        if (service != null) {
            Log.d("BluetoothRobotPrivate", "stopTransfer service " + service);
            service.stopTransfer();
            return;
        }
        Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service stopTransfer");
    }

    public void writeFirmwareToNuvoton() {
        Log.d("BluetoothRobotPrivate", BluetoothRobotConstants.f8x2943a32);
        BRNuvotonBootloaderService service = (BRNuvotonBootloaderService) findService(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID);
        if (service != null) {
            Log.d("BluetoothRobotPrivate", "writeFirmwareToNuvoton service " + service);
            service.writeFirmwareToNuvoton();
            return;
        }
        Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service writeFirmwareToNuvoton");
    }

    public void writeNextPackets_im() {
        Log.d("BluetoothRobotPrivate", "writeNextPackets");
        BRNuvotonBootloaderService service = (BRNuvotonBootloaderService) findService(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID);
        if (service != null) {
            Log.d("BluetoothRobotPrivate", "writeNextPackets service " + service);
            service.writeNextPackets();
            return;
        }
        Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service writeNextPackets");
    }

    public void restartNuvotonChipToMode(nuvotonBootloaderMode mode) {
        Log.d("BluetoothRobotPrivate", "restartNuvotonChipToMode mode" + mode);
        BRNuvotonBootloaderService service = (BRNuvotonBootloaderService) findService(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID);
        if (service != null) {
            Log.d("BluetoothRobotPrivate", "restartNuvotonChipToMode service " + service);
            service.restartNuvotonChipToMode(mode);
            return;
        }
        Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service restartNuvotonChipToMode");
    }

    public void sendFirmwareDataToCache_im(byte[] data) {
        Log.d("BluetoothRobotPrivate", "sendFirmwareDataToCache");
        BRNuvotonBootloaderService service = (BRNuvotonBootloaderService) findService(BluetoothRobotConstants.kDeviceNuvotonBootloaderServiceUUID);
        if (service != null) {
            Log.d("BluetoothRobotPrivate", "sendFirmwareDataToCache service " + service);
            service.sendFirmwareDataToCache_im(data);
            return;
        }
        Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service sendFirmwareDataToCache");
    }

    public void didReceiveFirmwareSentdata(int sentdata) {
        Log.d("BluetoothRobotPrivate", "BluetoothRobotPrivate didReceiveFirmwareSentdata call interface call back");
    }

    public void didReceiveFirmwareToChip(int sentdata) {
        Log.d("BluetoothRobotPrivate", "BluetoothRobotPrivate didReceiveFirmwareToChip call interface call back");
    }*/

    public void getBluetoothModuleSystemID() {
        BRDeviceInformationService service = (BRDeviceInformationService) findService(BluetoothRobotConstants.kMipDeviceInformationServiceUUID);
        if (service != null) {
            service.readSystemId();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service");
        }
    }

    public void getBluetoothModuleSoftwareVersion() {
        BRDeviceInformationService service = (BRDeviceInformationService) findService(BluetoothRobotConstants.kMipDeviceInformationServiceUUID);
        if (service != null) {
            service.readModuleSoftwareVersion();
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Device Information Service");
        }
    }

    public void setFirmwareUpdateMode(boolean value) {
        BRReceiveDataService service = (BRReceiveDataService) findService(BluetoothRobotConstantsBase.kMipReceiveDataServiceUUID);
        if (service == null) {
            Log.d("BluetoothRobotPrivate", "eive Data Service");
        } else if (this.kBluetoothRobotState == 2) {
            service.firmwareUpdateMode = value;
        }
    }

    public boolean firmwareUpdateMode() {
        BRReceiveDataService service = (BRReceiveDataService) findService(BluetoothRobotConstantsBase.kMipReceiveDataServiceUUID);
        if (service == null) {
            Log.d("BluetoothRobotPrivate", "This device does not support Receive Data Service");
        } else if (this.kBluetoothRobotState == 2) {
            return service.firmwareUpdateMode;
        }
        return false;
    }

   /* public void firmwareUpload(byte[] data, Date firmwareDate, byte firmwareRevision, FirmwareUploadSuccessCallback successCallback, FirmwareUploadFailureCallback errorCallback, FirmwareUploadProgressCallback progressCallback) {
        if (data == null || data.length == 0) {
            Log.e("Bootloader", "Tried to update with blank data");
            if (errorCallback != null) {
                errorCallback.callbackAction();
            }
        } else if (this.firmwareUpdateProtocol != null) {
            Log.d("Bootloader", "updateFirmwareWithData length = " + data.length);
            this.firmwareUpdateProtocol.updateFirmwareWithData(data, firmwareDate, firmwareRevision, successCallback, errorCallback, progressCallback);
        } else {
            Log.d("Bootloader", "Firmware update is not supported");
        }
    }

    public void cancelFirmwareUpdate() {
        if (this.firmwareUpdateProtocol != null) {
            this.firmwareUpdateProtocol.cancelFirmwareUpdate();
        }
    }*/

    public void didReceiveBatteryUpdate(int batteryPercentage) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveBatteryUpdate");
    }

    public void didReceiveBluetoothRSSIUpdate(int rssi) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveBluetoothRSSIUpdate");
    }

    public void didReceiveRawCommandData(byte[] data) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveRawCommandData");
    }

    public void didReceiveRobotCommand(RobotCommand robotCommand) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveRobotCommand");
    }

    public void didReceiveModuleDeviceName(String deviceName) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveModuleDeviceName");
    }

    public void didReceiveModuleBtTransmissionInterval(kModuleParameterBTCommunicationIntervalValues interval) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveModuleBtTransmissionInterval");
    }

    public void didReceiveModuleUARTBuadRate(kModuleParameterUARTBaudRateValues rate) {
    }

    public void didReceiveModuleBtBroadcastPeriod(kModuleParameter_BroadcastPeriodValues period) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveModuleBtBroadcastPeriod");
    }

    public void didReceiveModuleProductId(short productId) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveModuleProductId");
    }

    public void didReceiveModuleBtTransmitPower(kMipModuleParameter_TransmitPowerValues transmitPower) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveModuleBtTransmitPower");
    }

    public void didReceiveModuleBtCustomBroadcastData(HashMap<Byte, Byte> hashMap) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveModuleBtCustomBroadcastData");
    }

    public void didReceiveDeviceSystemID(String systemId) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveDeviceSystemID");
    }

    public void didReceiveDeviceSoftwareVersion(String version) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveDeviceSoftwareVersion");
    }

    public void didReceiveNuvotonChipStatus(nuvotonBootloaderMode NuvotonChipstatus) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveNuvotonChipstatus " + NuvotonChipstatus + " - ");
    }

    public void didReceiveFirmwareCompleteStatus(nuvotonFirmwareCompleteStatus NuvotonFirmwareCompleteStatus) {
        Log.w("BluetoothRobotPrivate", "should override nuvotonFirmwareCompleteStatus " + NuvotonFirmwareCompleteStatus + " - ");
    }

    public void didReceiveFirmwareDataStatus(nuvotonFirmwareStatus FirmwareDataStatus) {
        Log.w("BluetoothRobotPrivate", "should override didReceiveFirmwareDataStatus " + FirmwareDataStatus + " - ");
    }

    public void didReceiveProductActivationStatus(byte status) {
    }

    private void writeProductActivation(int activationType) {
        Log.d("BluetoothRobotPrivate", "writeActivationStatus");
        BRSettingService service = (BRSettingService) findService(BluetoothRobotConstants.kSettingServiceUUID);
        if (service != null) {
            service.writeProductActivationStatus(activationType);
        } else {
            Log.d("BluetoothRobotPrivate", "BluetoothRobot: This device does not support writeActivationStatus");
        }
    }

    public void resetProductActivationStatus() {
        this.toyActivationStatus = kActivation_FactoryDefault;
        writeProductActivation(this.toyActivationStatus);
    }

    public void setProductActivated() {
        this.toyActivationStatus = kActivation_Activate;
        writeProductActivation(this.toyActivationStatus);
    }

    public void setProductActivationUploaded() {
        this.toyActivationStatus = kActivation_ActivationSentToFlurry;
        writeProductActivation(this.toyActivationStatus);
    }

    public void getProductActivationStatus() {
        Log.d("BluetoothRobotPrivate", "readActivationStatus");
        BRSettingService service = (BRSettingService) findService(BluetoothRobotConstants.kSettingServiceUUID);
        if (service != null) {
            service.readProductActivationStatus();
        } else {
            Log.d("BluetoothRobotPrivate", "BluetoothRobot: This device does not support readActivationStatus");
        }
    }

    public void setConnectedBroadcastData(byte[] data) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setConnectedBroadcastData(data);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void setConnectedBroadcastOn(boolean isOn) {
        BRModuleParametersService service = (BRModuleParametersService) findService(BluetoothRobotConstants.kMipModuleParametersServiceUUID);
        if (service != null) {
            service.setConnectedBroadcastOn(isOn);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support Module Parameters Service");
        }
    }

    public void nordicRebootToMode(byte mode) {
        BRDFUService service = (BRDFUService) findService(BluetoothRobotConstants.kDeviceDFUServiceUUID);
        if (service != null) {
            service.rebootToMode(mode);
        } else {
            Log.d("BluetoothRobotPrivate", "This device does not support DFU Service");
        }
    }

    public void checkDFU() {
        Log.d("BluetoothRobotPrivate", "checkDFU()");
        BRDFUService service = (BRDFUService) findService(BluetoothRobotConstants.kDeviceDFUServiceUUID);
        Log.d("BluetoothRobotPrivate", "checkDFU()" + service);
        if (service != null) {
            this.isDFUSupported = Boolean.valueOf(true);
            Log.d("BluetoothRobotPrivate", "isDFUSupported = true");
            return;
        }
        this.isDFUSupported = Boolean.valueOf(false);
        Log.d("BluetoothRobotPrivate", "isDFUSupported = false");
    }
}
