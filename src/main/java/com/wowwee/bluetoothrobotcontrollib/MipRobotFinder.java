package com.wowwee.bluetoothrobotcontrollib;

import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;
//import com.wowwee.bluetoothrobotcontrollib.minionmip.MinionMipRobot;
import com.wowwee.bluetoothrobotcontrollib.util.AdRecord;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MipRobotFinder extends BluetoothRobotFinder {
    public static final int MRFScanOptionMask_FilterByDeviceName = 4;
    public static final int MRFScanOptionMask_FilterByProductId = 1;
    public static final int MRFScanOptionMask_FilterByServices = 2;
    public static final int MRFScanOptionMask_ShowAllDevices = 0;
    public static final String MipRobotFinder_BluetoothError = "com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_BluetoothError";
    public static final String MipRobotFinder_BluetoothIsOff = "com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_BluetoothIsOff";
    public static final String MipRobotFinder_MipFound = "com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipFound";
    public static final String MipRobotFinder_MipListCleared = "com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipListCleared";
    public static final String MipRobotFinder_MipPairedFound = "com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipPairedFound";
    private static MipRobotFinder instance = null;
    private LeScanCallback mLeScanCallback = new C00174();
    private List<MipRobot> mMipsConnected = new ArrayList();
    private List<MipRobot> mMipsFound = new ArrayList();
    private List<Integer> mProductIDFilter = new ArrayList();
    private int mScanOptionsFlagMask = 1;
    private ArrayList<BluetoothDevice> pairedDeviceList;

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.MipRobotFinder$1 */
    class C00141 implements Runnable {
        C00141() {
        }

        public void run() {
            MipRobotFinder.this.stopScanForMips();
        }
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.MipRobotFinder$2 */
    class C00152 implements Runnable {
        C00152() {
        }

        public void run() {
            MipRobotFinder.this.stopScanForMips();
        }
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.MipRobotFinder$3 */
    class C00163 implements Runnable {
        C00163() {
        }

        public void run() {
            MipRobotFinder.this.stopScanForMips();
        }
    }

    /* renamed from: com.wowwee.bluetoothrobotcontrollib.MipRobotFinder$4 */
    class C00174 implements LeScanCallback {
        C00174() {
        }

        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            MipRobotFinder.this.handleFoundBluetoothDevice(device, scanRecord, rssi);
        }
    }

    protected MipRobotFinder() {
    }

    public static MipRobotFinder getInstance() {
        if (instance == null) {
            instance = new MipRobotFinder();
        }
        return instance;
    }

    public static IntentFilter getMipRobotFinderIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipFound");
        intentFilter.addAction("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipListCleared");
        intentFilter.addAction("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_BluetoothError");
        intentFilter.addAction(MipRobotFinder_BluetoothIsOff);
        intentFilter.addAction("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipPairedFound");
        return intentFilter;
    }

    public void mipRobotDidConnect(MipRobot mip) {
        if (mip != null && !this.mMipsConnected.contains(mip)) {
            this.mMipsConnected.add(mip);
        }
    }

    public void mipRobotDidDisconnect(MipRobot mip) {
        if (mip != null) {
            this.mMipsConnected.remove(mip);
            this.mMipsFound.remove(mip);
        }
    }

    public void scanForMips() {
        this.mProductIDFilter.clear();
        this.mProductIDFilter.add(Integer.valueOf(5));
        this.mProductIDFilter.add(Integer.valueOf(55));
        startScan();
    }

    public void scanForMipsForDuration(int pSeconds) {
        this.mProductIDFilter.clear();
        this.mProductIDFilter.add(Integer.valueOf(5));
        this.mProductIDFilter.add(Integer.valueOf(55));
        startScan();
        new Handler().postDelayed(new C00141(), (long) (pSeconds * 1000));
    }

    public void scanForMinions() {
        this.mProductIDFilter.clear();
        this.mProductIDFilter.add(Integer.valueOf(68));
        startScan();
    }

    public void scanForMinionsForDuration(int pSeconds) {
        this.mProductIDFilter.clear();
        this.mProductIDFilter.add(Integer.valueOf(68));
        startScan();
        new Handler().postDelayed(new C00152(), (long) (pSeconds * 1000));
    }

    public void scanForAllRobots() {
        this.mProductIDFilter.clear();
        this.mProductIDFilter.add(Integer.valueOf(5));
        this.mProductIDFilter.add(Integer.valueOf(55));
        this.mProductIDFilter.add(Integer.valueOf(68));
        startScan();
    }

    public void scanForAllRobotsForDuration(int pSeconds) {
        this.mProductIDFilter.clear();
        this.mProductIDFilter.add(Integer.valueOf(5));
        this.mProductIDFilter.add(Integer.valueOf(55));
        this.mProductIDFilter.add(Integer.valueOf(68));
        startScan();
        new Handler().postDelayed(new C00163(), (long) (pSeconds * 1000));
    }

    public void stopScanForMips() {
        if (this.mBluetoothAdapter != null) {
            this.mBluetoothAdapter.stopLeScan(this.mLeScanCallback);
        }
    }

    public MipRobot firstConnectedMip() {
        if (this.mMipsConnected.size() > 0) {
            return (MipRobot) this.mMipsConnected.get(0);
        }
        return null;
    }

    public void clearFoundMipList() {
        List<MipRobot> mipsToRemove = new ArrayList();
        for (MipRobot mip : this.mMipsFound) {
            if (!this.mMipsConnected.contains(mip)) {
                mipsToRemove.add(mip);
            }
        }
        for (MipRobot mipToRemove : mipsToRemove) {
            this.mMipsFound.remove(mipToRemove);
        }
        this.mContext.sendBroadcast(new Intent("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipListCleared"));
    }

    public MipRobot findMip(BluetoothDevice pDevice) {
        for (MipRobot mip : this.mMipsFound) {
            if (mip.getBluetoothDevice().equals(pDevice)) {
                return mip;
            }
        }
        return null;
    }

    public List<MipRobot> getMipsFoundList() {
        return this.mMipsFound;
    }

    public List<MipRobot> getMipsConnected() {
        return this.mMipsConnected;
    }

    private void startScan() {
        if (this.mBluetoothAdapter == null) {
            Log.e("BLERobotControlLib", "Could not start scan, bluetooth adapter is null. Your device may not support Bluetooth LE");
            this.mContext.sendBroadcast(new Intent("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_BluetoothError"));
        } else if (this.mBluetoothAdapter.isEnabled()) {
            Log.d("MipRobotFinder", "BluetoothAdapter is ENABLE");
            if ((this.mScanOptionsFlagMask & 2) != 0) {
                this.mBluetoothAdapter.startLeScan(BluetoothRobot.getAdvertisedServiceUUIDs(), this.mLeScanCallback);
            } else {
                this.mBluetoothAdapter.startLeScan(this.mLeScanCallback);
            }
            getPairedMip();
        } else {
            Log.d("MipRobotFinder", "BluetoothAdapter is DISABLE");
            this.mContext.sendBroadcast(new Intent(MipRobotFinder_BluetoothIsOff));
        }
    }

    private void getPairedMip() {
        if (this.mBluetoothAdapter != null) {
            Set<BluetoothDevice> paired = this.mBluetoothAdapter.getBondedDevices();
            if (paired != null && paired.size() != 0) {
                new ArrayList().addAll(paired);
                this.pairedDeviceList = new ArrayList();
                for (BluetoothDevice bluetoothDevice : paired) {
                    if (bluetoothDevice.getName() != null && bluetoothDevice.getType() == 2 && bluetoothDevice.getName().toLowerCase().contains("mip")) {
                        this.pairedDeviceList.add(bluetoothDevice);
                    }
                }
                if (this.pairedDeviceList.size() > 0) {
                    Intent intent = new Intent("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipPairedFound");
                    intent.putExtra("PairedBluetoothDevices", this.pairedDeviceList);
                    this.mContext.sendBroadcast(intent);
                }
            }
        }
    }

    private void handleFoundBluetoothDevice(BluetoothDevice device, byte[] advertisingData, int rssi) {
        if (!isBluetoothDeviceExist(device)) {
            List<AdRecord> records = AdRecord.parseScanRecord(advertisingData);
            MipRobot robot = new MipRobot(device, records, null);
            robot.debugLog();
            Log.d("MipRobotFinder", "robot.getProductId() " + robot.getProductId() + " robot.getName() " + robot.getName());
            boolean filterByProductId = (this.mScanOptionsFlagMask & 1) != 0;
            boolean getFiltered = true;
            for (Integer pID : this.mProductIDFilter) {
                if (robot.getProductId() == pID.intValue()) {
                    getFiltered = false;
                    break;
                }
            }
            if (!filterByProductId || !getFiltered) {
                if (robot.getProductId() == 68) {
                    robot = new MipRobot(device, records, null);
                    robot.mipRobotType = MipRobot.MipType_MinionMip;
                } else {
                    robot.mipRobotType = robot.getProductId() == 55 ? MipRobot.MipType_CoderMip : MipRobot.MipType_Mip;
                }
                if (robot.mName == null) {
                    robot.setName("UNKNOW DEVICE");
                }
                if (!((this.mScanOptionsFlagMask & 4) != 0) || robot.getName().contains("WW-MIP")) {
                    int i = (robot.getProductId() == 55 || robot.getProductId() == 5 || robot.getProductId() == 68) ? 0 : 1;
                    if (((robot.getProductId() != 0 ? 1 : 0) & ((robot.getProductId() != 65535 ? 1 : 0) & i)) == 0) {
                        if (robot.mName == null) {
                            robot.setName("UNKNOW DEVICE");
                        }
                        this.mMipsFound.add(robot);
                        Intent intent = new Intent("com.wowwee.bluetoothrobotcontrollib.MipRobotFinder_MipFound");
                        intent.putExtra("BluetoothDevice", robot.getBluetoothDevice());
                        this.mContext.sendBroadcast(intent);
                    }
                }
            }
        }
    }

    private boolean isBluetoothDeviceExist(BluetoothDevice pDevice) {
        for (MipRobot mip : this.mMipsFound) {
            if (mip.getBluetoothDevice().equals(pDevice)) {
                return true;
            }
        }
        return false;
    }

    public int getmScanOptionsFlagMask() {
        return this.mScanOptionsFlagMask;
    }

    public void setmScanOptionsFlagMask(int mScanOptionsFlagMask) {
        this.mScanOptionsFlagMask = mScanOptionsFlagMask;
    }
}
