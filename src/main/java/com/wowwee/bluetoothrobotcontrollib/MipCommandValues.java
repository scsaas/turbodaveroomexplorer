package com.wowwee.bluetoothrobotcontrollib;

public class MipCommandValues {
    public static final int MIP_BLUETOOTH_PRODUCT_ID = 5;
    public static final int MIP_CODER_BLUETOOTH_PRODUCT_ID = 55;
    public static byte kMipActivation_Activate = (byte) 1;
    public static byte kMipActivation_ActivationSentToFlurry = (byte) 2;
    public static byte kMipActivation_FactoryDefault = (byte) 0;
    public static byte kMipActivation_HackerUartUsed = (byte) 4;
    public static byte kMipActivation_HackerUartUsedSentToFlurry = (byte) 8;
    public static byte kMipBroadcastDataAvatarIcon = (byte) 0;
    public static byte kMipCheckBootmode = (byte) -1;
    public static byte kMipClapDetectionStatus = (byte) 31;
    public static byte kMipClapsDetected = (byte) 29;
    public static byte kMipCurrentGameMode = (byte) -126;
    public static byte kMipDetectionMode_Off = (byte) 0;
    public static byte kMipDriveBackwardWithTime = (byte) 114;
    public static byte kMipDriveCont_BW_Speed1 = (byte) 32;
    public static byte kMipDriveCont_FW_Speed1 = (byte) 0;
    public static byte kMipDriveCont_Left_Speed1 = (byte) 96;
    public static byte kMipDriveCont_Right_Speed1 = (byte) 64;
    public static byte kMipDriveForwardWithTime = (byte) 113;
    public static byte kMipDrive_Continuous = (byte) 120;
    public static byte kMipDrive_DriveDirectionBackward = (byte) 1;
    public static byte kMipDrive_DriveDirectionForward = (byte) 0;
    public static byte kMipDrive_FixedDistance = (byte) 112;
    public static byte kMipDrive_TurnDirectionAnticlockwise = (byte) 1;
    public static byte kMipDrive_TurnDirectionClockwise = (byte) 0;
    public static byte kMipFlashChestRGBLed = (byte) -119;
    public static byte kMipGameMode_App = (byte) 0;
    public static byte kMipGameMode_Cage = (byte) 1;
    public static byte kMipGameMode_Dance = (byte) 3;
    public static byte kMipGameMode_DefaultMipMode = (byte) 4;
    public static byte kMipGameMode_RoamMode = (byte) 7;
    public static byte kMipGameMode_Stack = (byte) 5;
    public static byte kMipGameMode_Tracking = (byte) 2;
    public static byte kMipGameMode_Trick = (byte) 6;
    public static byte kMipGestureMode_Off = (byte) 0;
    public static byte kMipGestureMode_On = (byte) 2;
    public static byte kMipGesture_Hold = (byte) 14;
    public static byte kMipGesture_Left = (byte) 10;
    public static byte kMipGesture_PushBackward = (byte) 8;
    public static byte kMipGesture_PushDown1 = (byte) 33;
    public static byte kMipGesture_PushDown2 = (byte) 34;
    public static byte kMipGesture_PushDown3 = (byte) 35;
    public static byte kMipGesture_PushDown4 = (byte) 36;
    public static byte kMipGesture_PushDownHold = (byte) 32;
    public static byte kMipGesture_PushDownOver4 = (byte) 37;
    public static byte kMipGesture_PushForward = (byte) 9;
    public static byte kMipGesture_Right = (byte) 11;
    public static byte kMipGesture_SweepCenterLeft = (byte) 12;
    public static byte kMipGesture_SweepCenterRight = (byte) 13;
    public static byte kMipGesture_SweepLeftRightLeft = (byte) 19;
    public static byte kMipGesture_SweepLeftRightLeftRight = (byte) 17;
    public static byte kMipGesture_SweepRightLeftRight = (byte) 20;
    public static byte kMipGesture_SweepRightLeftRightLeft = (byte) 18;
    public static byte kMipGesture_back = (byte) 16;
    public static byte kMipGesture_forward = (byte) 15;
    public static byte kMipGetChestRGBLed = (byte) -125;
    public static byte kMipGetClapDetectionStatus = (byte) 30;
    public static byte kMipGetDetectionMode = (byte) 15;
    public static byte kMipGetGameMode = (byte) 120;
    public static byte kMipGetGestureMode = (byte) 11;
    public static byte kMipGetHackerUartMode = (byte) 2;
    public static byte kMipGetHardwareVersion = (byte) 25;
    public static byte kMipGetHeadLed = (byte) -117;
    public static byte kMipGetIRRemoteEnabledDisabled = (byte) 17;
    public static byte kMipGetMipDetectionMode = (byte) 15;
    public static byte kMipGetOdometer = (byte) -123;
    public static byte kMipGetRadarMode = (byte) 13;
    public static byte kMipGetSoftwareVersion = (byte) 20;
    public static byte kMipGetStatus = (byte) 121;
    public static byte kMipGetToyActivatedStatus = (byte) 33;
    public static byte kMipGetUpFromPosition = (byte) 35;
    public static byte kMipGetUserData = (byte) 19;
    public static byte kMipGetVolumeLevel = (byte) 22;
    public static byte kMipGetWeightLevel = (byte) -127;
    public static byte kMipHackerUartConnectedStatusUpdated = (byte) 28;
    public static byte kMipOtherMipDetected = (byte) 4;
    public static byte kMipPlaySound = (byte) 6;
    public static byte kMipPositionBackWithKickstand = (byte) 6;
    public static byte kMipPositionFaceDown = (byte) 1;
    public static byte kMipPositionFaceDownTray = (byte) 5;
    public static byte kMipPositionHandStand = (byte) 4;
    public static byte kMipPositionOnBack = (byte) 0;
    public static byte kMipPositionPickedUp = (byte) 3;
    public static byte kMipPositionUpright = (byte) 2;
    public static byte kMipRadarMode_Off = (byte) 0;
    public static byte kMipRadarMode_On = (byte) 4;
    public static byte kMipRadarResponseClear = (byte) 1;
    public static byte kMipRadarResponseObjectsIn10Cm = (byte) 3;
    public static byte kMipRadarResponseObjectsIn30Cm = (byte) 2;
    public static byte kMipReboot = (byte) -5;
    public static byte kMipReceiveIRCommand = (byte) 3;
    public static byte kMipResetOdometer = (byte) -122;
    public static byte kMipSendWowWeeIRDongleCode = (byte) -116;
    public static byte kMipSetChestRGBLed = (byte) -124;
    public static byte kMipSetClapDetectTiming = (byte) 32;
    public static byte kMipSetDetectionModeOrDetectionResponse = (byte) 14;
    public static byte kMipSetGameMode = (byte) 118;
    public static byte kMipSetGestureModeOrGestureDetected = (byte) 10;
    public static byte kMipSetHackerUartMode = (byte) 1;
    public static byte kMipSetHeadLed = (byte) -118;
    public static byte kMipSetIRRemoteEnabledDisabled = (byte) 16;
    public static byte kMipSetMipDetectionModeActivated = (byte) 14;
    public static byte kMipSetRadarModeOrRadarResponse = (byte) 12;
    public static byte kMipSetToyActivatedStatus = (byte) 34;
    public static byte kMipSetUserData = (byte) 18;
    public static byte kMipSetVolumeLevel = (byte) 21;
    public static byte kMipShakeDetected = (byte) 26;
    public static byte kMipShouldDisconnectAppMode = (byte) -2;
    public static byte kMipShouldFallover = (byte) 8;
    public static byte kMipShouldForceBLEDisconnect = (byte) -4;
    public static byte kMipShouldPowerOff = (byte) -5;
    public static byte kMipShouldSleep = (byte) -6;
    public static byte kMipSoundFile_ACTION_BURPING = (byte) 2;
    public static byte kMipSoundFile_ACTION_DRINKING = (byte) 3;
    public static byte kMipSoundFile_ACTION_EATING = (byte) 4;
    public static byte kMipSoundFile_ACTION_FARTING_SHORT = (byte) 5;
    public static byte kMipSoundFile_ACTION_OUT_OF_BREATH = (byte) 6;
    public static byte kMipSoundFile_BOXING_PUNCHCONNECT_1 = (byte) 7;
    public static byte kMipSoundFile_BOXING_PUNCHCONNECT_2 = (byte) 8;
    public static byte kMipSoundFile_BOXING_PUNCHCONNECT_3 = (byte) 9;
    public static byte kMipSoundFile_FREESTYLE_TRACKING_1 = (byte) 10;
    public static byte kMipSoundFile_FREESTYLE_TRACKING_2 = (byte) 106;
    public static byte kMipSoundFile_MIP_1 = (byte) 11;
    public static byte kMipSoundFile_MIP_2 = (byte) 12;
    public static byte kMipSoundFile_MIP_3 = (byte) 13;
    public static byte kMipSoundFile_MIP_APP = (byte) 14;
    public static byte kMipSoundFile_MIP_AWWW = (byte) 15;
    public static byte kMipSoundFile_MIP_BIG_SHOT = (byte) 16;
    public static byte kMipSoundFile_MIP_BLEH = (byte) 17;
    public static byte kMipSoundFile_MIP_BOOM = (byte) 18;
    public static byte kMipSoundFile_MIP_BYE = (byte) 19;
    public static byte kMipSoundFile_MIP_CONVERSE_1 = (byte) 20;
    public static byte kMipSoundFile_MIP_CONVERSE_2 = (byte) 21;
    public static byte kMipSoundFile_MIP_DROP = (byte) 22;
    public static byte kMipSoundFile_MIP_DUNNO = (byte) 23;
    public static byte kMipSoundFile_MIP_FALL_OVER_1 = (byte) 24;
    public static byte kMipSoundFile_MIP_FALL_OVER_2 = (byte) 25;
    public static byte kMipSoundFile_MIP_FIGHT = (byte) 26;
    public static byte kMipSoundFile_MIP_GAME = (byte) 27;
    public static byte kMipSoundFile_MIP_GLOAT = (byte) 28;
    public static byte kMipSoundFile_MIP_GO = (byte) 29;
    public static byte kMipSoundFile_MIP_GOGOGO = (byte) 30;
    public static byte kMipSoundFile_MIP_GRUNT_1 = (byte) 31;
    public static byte kMipSoundFile_MIP_GRUNT_2 = (byte) 32;
    public static byte kMipSoundFile_MIP_GRUNT_3 = (byte) 33;
    public static byte kMipSoundFile_MIP_HAHA_GOT_IT = (byte) 34;
    public static byte kMipSoundFile_MIP_HI_CONFIDENT = (byte) 35;
    public static byte kMipSoundFile_MIP_HI_NOT_SURE = (byte) 36;
    public static byte kMipSoundFile_MIP_HI_SCARED = (byte) 37;
    public static byte kMipSoundFile_MIP_HUH = (byte) 38;
    public static byte kMipSoundFile_MIP_HUMMING_1 = (byte) 39;
    public static byte kMipSoundFile_MIP_HUMMING_2 = (byte) 40;
    public static byte kMipSoundFile_MIP_HURT = (byte) 41;
    public static byte kMipSoundFile_MIP_HUUURGH = (byte) 42;
    public static byte kMipSoundFile_MIP_IN_LOVE = (byte) 43;
    public static byte kMipSoundFile_MIP_IT = (byte) 44;
    public static byte kMipSoundFile_MIP_JOKE = (byte) 45;
    public static byte kMipSoundFile_MIP_K = (byte) 46;
    public static byte kMipSoundFile_MIP_LOOP_1 = (byte) 47;
    public static byte kMipSoundFile_MIP_LOOP_2 = (byte) 48;
    public static byte kMipSoundFile_MIP_LOW_BATTERY = (byte) 49;
    public static byte kMipSoundFile_MIP_MIPPEE = (byte) 50;
    public static byte kMipSoundFile_MIP_MORE = (byte) 51;
    public static byte kMipSoundFile_MIP_MUAH_HA = (byte) 52;
    public static byte kMipSoundFile_MIP_MUSIC = (byte) 53;
    public static byte kMipSoundFile_MIP_OBSTACLE = (byte) 54;
    public static byte kMipSoundFile_MIP_OHOH = (byte) 55;
    public static byte kMipSoundFile_MIP_OH_YEAH = (byte) 56;
    public static byte kMipSoundFile_MIP_OOPSIE = (byte) 57;
    public static byte kMipSoundFile_MIP_OUCH_1 = (byte) 58;
    public static byte kMipSoundFile_MIP_OUCH_2 = (byte) 59;
    public static byte kMipSoundFile_MIP_PLAY = (byte) 60;
    public static byte kMipSoundFile_MIP_PUSH = (byte) 61;
    public static byte kMipSoundFile_MIP_RUN = (byte) 62;
    public static byte kMipSoundFile_MIP_SHAKE = (byte) 63;
    public static byte kMipSoundFile_MIP_SIGH = (byte) 64;
    public static byte kMipSoundFile_MIP_SINGING = (byte) 65;
    public static byte kMipSoundFile_MIP_SNEEZE = (byte) 66;
    public static byte kMipSoundFile_MIP_SNORE = (byte) 67;
    public static byte kMipSoundFile_MIP_STACK = (byte) 68;
    public static byte kMipSoundFile_MIP_SWIPE_1 = (byte) 69;
    public static byte kMipSoundFile_MIP_SWIPE_2 = (byte) 70;
    public static byte kMipSoundFile_MIP_TRICKS = (byte) 71;
    public static byte kMipSoundFile_MIP_TRIIICK = (byte) 72;
    public static byte kMipSoundFile_MIP_TRUMPET = (byte) 73;
    public static byte kMipSoundFile_MIP_WAAAAA = (byte) 74;
    public static byte kMipSoundFile_MIP_WAKEY = (byte) 75;
    public static byte kMipSoundFile_MIP_WHEEE = (byte) 76;
    public static byte kMipSoundFile_MIP_WHISTLING = (byte) 77;
    public static byte kMipSoundFile_MIP_WHOAH = (byte) 78;
    public static byte kMipSoundFile_MIP_WOO = (byte) 79;
    public static byte kMipSoundFile_MIP_YEAH = (byte) 80;
    public static byte kMipSoundFile_MIP_YEEESSS = (byte) 81;
    public static byte kMipSoundFile_MIP_YO = (byte) 82;
    public static byte kMipSoundFile_MIP_YUMMY = (byte) 83;
    public static byte kMipSoundFile_MOOD_ACTIVATED = (byte) 84;
    public static byte kMipSoundFile_MOOD_ANGRY = (byte) 85;
    public static byte kMipSoundFile_MOOD_ANXIOUS = (byte) 86;
    public static byte kMipSoundFile_MOOD_BORING = (byte) 87;
    public static byte kMipSoundFile_MOOD_CRANKY = (byte) 88;
    public static byte kMipSoundFile_MOOD_ENERGETIC = (byte) 89;
    public static byte kMipSoundFile_MOOD_EXCITED = (byte) 90;
    public static byte kMipSoundFile_MOOD_GIDDY = (byte) 91;
    public static byte kMipSoundFile_MOOD_GRUMPY = (byte) 92;
    public static byte kMipSoundFile_MOOD_HAPPY = (byte) 93;
    public static byte kMipSoundFile_MOOD_IDEA = (byte) 94;
    public static byte kMipSoundFile_MOOD_IMPATIENT = (byte) 95;
    public static byte kMipSoundFile_MOOD_NICE = (byte) 96;
    public static byte kMipSoundFile_MOOD_SAD = (byte) 97;
    public static byte kMipSoundFile_MOOD_SHORT = (byte) 98;
    public static byte kMipSoundFile_MOOD_SLEEPY = (byte) 99;
    public static byte kMipSoundFile_MOOD_TIRED = (byte) 100;
    public static byte kMipSoundFile_ONEKHZ_500MS_8K16BIT = (byte) 1;
    public static byte kMipSoundFile_SHORT_MUTE_FOR_STOP = (byte) 105;
    public static byte kMipSoundFile_SOUND_BOOST = (byte) 101;
    public static byte kMipSoundFile_SOUND_CAGE = (byte) 102;
    public static byte kMipSoundFile_SOUND_GUNS = (byte) 103;
    public static byte kMipSoundFile_SOUND_ZINGS = (byte) 104;
    public static byte kMipStop = (byte) 119;
    public static byte kMipTransmitIRCommand = (byte) -116;
    public static byte kTurnLeftByAngle = (byte) 115;
    public static byte kTurnRightByAngle = (byte) 116;

    public enum kMipHeadLedValue {
        kMipHeadLed_Off((byte) 0),
        kMipHeadLed_On((byte) 1),
        kMipHeadLed_BlinkSlow((byte) 2),
        kMipHeadLed_BlinkFast((byte) 3),
        kMipHeadLed_FadeIn((byte) 4);
        
        byte value;

        private kMipHeadLedValue(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kMipHeadLedValue getParamWithValue(byte value) {
            for (kMipHeadLedValue param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum kMipPingResponse {
        kMipPingResponseNormalRomNoBootloader((byte) 0),
        kMipPingResponseNormalRomHasBootloader((byte) 1),
        kMipPingResponseBootloader((byte) 2);
        
        byte value;

        private kMipPingResponse(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kMipPingResponse getParamWithValue(byte value) {
            for (kMipPingResponse param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }

    public enum kMipResetMcu {
        kMipResetMcu_NormalReset((byte) 1),
        kMipResetMcu_ResetAndForceBootloader((byte) 2);
        
        byte value;

        private kMipResetMcu(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return this.value;
        }

        public static kMipResetMcu getParamWithValue(byte value) {
            for (kMipResetMcu param : values()) {
                if (param.value == value) {
                    return param;
                }
            }
            return null;
        }
    }
}
